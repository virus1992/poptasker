<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TaskKeywords extends Model {

    public $table = 'task_keywords';
    public $primaryKey = 'id';
    protected $fillable = [
        'keyword_id', 'task_id'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public static function getKeywordWiseData($filter = '') {
        $data = TaskKeywords::select()
                ->join('tasks', 'tasks.id', '=', 'task_keywords.task_id')
                ->join('keywords', 'keywords.id', '=', 'task_keywords.keyword_id');
        if ($filter) {
            $data->whereBetween('start_date', [Carbon::parse($filter['from_date'])->format('Y-m-d'), Carbon::parse($filter['to_date'])->format('Y-m-d')]);
        } else {
            $data->whereBetween('start_date', [Carbon::now()->addMonths(-1)->format('y-m-d'), Carbon::now()->format('y-m-d')]);
        }
        $details = $data->get()->toArray();
        return $details;
    }

    public static function getKeywordRatingData($filter = '') {
        $data = Tasks::select()
                ->leftjoin('reviews', 'reviews.task_id', '=', 'tasks.id');
        if ($filter) {
            $data->whereBetween('start_date', [Carbon::parse($filter['from_date'])->format('Y-m-d'), Carbon::parse($filter['to_date'])->format('Y-m-d')]);
        } else {
            $data->whereBetween('start_date', [Carbon::now()->addMonths(-1)->format('y-m-d'), Carbon::now()->format('y-m-d')]);
        }
        $details = $data->get()->toArray();
        return $details;
    }

    public static function getKeywordbyTask($id) {
        $keyword = TaskKeywords::where(['task_id' => $id])
                        ->join('keywords', 'keywords.id', '=', 'task_keywords.keyword_id')
                        ->get()->toArray();
        return $keyword;
    }

}
