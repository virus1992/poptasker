<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Favorite extends Model {

    public $table = 'favorite';
    public $primaryKey = 'id';
    protected $fillable = [
        'from_id', 'to_id', 'is_favorite'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public static function makeUserFavorite($fromid, $toid, $is_fav) {
        $favorite = Favorite::updateOrCreate([
                    'from_id' => $fromid,
                    'to_id' => $toid
                        ], [
                    'from_id' => $fromid,
                    'to_id' => $toid,
                    'is_favorite' => $is_fav
        ]);
        return $favorite;
    }

    public static function getFavoriteEmployees() {
        $favoritelist = Favorite::select('user_details.*', 'users.email', 'favorite.created_at as applicationdate', 'favorite.*', DB::raw("(SELECT count(endorsements.id) FROM endorsements
                                WHERE endorsements.employee_id = favorite.to_id AND is_endorse = 1)as endorsements"), DB::raw("(SELECT SUM(reviews.rating) FROM reviews
                                WHERE reviews.employee_id = favorite.to_id) as ratings"), DB::raw("(SELECT COUNT(reviews.review_id) FROM reviews
                                WHERE reviews.employee_id = favorite.to_id) as totalratings"))
                        ->where(['favorite.from_id' => Auth::id()])
                        ->where(['favorite.is_favorite' => 1])
                        ->join('user_details', 'favorite.to_id', '=', 'user_details.user_id')
                        ->join('users', 'favorite.to_id', '=', 'users.id')->paginate(\Config::get('constants.PAGINATE'));
        return $favoritelist;
    }

}
