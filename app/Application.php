<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
use URL;
use Auth;

class Application extends Model {

    public $table = 'applications';
    public $primaryKey = 'application_id';
    protected $fillable = [
        'user_id', 'task_id', 'application_status'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public function getUser() {
        return $this->hasOne('App\Users', 'id', 'user_id');
    }

    public static function checkApplication($data) {
        $check = Application::where(['user_id' => Auth::id(), 'task_id' => $data['task_id']])->first();
        return $check;
    }

    public static function checkUserApplication($data) {
        $check = Application::where(['user_id' => $data['employee_id'], 'task_id' => $data['task_id']])->first();
        return $check;
    }

    public static function getApplicationsList($taskid) {
        $application = Application::select('user_details.*', 'users.email', 'applications.created_at as applicationdate', 'applications.*', DB::raw("(SELECT count(endorsements.id) FROM endorsements
                                WHERE endorsements.employee_id = applications.user_id AND is_endorse = 1)as endorsements,CASE WHEN EXISTS (SELECT favorite.id from favorite where favorite.from_id=" . Auth::id() . " AND favorite.to_id = applications.user_id AND is_favorite = 1) THEN 1 ELSE 0 END as is_favorite"), DB::raw("(SELECT SUM(reviews.rating) FROM reviews
                                WHERE reviews.employee_id = applications.user_id) as ratings"), DB::raw("(SELECT COUNT(reviews.review_id) FROM reviews
                                WHERE reviews.employee_id = applications.user_id) as totalratings"))
                        ->where(['applications.task_id' => $taskid])
                        ->where(['applications.application_status' => 'accepted'])
                        ->join('user_details', 'applications.user_id', '=', 'user_details.user_id')
                        ->join('users', 'applications.user_id', '=', 'users.id')->paginate(\Config::get('constants.PAGINATE'));
        return $application;
    }

    public static function getApplicant($taskid, $user) {
        $application = Application::select('user_details.*', 'users.email', 'applications.created_at as applicationdate', 'applications.*', DB::raw("(SELECT count(endorsements.id) FROM endorsements
                                WHERE endorsements.employee_id = applications.user_id AND is_endorse = 1)as endorsements"), DB::raw("(SELECT SUM(reviews.rating) FROM reviews
                                WHERE reviews.employee_id = applications.user_id) as ratings"), DB::raw("(SELECT COUNT(reviews.review_id) FROM reviews
                                WHERE reviews.employee_id = applications.user_id) as totalratings"))
                        ->where(['applications.task_id' => $taskid])
                        ->where(['applications.user_id' => $user])
                        ->where(['applications.application_status' => 'accepted'])
                        ->join('user_details', 'applications.user_id', '=', 'user_details.user_id')
                        ->join('users', 'applications.user_id', '=', 'users.id')->first();
        return $application;
    }

    public static function updateApplication($task_id, $user_id) {
        $get = Application::where(['task_id' => $task_id, 'user_id' => $user_id])->first();
        $get->application_status = 'rejected';
        $get->save();
    }

}
