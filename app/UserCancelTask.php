<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCancelTask extends Model {

    public $table = 'user_cancel_job';
    protected $fillable = [
        'user_id', 'task_id'];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public function getTaskDetails() {
        return $this->hasOne('App\Tasks', 'id', 'user_id');
    }

}
