<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Transactions extends Model {

    public $table = 'transactions';
    protected $fillable = [
        'user_id', 'transaction_id', 'amount', 'transaction_status'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public static function addTransaction($data) {
        $create = Transactions::create([
                    'user_id' => Auth::id(),
                    'transaction_id' => $data['id'],
                    'amount' => ($data['amount'] / 100),
                    'transaction_status' => (($data['status'] == 'succeeded') ? self::IS_TRUE : self::IS_FALSE)
        ]);
        return $create;
    }

}
