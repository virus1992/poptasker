<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskPictures extends Model {

    public $table = 'task_pictures';
    protected $fillable = [
        'picture', 'task_id'
    ];

}
