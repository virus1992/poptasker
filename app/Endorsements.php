<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Endorsements extends Model {

    public $table = 'endorsements';
    public $primaryKey = 'id';
    protected $fillable = [
        'employer_id', 'employee_id', 'is_endorse', 'task_id',
    ];

    public static function createEndorsement($data) {
        $endorse = Endorsements::updateOrCreate([
                    'employer_id' => Auth::id(),
                    'employee_id' => $data['employee_id'],
                    'task_id' => $data['task_id']], [
                    'is_endorse' => $data['is_endorse']
                        ]
        );
    }

}
