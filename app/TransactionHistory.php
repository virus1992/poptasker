<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model {

    public $table = 'transaction_history';
    protected $fillable = [
        'user_id', 'task_id', 'amount', 'type', 'commision', 'is_paid'
    ];

    public static function addTransactionnHistroy($user_id, $data) {
        $create = TransactionHistory::create([
                    'user_id' => $user_id,
                    'task_id' => $data['task_id'],
                    'amount' => $data['amount'],
                    'type' => $data['type'],
                    'commision' => ($data['commision']) ? $data['commision'] : null,
        ]);
        return $create;
    }

    public static function getTransactionHistory($user_id, $type) {
        $transation = TransactionHistory::where(['user_id' => $user_id, 'type' => $type])
                        ->join('tasks', 'tasks.id', '=', 'transaction_history.task_id')->paginate(\Config::get('constants.PAGINATE'));
        return $transation;
    }

}
