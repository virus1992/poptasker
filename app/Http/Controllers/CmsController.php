<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repositories\CmsRepository;

class CmsController extends Controller {

    public function __construct(CmsRepository $CmsRepository) {
        $this->CmsRepository = $CmsRepository;
    }

    public function terms_and_conditions() {
        $Cms = $this->CmsRepository->getcmsrecord("TERMS_AND_CONDITIONS");
        return view('cms.terms_and_conditions', compact('Cms'));
    }
    
    public function privacy_policy() {
        $Cms = $this->CmsRepository->getcmsrecord("PRIVACY_POLICY");
        return view('cms.privacy_policy', compact('Cms'));
    }
    
    public function aboutus() {
        $Cms = $this->CmsRepository->getcmsrecord("ABOUTUS");
        return view('cms.aboutus', compact('Cms'));
    }

}
