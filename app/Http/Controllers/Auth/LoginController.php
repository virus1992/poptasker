<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Services\UserService;
use App\Http\Services\CommonTrait;
use App\Users;
use Illuminate\Http\Request;
use Validator;
use DB;
use URL;
use App\Http\Services\ResponseTrait;

class LoginController extends Controller {

    use CommonTrait;
    use ResponseTrait;
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    private $userService;

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService) {
        $this->middleware('guest')->except('logout');
        $this->userService = $userService;
    }

    protected function validatormanually(array $data) {
        return Validator::make($data, [
                    'password' => 'required|string|min:6',
                    'confirm_password' => 'required|same:password',
        ]);
    }

    public function verifyEmail($token) {
        $user = Users::getUserFromToken($token);
        if ($user) {
            $user->is_active = 1;
            $user->remember_token = null;
            $user->save();
            $this->userService->saveDeviceInfo($user->id);
            echo 'Your Email has been Verified!!';
        } else {
            echo 'Not Authorised...........';
        }
    }

    public function forgotPassword($token) {
        $user = Users::getUserFromToken($token);
        if ($user) {
            return view('resetpassword', ['user' => $user]);
        } else {
            echo 'Not Authorised...........';
        }
    }

    public function resetPassword(Request $request) {
        $data = $request->all();
        $validator = $this->validatormanually($data);
        if ($validator->fails()) {
            return $this->responseJson('error', $validator->errors()->first(), 400);
        }
        $reset = Users::resetPassword($data['user_id'], $data['password']);
        if ($reset) {
            return view('resetpasswordsuccess', ['user' => $user]);
//            redirect()->back()->with('success',  \Config::get('constants.PASSWORD_RESET_SUCCESSFULLY'));
////            return redirect()->route('statistics.index')->with('success',  \Config::get('constants.PASSWORD_RESET_SUCCESSFULLY'));
////            return $this->responseJson('success', \Config::get('constants.PASSWORD_RESET_SUCCESSFULLY'), 200);
        } else {
            return view('resetpassworderror', ['user' => $user]);
        }
    }

}
