<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ResponseTrait;
use App\Http\Services\CommonTrait;
use App\Http\Services\UserService;
use App\Http\Services\MailTrait;
use App\Http\Services\NotificationTrait;
use App\Http\Transformer\UserTransformer;
use App\Users;
use App\Tasks;
use App\UserDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use DB;
use App\Login;
use URL;
use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use Srmklive\PayPal\Facades\PayPal;

class LoginController extends Controller {

    use ResponseTrait;
    use CommonTrait;
    use MailTrait;
    use NotificationTrait;

    private $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    protected function validatormanually(array $data) {
        return Validator::make($data, [
                    'email' => 'required',
                    'password' => 'required',
        ]);
    }

    protected function validatorResetPassword(array $data) {
        return Validator::make($data, [
                    'email' => 'required|string|email|max:255',
        ]);
    }

    public function login(Request $request) {
        $data = $request->all();
        $validator = $this->validatormanually($data);
        if ($validator->fails()) {
            return $this->responseJson('error', $validator->errors()->first(), 400);
        }
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'is_deleted' => 0])) {
            $user = Auth::user();
            if ($user->is_deleted == 1) {
                return $this->responseJson('error', \Config::get('constants.USER_DELETE'), 400);
            } else if ($user->is_active == 0) {
                return $this->responseJson('error', \Config::get('constants.USER_NOT_CONFIRMED'), 400);
            }
            $data['user_id'] = $user->id;
            $checkdevice = Login::checkDeviceStatus($data['user_id']);
            $addToken = Users::addFcmToken($data);
            if ($checkdevice == 0) {
                $token = $this->generateAuthToken();
                $data['api_token'] = $token;
                $update = Users::updateToken($data);
            } else {
                $update = Users::where(['id' => $data['user_id'], 'is_deleted' => 0])->first();
            }
            $this->userService->saveDeviceInfo($user->id, $data['device_id']);
            $data = (new UserTransformer)->transformLogin($update);
            return $this->responseJson('success', \Config::get('constants.LOGIN_SUCCESS'), 200, $data);
        } else {
            return $this->responseJson('error', \Config::get('constants.LOGIN_FAILED'), 400);
        }
    }

    public function loginWithFb(Request $request) {
        $data = $request->all();
        if ($data['fb_token']) {
            $user = Users::getFacebookToken($data);
            if ($user) {
                $data['user_id'] = $user->id;
                $checkdevice = Login::checkDeviceStatus($data['user_id']);
                $addToken = Users::addFcmToken($data);
                if ($checkdevice == 0) {
                    $token = $this->generateAuthToken();
                    $data['api_token'] = $token;
                    $update = Users::updateToken($data);
                } else {
                    $update = Users::find($data['user_id']);
                }
                $data = (new UserTransformer)->transformLogin($update);
            } else {
                $existuser = Users::checkUserEmail($data);
                if ($existuser) {
                    $existuser->fb_token = $data['fb_token'];
                    $existuser->save();
//                    return $this->responseJson('error', \Config::get('constants.USER_EXISTS'), 400);
                    $data['user_id'] = $existuser->id;
                    $checkdevice = Login::checkDeviceStatus($data['user_id']);
                    $addToken = Users::addFcmToken($data);
                    if ($checkdevice == 0) {
                        $token = $this->generateAuthToken();
                        $data['api_token'] = $token;
                        $update = Users::updateToken($data);
                    } else {
                        $update = Users::find($data['user_id']);
                    }
                    $data = (new UserTransformer)->transformLogin($update);
                } else {
                    DB::beginTransaction();
                    try {
                        $data['is_active'] = 1;
                        $useradd = Users::create($data);
                        $userdetails = UserDetails::addUserdetails($useradd->id, $data);
                        $data['user_id'] = $useradd->id;
                        $checkdevice = Login::checkDeviceStatus($data['user_id']);
                        $addToken = Users::addFcmToken($data);
                        if ($checkdevice == 0) {
                            $token = $this->generateAuthToken();
                            $data['api_token'] = $token;
                            $update = Users::updateToken($data);
                        } else {
                            $update = Users::where(['id' => $data['user_id'], 'is_deleted' => 0])->first();
                        }
                        $remeber_token = $this->generateRandomString();
                        $useradd->remember_token = $remeber_token;
                        $useradd->save();
                        $this->userService->saveDeviceInfo($useradd->id);
                        $array_mail = [
                            'type' => 'registration',
                            'to' => $data['email'],
                            'data' => [
                                'url' => URL::to('/verify/email/' . $remeber_token),
                                'confirmation_code' => $remeber_token,
                                'user' => $update
                            ],
                        ];
                        $mail = $this->sendEmail($array_mail);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return $this->responseJson('error', $e->getMessage(), 400);
                    }
                    DB::commit();
                    $data = (new UserTransformer)->transformLogin($update);
                }
            }

            return $this->responseJson('success', \Config::get('constants.LOGIN_SUCCESS'), 200, $data);
        } else {
            return $this->responseJson('error', \Config::get('constants.LOGIN_FAILED'), 400);
        }
    }

    public function loginWithGoogle(Request $request) {
        $data = $request->all();
        if ($data['google_token']) {
            $user = Users::getGoogleToken($data);
            if ($user) {
                $data['user_id'] = $user->id;
                $checkdevice = Login::checkDeviceStatus($data['user_id']);
                $addToken = Users::addFcmToken($data);
                if ($checkdevice == 0) {
                    $token = $this->generateAuthToken();
                    $data['api_token'] = $token;
                    $update = Users::updateToken($data);
                } else {
                    $update = Users::where(['id' => $data['user_id'], 'is_deleted' => 0])->first();
                }
                $data = (new UserTransformer)->transformLogin($update);
            } else {
                $existuser = Users::checkUserEmail($data);
                if ($existuser) {
                    $existuser->google_token = $data['google_token'];
                    $existuser->save();
//                    return $this->responseJson('error', \Config::get('constants.USER_EXISTS'), 400);
                    $data['user_id'] = $existuser->id;
                    $checkdevice = Login::checkDeviceStatus($data['user_id']);
                    $addToken = Users::addFcmToken($data);
                    if ($checkdevice == 0) {
                        $token = $this->generateAuthToken();
                        $data['api_token'] = $token;
                        $update = Users::updateToken($data);
                    } else {
                        $update = Users::find($data['user_id']);
                    }
                    $data = (new UserTransformer)->transformLogin($update);
                } else {
                    DB::beginTransaction();
                    try {
                        $data['is_active'] = 1;
                        $useradd = Users::create($data);
                        $userdetails = UserDetails::addUserdetails($useradd->id, $data);
                        $data['user_id'] = $useradd->id;
                        $checkdevice = Login::checkDeviceStatus($data['user_id']);
                        $addToken = Users::addFcmToken($data);
                        if ($checkdevice == 0) {
                            $token = $this->generateAuthToken();
                            $data['api_token'] = $token;
                            $update = Users::updateToken($data);
                        } else {
                            $update = Users::find($data['user_id']);
                        }
                        $remeber_token = $this->generateRandomString();
                        $useradd->remember_token = $remeber_token;
                        $useradd->save();
                        $array_mail = [
                            'type' => 'registration',
                            'to' => $data['email'],
                            'data' => [
                                'url' => URL::to('/verify/email/' . $remeber_token),
                                'confirmation_code' => $remeber_token,
                                'user' => $update
                            ],
                        ];
                        $mail = $this->sendEmail($array_mail);
                        $this->userService->saveDeviceInfo($useradd->id);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return $this->responseJson('error', $e->getMessage(), 400);
                    }
                    DB::commit();
                    $data = (new UserTransformer)->transformLogin($update);
                }
            }
            return $this->responseJson('success', \Config::get('constants.LOGIN_SUCCESS'), 200, $data);
        } else {
            return $this->responseJson('error', \Config::get('constants.LOGIN_FAILED'), 400);
        }
    }

    public function resetPassword(Request $request) {
        $data = $request->all();
        $validator = $this->validatorResetPassword($data);
        if ($validator->fails()) {
            return $this->responseJson('error', $validator->errors()->first(), 400);
        }
        $check = Users::where(['email' => $data['email'], 'is_deleted' => 0])->first();
        if ($check) {
            $remeber_token = $this->generateRandomString();
            $check->remember_token = $remeber_token;
            $check->save();
            $array_mail = array(
                'type' => 'reset-password',
                'to' => $data['email'],
                'data' => [
                    'remember_token' => $remeber_token,
                    'user' => $check->userDetails,
                    'url' => ''
                ],
            );
            $this->sendEmail($array_mail);
            return $this->responseJson('success', \Config::get('constants.RESET_PASSWORD'), 200);
        } else {
            return $this->responseJson('error', \Config::get('constants.EMAIL_NOT_FOUND'), 400);
        }
    }

    public function testPayment(Request $request) {
        $data = $request->all();
        $user = Users::find($data['user_id']);
        $task = Tasks::find($data['task_id']);
        $noti_type = 'Application for Task ' . $task->title;
        $type = 'new-application';
        $message = $user->userDetails->firstname . ' has apply for your task ' . $task->title;
        $this->sendNotification($user, $task, $type, $noti_type, $message);
    }

    public function checkSocialLogin(Request $request) {
        $data = $request->all();
        $check = Users::checkSocialLogin($data);
        $data = [
            'is_exists' => ($check) ? 1 : 0,
        ];
        return $this->responseJson('success', \Config::get('constants.SOCIAL_STATE'), 200, $data);
    }

}
