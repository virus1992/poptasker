<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ResponseTrait;
use App\Http\Services\MailTrait;
use App\Http\Services\CommonTrait;
use App\Http\Services\ImageTrait;
use App\Http\Services\NotificationTrait;
use App\Http\Transformer\TaskTransformer;
use App\TaskPictures;
use App\Users;
use App\UserDetails;
use App\Keywords;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Tasks;
use App\Application;
use App\Http\Transformer\ApplicationTransformer;
use App\Reviews;

class ApplicationController extends Controller {

    use ResponseTrait;
    use MailTrait;
    use CommonTrait;
    use ImageTrait;
    use NotificationTrait;

    public function applyForJob(Request $request) {
        $data = $request->all();
        $getTask = Tasks::find($data['task_id']);
        if ($getTask->employer_id != Auth::id()) {
            if ($getTask->employee_id == '') {
                $check = Application::checkApplication($data);
                if (!$check) {
                    $data['user_id'] = Auth::id();
                    $apply = Application::create($data);
                    $getTask->total_application = $getTask->total_application + 1;
                    $getTask->save();
                    $user_details = Users::find($getTask->employer_id);
                    $user_seeker = Users::find(Auth::id());

                    /* New application Email to Task Poster */
                    $array_email = [
                        'type' => 'new-application',
                        'to' => $user_details->email,
                        'data' => [
                            'user_poster' => $user_details->userDetails,
                            'user_seeker' => $user_seeker->userDetails,
                            'task' => $getTask
                        ]
                    ];
                    $this->sendEmail($array_email);
                    /* New application notification to Task Poster */
                    $noti_type = 'task-application';
                    $noti_message = 'Congratulations! ' . $user_seeker->userDetails->firstname . ' has applied for ' . $getTask->title . '.Check out ' . $user_seeker->userDetails->firstname . '\'s profile quickly!';
                    $message = 'Congratulations! ' . $user_seeker->userDetails->firstname . ' has applied for ' . $getTask->title . '.Check out ' . $user_seeker->userDetails->firstname . '\'s profile quickly!';
                    $this->sendNotification($user_details, $getTask, $noti_type, $noti_message, $message);

                    /* Application submited mail to seeker */
                    $array_email2 = [
                        'type' => 'application-seeker',
                        'to' => $user_seeker->email,
                        'data' => [
                            'user_seeker' => $user_seeker->userDetails,
                            'task' => $getTask
                        ]
                    ];
                    $this->sendEmail($array_email2);
                    return $this->responseJson('success', \Config::get('constants.JOB_APPLYED') . $getTask->title, 200);
                } else {
                    return $this->responseJson('error', \Config::get('constants.JOB_ALREADY_APPLY'), 400);
                }
            } else {
                return $this->responseJson('error', \Config::get('constants.JOB_ALREADY_AWARDED'), 400);
            }
        } else {
            return $this->responseJson('error', \Config::get('constants.CANNOT_APPLU_POSTEDTASK'), 400);
        }
    }

    public function getApplicantsfromjob(Request $request) {
        $data = $request->all();
        $tasktitle = Tasks::select('*')->where(['id' => $data['task_id']])->first();
        $applications = Application::getApplicationsList($data['task_id']);
        if ($applications) {
            foreach ($applications as $application) {
                $application->completedjobs = Tasks::getCompletedJobs($application->user_id);
            }
            $data = (new ApplicationTransformer())->transformList($applications);
            $data['task'] = $tasktitle->title;
            $data['is_awarded'] = ($tasktitle->employee_id) ? 1 : 0;
            return $this->responseJson('success', \Config::get('constants.APPLICATIONS'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function getApplicant(Request $request) {
        $data = $request->all();
        $applicant = Application::getApplicant($data['task_id'], $data['employee_id']);
        if ($applicant) {
            $applicant->completedjobs = Tasks::getCompletedJobs($applicant->user_id);
//            $reviews = Reviews::getApplicantReviews($applicant->user_id);
//            foreach ($reviews as $key => $val) {
//                if ($val['profilepic'] != '') {
//                    $reviews[$key]['profilepic'] = UserDetails::getProfilepic($val['profilepic']);
//                }
//            }
//            $applicant->reviews = $reviews;
            $data = (new ApplicationTransformer())->transformUser($applicant);
            return $this->responseJson('success', \Config::get('constants.APPLICANT_PROFILE'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

}
