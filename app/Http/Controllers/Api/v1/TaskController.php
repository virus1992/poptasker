<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ResponseTrait;
use App\Http\Services\MailTrait;
use App\Http\Services\CommonTrait;
use App\Http\Services\ImageTrait;
use App\Http\Services\NotificationTrait;
use App\Http\Services\UserService;
use App\Http\Transformer\TaskTransformer;
use App\Http\Transformer\ApplicationTransformer;
use App\TaskPictures;
use App\Users;
use App\UserDetails;
use App\Keywords;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Tasks;
use App\Application;
use App\TaskKeywords;
use Carbon\Carbon;
use App\TransactionHistory;
use App\UserWallet;
use App\StripeUser;
use App\UserCancelTask;
use App\Messages;

class TaskController extends Controller {

    use ResponseTrait;
    use MailTrait;
    use CommonTrait;
    use ImageTrait;
    use NotificationTrait;

    private $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function getKeywords() {
        $keywords = Keywords::orderBy('id', 'DESC')->get();
        if ($keywords) {
            return $this->responseJson('success', \Config::get('constants.KEYWORD_LIST'), 200, $keywords);
        } else {
            return $this->responseJson('error', \Config::get('constants.NO_DATA'), 400);
        }
    }

    public function createTask(Request $request) {
        $data = $request->all();
        $user = Auth::user();
        if ($user->is_confirm == 0) {
            return $this->responseJson('error', \Config::get('constants.NOT_AUTHORIZE_TO_POST_JOB'), 422);
        }
        DB::beginTransaction();
        try {
            $data['employer_id'] = $user->id;
            $taskdetails = $data;
            unset($taskdetails['pictures']);
            $task = Tasks::createTask($taskdetails);
            $task_id = $task->id;
            if ($data['keywords']) {
                $keywords = explode(',', $data['keywords']);
                foreach ($keywords as $word) {
                    $keyw = ['keyword_id' => $word, 'task_id' => $task_id];
                    $add = TaskKeywords::create($keyw);
                }
            }
            if ($request->hasFile('pictures')) {
                $i = 0;
                foreach ($request['pictures'] as $picture) {
                    $image['picture'] = $this->addImage($picture, 'taskpictures', $i);
                    $image['task_id'] = $task_id;
                    $img = TaskPictures::create($image);
                    $i++;
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseJson('error', $e->getMessage(), 400);
        }
        DB::commit();
        $array_email = [
            'type' => 'task-added',
            'to' => $user->email,
            'data' => [
                'user_poster' => $user->userDetails
            ]
        ];
        $this->sendEmail($array_email);
        return $this->responseJson('success', \Config::get('constants.TASK_ADDED'), 200);
    }

    public function getActivities(Request $request) {
        $data = $request->all();
        $status = '';
        if ($data['status']) {
            $status = strtolower($data['status']);
        }
        $user = Auth::user();
        $activities = Tasks::getAllActivities($status);
        $data = (new TaskTransformer)->transformList($activities);
        return $this->responseJson('success', \Config::get('constants.TASK_DETAILS'), 200, $data);
    }

    /* Award Task to applicant */

    public function awardJob(Request $request) {
        $data = $request->all();
        $checkapplication = Application::checkUserApplication($data);
        if ($checkapplication) {
            $getTask = Tasks::where(['id' => $data['task_id'], 'status' => 'pending'])->first();
            if ($getTask) {
                DB::beginTransaction();
                try {
                    $makePayment = StripeUser::makePayment($getTask->employer_id, $getTask->wage);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return $this->responseJson('error', $e->getMessage(), 400);
                }
                if ($makePayment == FALSE) {
                    return $this->responseJson('error', \Config::get('constants.NO_PAYMENT_METHOD_FOUND'), 400);
                }
                DB::commit();
                $getTask->employee_id = $data['employee_id'];
                $getTask->status = 'accepted';
                if ($getTask->save()) {
                    $transaction_hostory = ['task_id' => $getTask->id, 'amount' => $getTask->wage, 'type' => 'spend', 'commision' => null];
                    TransactionHistory::addTransactionnHistroy($getTask->employer_id, $transaction_hostory);
                    UserWallet::addUpdateUserWallet($getTask->employer_id, ($getTask->wage * -1));
                    /* Email for Award job */
                    $user_details = Users::find($getTask->employee_id);
                    $getTask->start_time = Carbon::parse($getTask->start_time)->format('h:i a');
                    $getTask->start_date = Carbon::parse($getTask->start_date)->format('Y-m-d');
                    $user_poster = Users::find($getTask->employer_id);
                    $array_email = [
                        'type' => 'award-task',
                        'to' => $user_details->email,
                        'data' => [
                            'user_seeker' => $user_details->userDetails,
                            'user_poster' => $user_poster->userDetails,
                            'task' => $getTask
                        ]
                    ];
                    $this->sendEmail($array_email);

                    /* Send Task Awarded notificaion to job seeker */
                    $noti_type = 'award-task';
                    $noti_message = 'Congratulations! ' . $user_poster->userDetails->firstname . ' has accepted your application';
                    $message = 'Congratulations! ' . $user_poster->userDetails->firstname . ' has accepted your application';
                    $this->sendNotification($user_details, $getTask, $noti_type, $noti_message, $message);
                    /*                     * **************************** */

                    /* Email for Award job to other user to notify that task is not available. */
                    $applications = Application::where('user_id', '!=', $getTask->employee_id)->where(['task_id' => $getTask->id])->get();
                    foreach ($applications as $applicant) {
                        $user_seeker = Users::find($applicant->user_id);
                        $array_email_all = [
                            'type' => 'awarded-task',
                            'to' => $user_seeker->email,
                            'data' => [
                                'user_seeker' => $user_seeker->userDetails,
                                'task' => $getTask,
                                'url' => ''
                            ]
                        ];
                        $this->sendEmail($array_email_all);

                        /* Send Task Not availlable Notification to other task seeker */

                        $noti_type = 'awarded-task';
                        $noti_message = 'The ' . $getTask->title . '  is no longer available,Remember to quickly check out other tasks!';
                        $message = 'The ' . $getTask->title . '  is no longer available,Remember to quickly check out other tasks!';
                        $this->sendNotification($user_seeker, $getTask, $noti_type, $noti_message, $message);
                    }
                    $this->userService->createConversation(Auth::id(), $getTask);
                    return $this->responseJson('success', \Config::get('constants.AWARD_JOB'), 200);
                } else {
                    return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
                }
            } else {
                return $this->responseJson('error', \Config::get('constants.ALREADY_AWARDED'), 400);
            }
        } else {
            return $this->responseJson('error', \Config::get('constants.NOT_APPLIED'), 400);
        }
    }

    /* List of Applications */

    public function getApplications(Request $request) {
        $data = $request->all();
        $applications = Tasks::getPostedApplications();
        if ($applications) {
            $data = (new ApplicationTransformer())->transformListApplication($applications);
            return $this->responseJson('success', \Config::get('constants.APPLICATIONS_POSTED'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    /* User Cancel job */

    public function cancelJob(Request $request) {
        $data = $request->all();
        $user_id = Auth::id();
        $task = Tasks::where(['id' => $data['task_id']])->first();
        if ($task->employee_id == $user_id) {
            $current_date = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('Y-m-d');
            if ($task->start_date <= $current_date) {
                return $this->responseJson('error', \Config::get('constants.APPLICATION_CANNOT_CANCELLED'), 400);
            } else {
                $add_cancel = ['user_id' => Auth::id(), 'task_id' => $data['task_id']];
                UserCancelTask::create($add_cancel);
                $task->employee_id = null;
                $task->status = 'pending';
                $task->total_application = $task->total_application - 1;
                $task->save();
                $application = Application::updateApplication($task->id, $user_id);
                $message = Messages::updateMessages($task);
                return $this->responseJson('success', \Config::get('constants.APPLICATION_CANCELLED'), 200);
            }
        } else {
            $task->status = 'cancelled';
            $task->is_deleted = 1;
            if ($task->save()) {
                /* Email for Cancel task */
                $applicants = $task->getApplications->toArray();
                foreach ($applicants as $application) {
                    $user_details = Users::find($application['user_id']);
                    $user_poster = Users::find($task->employer_id);
                    $array_email = [
                        'type' => 'cancel-task',
                        'to' => $user_details->email,
                        'data' => [
                            'user_seeker' => $user_details->userDetails,
                            'user_poster' => $user_poster->userDetails,
                            'task' => $task
                        ]
                    ];
                    $this->sendEmail($array_email);

                    /* notification for task cancel */
                    $noti_type = 'task-canceled';
                    $noti_message = 'The task ' . $task->title . ' has been cancelled,Remember to quickly check out other tasks on PopTasker.';
                    $message = 'The task ' . $task->title . ' has been cancelled,Remember to quickly check out other tasks on PopTasker.';
                    $this->sendNotification($user_details, $task, $noti_type, $noti_message, $message);
                }
                $message = Messages::updateMessages($task);
                return $this->responseJson('success', \Config::get('constants.TASK_CANCELLED'), 200);
            } else {
                return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
            }
        }
    }

    public function completeJob(Request $request) {
        $data = $request->all();
        $task = Tasks::getTaskByTaskid($data['task_id']);
        if ($task) {
            $task->status = 'completed';
            $task->save();
            $user_details = Users::find($task->employer_id);
            $noti_type = 'task-completed';
            $noti_message = 'Congratulations! Your task has been finished. Happy Popping';
            $message = 'Congratulations! Your task has been finished. Happy Popping';
            $this->sendNotification($user_details, $task, $noti_type, $noti_message, $message);
            return $this->responseJson('success', \Config::get('constants.TASK_COMPLETED'), 200);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function getTaskDetails(Request $request) {
        $data = $request->all();
        $taskdetails = Tasks::getTaskById($data['task_id']);
        if ($taskdetails) {
            $current_date = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('Y-m-d');
            if ($current_date == $taskdetails->start_date) {
                $current_time = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('H:i:s');
                $task_time = Carbon::parse($taskdetails->start_time)->format('H:i:s');
                if ($task_time <= $current_time && $taskdetails->employee_id != '') {
                    $taskdetails->is_start = 1;
                } else {
                    $taskdetails->is_start = 0;
                }
            } elseif ($current_date > $taskdetails->start_date && $taskdetails->employee_id != '') {
                $taskdetails->is_start = 1;
            } else {
                $taskdetails->is_start = 0;
            }
            $data = (new TaskTransformer)->transformTask($taskdetails);
            return $this->responseJson('success', \Config::get('constants.TASK_DETAILS'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function getTaskNotReviewed() {
        $user_id = Auth::id();
        $getnotreviewedtask = Tasks::getNotReviewedTask($user_id);
        if ($getnotreviewedtask) {
            $data = (new TaskTransformer)->transformTaskNoReview($getnotreviewedtask);
            return $this->responseJson('success', \Config::get('constants.TASK_NO_REVIEW_DETAILS'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

}
