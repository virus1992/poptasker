<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\SaveSearch;
use App\Http\Services\ResponseTrait;
use App\UserDetails;
use App\Tasks;
use App\Http\Transformer\TaskTransformer;

class SearchController extends Controller {

    use ResponseTrait;

    public function saveSearch(Request $request) {
        $data = $request->all();
        $save = SaveSearch::saveSearchFilter($data, Auth::id());
        $saveLocation = UserDetails::saveUserLocation($data);
        $limit = \Config::get('constants.PAGINATE');
        $offset = 0;
        if ($data['page'] == 1) {
            $offset = 0;
        } else if ($data['page'] != 'all') {
            $offset = (($data['page'] - 1) * $limit);
        }
        $jobs = Tasks::getNearestJobs($data, $offset, $limit);
        $data['currentPage'] = $data['page'];
        if (count($jobs) < (int) $limit) {
            $data['hasMorePages'] = false;
        } else {
            $data['hasMorePages'] = true;
        }
        $data = (new TaskTransformer)->transformListNearBy($jobs, $data);
        if ($save) {
            return $this->responseJson('success', \Config::get('constants.SAVE_SEARCH'), 200, $data);
        } else {
            return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
        }
    }

    public function searchNearByJobs(Request $request) {
        $data = $request->all();
        $saveLocation = UserDetails::saveUserLocation($data);
        $limit = \Config::get('constants.PAGINATE');
        $offset = 0;
        if ($data['page'] == 1) {
            $offset = 0;
        } else if ($data['page'] != 'all') {
            $offset = (($data['page'] - 1) * $limit);
        }
        $jobs = Tasks::getNearestJobs($data, $offset, $limit);
        $data['currentPage'] = $data['page'];
        if (count($jobs) < (int) $limit) {
            $data['hasMorePages'] = false;
        } else {
            $data['hasMorePages'] = true;
        }
        $data = (new TaskTransformer)->transformListNearBy($jobs, $data);
        if ($data) {
            return $this->responseJson('success', \Config::get('constants.NEARBY_JOBS'), 200, $data);
        } else {
            return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
        }
    }

    public function getFilters() {
        $user_id = Auth::id();
        $getFilter = SaveSearch::getFiltersByUser($user_id);
        if ($getFilter) {
            $data = $getFilter->toArray();
            $data['keywords'] = explode(',', $data['keywords']);
            return $this->responseJson('success', \Config::get('constants.FILTERS'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

}
