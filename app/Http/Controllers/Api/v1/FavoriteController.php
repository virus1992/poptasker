<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Favorite;
use App\Http\Transformer\UserTransformer;
use App\Http\Services\ResponseTrait;
use Auth;
use App\Tasks;

class FavoriteController extends Controller {

    use ResponseTrait;

    public function favoriteEmployee(Request $request) {
        $data = $request->all();
        $userid = Auth::id();
        $favorite = Favorite::makeUserFavorite($userid, $data['employee_id'], $data['is_favorite']);
        if ($favorite) {
            if ($data['is_favorite'] == 1) {
                return $this->responseJson('success', \Config::get('constants.FAVORITE_EMPLOYEE'), 200);
            } else {
                return $this->responseJson('success', \Config::get('constants.REMOVE_FAVORITE_EMPLOYEE'), 200);
            }
        } else {
            return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
        }
    }

    public function getFavoriteEmployee(Request $request) {
        $userid = Auth::id();
        $favoritelist = Favorite::getFavoriteEmployees($userid);
        foreach ($favoritelist as $application) {
            $application->completedjobs = Tasks::getCompletedJobs($application->user_id);
        }
        $data = (new UserTransformer())->transformFavorite($favoritelist);
        return $this->responseJson('success', \Config::get('constants.FAVORITE_EMPLOYEE_LIST'), 200, $data);
    }

}
