<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\ResponseTrait;
use App\Http\Transformer\UserTransformer;
use App\Http\Transformer\ApplicationTransformer;
use App\Http\Services\ImageTrait;
use App\Http\Services\MailTrait;
use App\Http\Services\NotificationTrait;
use App\UserDetails;
use App\Users;
use Auth;
use App\Help;
use App\Tasks;
use App\Reviews;
use App\Endorsements;
use App\Messages;
use App\PayoutDetails;
use App\TransactionHistory;
use App\UserWallet;
use App\Settings;
use DB;
use App\Login;

class UserController extends Controller {

    use ResponseTrait;
    use ImageTrait;
    use NotificationTrait;
    use MailTrait;

    protected function validatormanually(array $data) {
        return Validator::make($data, [
                    'firstname' => 'sometimes|string|max:255',
                    'lastname' => 'sometimes|string|max:255',
                    'linkedin' => 'sometimes|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                    'phone' => 'sometimes|max:15|regex:/^(\+\d{1,3}[- ]?)?\d{10}$/',
                    'profile_pic' => 'sometimes|required|image|mimes:jpg,png,jpeg',
        ]);
    }

    public function editProfile(Request $request) {
        $data = $request->all();
        $validator = $this->validatormanually($data);
        if ($validator->fails()) {
            return $this->responseJson('error', $validator->errors()->first(), 400);
        }
        if ($request->hasFile('profilepic')) {
            $data['profilepic'] = $this->addImage($data['profilepic'], 'profilepic');
        } else {
            $data['profilepic'] = '';
        }
        $user_id = Auth::id();
        $update = UserDetails::editUserdetails($user_id, $data);
        $data = (new UserTransformer())->transformUserDetails($update);
        return $this->responseJson('success', \Config::get('constants.PROFILE_UPDATE'), 200, $data);
    }

    public function submitQuery(Request $request) {
        $data = $request->all();
        $submit = Help::addQuery($data, Auth::id());
        if ($submit) {
            return $this->responseJson('success', \Config::get('constants.QUERY_SUBMITED'), 200);
        } else {
            return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
        }
    }

    public function getProfile(Request $request) {
        $user_id = Auth::id();
        $userdetails = UserDetails::where(['user_id' => $user_id])->first();
        $userdetails = Users::getUserDetails($user_id);
        if ($userdetails) {
            $userdetails->completedjobs = Tasks::getCompletedJobs($userdetails->user_id);
            $data = (new ApplicationTransformer())->transformUser($userdetails);
            return $this->responseJson('success', \Config::get('constants.USER_PROFILE'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function reviewEmployee(Request $request) {
        $data = $request->all();
        DB::beginTransaction();
        try {
            $task = Tasks::find($data['task_id']);
            $task->no_show = $data['no_show'];
            $task->status = 'completed';
            $task->is_reviewed = 1;
            $task->save();
            if ($data['endorsement'] == 1) {
                $endorse = ['employer_id' => Auth::id(), 'employee_id' => $data['employee_id'], 'is_endorse' => 1, 'task_id' => $data['task_id']];
                $endorse = Endorsements::createEndorsement($endorse);
            }
            $review = ['employer_id' => Auth::id(), 'employee_id' => $data['employee_id'], 'comments' => $data['comments'], 'rating' => $data['rating'], 'task_id' => $data['task_id']];
            $postreview = Reviews::createReview($review);
            if ($postreview) {
                $user_poster = Users::find($task->employer_id);
                $user_seeker = Users::find($task->employee_id);
                $array_email_poster = [
                    'type' => 'task-complete-poster',
                    'to' => $user_poster->email,
                    'data' => [
                        'user' => $user_poster->userDetails,
                        'task' => $task,
                        'url_tasks' => '',
                        'url_review' => ''
                    ]
                ];
                $this->sendEmail($array_email_poster);
                $array_email_seeker = [
                    'type' => 'task-complete-seeker',
                    'to' => $user_seeker->email,
                    'data' => [
                        'user' => $user_seeker->userDetails,
                        'task' => $task,
                        'url_tasks' => '',
                        'url_review' => ''
                    ]
                ];
                $this->sendEmail($array_email_seeker);
                $getCommison = Settings::getSettings();
                $transaction_hostory = ['task_id' => $task->id, 'amount' => ($task->wage - (($task->wage * $getCommison->commision) / 100)), 'type' => 'earned', 'commision' => $getCommison->commision];
                TransactionHistory::addTransactionnHistroy($task->employee_id, $transaction_hostory);
                UserWallet::addUpdateUserWallet($task->employee_id, $task->wage);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseJson('error', $e->getMessage(), 400);
        }
        DB::commit();
        return $this->responseJson('success', \Config::get('constants.REVIEW_SUBMITTED'), 200);
    }

    public function getAllMessages() {
        $user_id = Auth::id();
        $messages = Messages::getAllUserConversations($user_id);
//        print_r($messages->toArray());die;
        if ($messages) {
            $data = (new UserTransformer())->transformUserMessages($messages);
            return $this->responseJson('success', \Config::get('constants.MESSAGES_LIST'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function addPayoutDetails(Request $request) {
        $data = $request->all();
        $userid = Auth::id();
        $adddetails = PayoutDetails::addPayoutDetails($userid, $data);
        return $this->responseJson('success', \Config::get('constants.PAYOUT_DETAILS'), 200);
    }

    public function getPayoutDetails() {
        $user_id = Auth::id();
        $data = PayoutDetails::getPayoutDetails($user_id);
        return $this->responseJson('success', \Config::get('constants.GET_PAYOUT_DETAILS'), 200, $data);
    }

    public function getUserProfile(Request $request) {
        $user_id = Auth::id();
        $data = $request->all();
        $userdetails = Users::getUserDetails($data['employee_id']);
        if ($userdetails) {
            $userdetails->completedjobs = Tasks::getCompletedJobs($userdetails->user_id);
//            $reviews = Reviews::getApplicantReviews($userdetails->user_id);
//            foreach ($reviews as $key => $val) {
//                if ($val['profilepic'] != '') {
//                    $reviews[$key]['profilepic'] = UserDetails::getProfilepic($val['profilepic']);
//                }
//            }
//            $userdetails->reviews = $reviews;
            $data = (new ApplicationTransformer())->transformUser($userdetails);
            return $this->responseJson('success', \Config::get('constants.USER_PROFILE'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function getUserReviews(Request $request) {
        $data = $request->all();
        $reviews = Reviews::getUserReviews($data['user_id']);
        if ($reviews) {
            $data = (new ApplicationTransformer())->transformUserReviews($reviews);
            return $this->responseJson('success', \Config::get('constants.USER_REVIEWS'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function userLogout(Request $request) {
        $data = $request->all();
        try {
            $user_id = Auth::id();
            $user = Users::find($user_id);
            $checktotaldevice = Login::checkDeviceStatus($user_id);
            if ($checktotaldevice == 1) {
                $user->api_token = null;
                $user->save();
            }
            $device = Login::getUser($data['device_id'], $user_id);
            if ($device) {
                $device->delete();
            }
        } catch (\Exception $e) {
            return $this->responseJson('error', $e->getMessage(), 400);
        }
        return $this->responseJson('success', \Config::get('constants.LOGOUT_SUCCESS'), 200);
    }

}
