<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\StripeUser;
use App\Http\Services\ResponseTrait;
use App\StripeCards;
use App\TransactionHistory;
use App\Settings;
use App\Http\Transformer\UserTransformer;
use DB;

class StripeController extends Controller {

    use ResponseTrait;

    public function makePayment(Request $request) {
        $data = $request->all();
        $user_id = Auth::id();
        DB::beginTransaction();
        try {
            $makePayment = StripeUser::makePayment($user_id, $data['amount']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseJson('error', $e->getMessage(), 400);
        }
        DB::commit();
        return $this->responseJson('success', \Config::get('constants.PAYMENT_SUCCESS'), 200);
    }

    public function checkPaymentMethod(Request $request) {
        $user_id = Auth::id();
        $check = StripeCards::checkForAvailableCards($user_id);
        if ($check) {
            return $this->responseJson('success', \Config::get('constants.CARD_AVAILBLE'), 200);
        } else {
            return $this->responseJson('error', \Config::get('constants.NO_CARD_AVAILBLE'), 400);
        }
    }

    public function addCard(Request $request) {
        $data = $request->all();
        $user = Auth::user();
        DB::beginTransaction();
        try {
            $stripe_user = StripeUser::createStripeUser($user->id, $data);
            $stripe_user->createToken($data, $user, $stripe_user);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseJson('error', $e->getMessage(), 400);
        }
        DB::commit();
        if ($stripe_user) {
            return $this->responseJson('success', \Config::get('constants.CARD_ADDED'), 200);
        } else {
            return $this->responseJson('error', \Config::get('constants.APP_ERROR'), 400);
        }
    }

    public function getTransactionHistory(Request $request) {
        $data = $request->all();
        $user_id = Auth::id();
        $History = TransactionHistory::getTransactionHistory($user_id, $data['type']);
        if ($History) {
            $data = (new UserTransformer)->transformHistory($History);
            return $this->responseJson('success', \Config::get('constants.TRANSACTION_HISTORY'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function getSavedCards() {
        $user_id = Auth::id();
        $getCards = StripeCards::getUserSavedCards($user_id);
        if ($getCards) {
            $data = (new UserTransformer)->transformCards($getCards);
            return $this->responseJson('success', \Config::get('constants.SAVED_CARDS'), 200, $data);
        } else {
            return $this->responseJson('success', \Config::get('constants.NO_DATA'), 200);
        }
    }

    public function deleteCard(Request $request) {
        $data = $request->all();
        $user_id = Auth::id();
        $card = StripeCards::find($data['card_id']);
        try {
            $deletecard = StripeUser::deleteCard($card['cust_id'], $card['card_id']);
            $card->card_status = 0;
            $card->save();
            return $this->responseJson('success', \Config::get('constants.DELETE_CARDS'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseJson('error', $e->getMessage(), 400);
        }
    }

}
