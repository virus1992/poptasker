<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ResponseTrait;
use App\Http\Services\MailTrait;
use App\Http\Services\CommonTrait;
use App\Http\Services\UserService;
use App\Users;
use App\UserDetails;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use URL;

class RegisterController extends Controller {

    private $userService;

    use ResponseTrait;
    use MailTrait;
    use CommonTrait;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    protected function validatormanually(array $data) {
        return Validator::make($data, [
                    'firstname' => 'required|string|max:255',
                    'lastname' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255',
                    'password' => 'required|string|min:6',
                    'confirm_password' => 'required|same:password',
        ]);
    }

    public function index(Request $request) {
        $data = $request->all();
        $existuser = Users::checkUserEmail($data);
        if ($existuser) {
            return $this->responseJson('error', \Config::get('constants.USER_EXISTS'), 400);
        } else {
            DB::beginTransaction();
            try {
                $validator = $this->validatormanually($data);
                if ($validator->fails()) {
                    return $this->responseJson('error', $validator->errors()->first(), 400);
                }
                $data['password'] = bcrypt($data['password']);
                $user = Users::create($data);
                $userdetails = UserDetails::addUserdetails($user->id, $data);
                $data['user_id'] = $user->id;
                $remeber_token = $this->generateRandomString();
                $user->remember_token = $remeber_token;
                $user->save();
                $array_mail = [
                    'type' => 'registration',
                    'to' => $data['email'],
                    'data' => [
                        'url' => URL::to('/verify/email/' . $remeber_token),
                        'confirmation_code' => $remeber_token,
                        'user' => $userdetails
                    ],
                ];
                $mail = $this->sendEmail($array_mail);
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->responseJson('error', $e->getMessage(), 400);
            }
            DB::commit();
            if ($userdetails) {
                return $this->responseJson('success', \Config::get('constants.REGISTER_SUCCESS'), 200);
            } else {
                return $this->responseJson('error', \Config::get('constants.REGISTER_FAILED'), 400);
            }
        }
    }

}
