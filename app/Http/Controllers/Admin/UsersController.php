<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\UsersRepository;
use App\Http\Repositories\TasksRepository;
use Illuminate\Routing\Route;
use App\Http\Services\ResponseTrait;

//*************************************************//
//@ * @controller   : UsersController
//@ * @todo         : Manage Users section
//************************************************//


class UsersController extends Controller {

    use ResponseTrait;

    public function __construct(UsersRepository $UsersRepository, TasksRepository $TasksRepository) {
        $this->middleware('auth.admin');
        $this->UsersRepository = $UsersRepository;
        $this->TasksRepository = $TasksRepository;
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Display all users
    // * @Date   : 1-June-2018
    //************************************************//

    public function index($usertype = NULL) {

        $Data = array();
        if (isset($usertype) && $usertype == "taskseeker") {
            $Users = $this->UsersRepository->getJobSeeker();
        } elseif (isset($usertype) && $usertype == "taskposter") {
            $Users = $this->UsersRepository->getJobPoster();
        } elseif (isset($usertype) && $usertype == "inactice") {
            $Users = $this->UsersRepository->getInactiveusers();
        } else {
            $Users = $this->UsersRepository->getAllUsers();
        }

        $Data['Users'] = $Users;
        $Data['usertype'] = $usertype;

        return view('admin.users.index', compact('Data'));
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Delete the record of User
    // * @Date   : 4-June-2018
    //************************************************//

    public function delete($id = NULL) {
        $Users = $this->UsersRepository->deleteUser($id);
        if ($Users) {
            return redirect()->route('users.index')->with('success', 'User deleted successfully !');
        } else {
            return redirect()->route('users.index')->with('error', 'Failed to delete user !');
        }
    }

    //*************************************************//
    // * @name   : changestatus
    // * @todo   : Change the status of users
    // * @Date   : 4-June-2018
    //************************************************//

    public function changestatus(Request $request) {
        $data = $request->all();
        $UserStatus = $this->UsersRepository->changeuserstatus($data);
        if ($UserStatus) {
            return $this->responseJson('success', \Config::get('constants.SAVE_SEARCH'), 200, $data);
        } else {
            return $this->responseJson('error', \Config::get('constants.SAVE_SEARCH'), 400, "");
        }
    }

    //*************************************************//
    // * @name   : details
    // * @todo   : Check details of user
    // * @Date   : 6-July-2018
    //************************************************//

    public function details($id = NULL) {

        $Data = [];
        $filterarray = [];
        $filterarray['user_id'] = $id;
        $UserDetails = $this->UsersRepository->getuserdetailsbyid($id);
        if ($UserDetails) {
            $Data['UserDetails'] = $UserDetails;
            $Data['ArchivedtaskCount'] = $this->TasksRepository->getArchievedTaskscount($filterarray); //get archived task count
            $Data['NoShowtaskCount'] = $this->TasksRepository->getnoshowTaskscount($filterarray); //get noshow task count
            $Data['DeletedTaskcount'] = $this->TasksRepository->getdeletedTaskscount($filterarray); //get Deleted task count
            $filterarray['task_status'] = "pending";
            $Data['PendingTaskcount'] = $this->TasksRepository->getTaskCountbystatus($filterarray); //get Pending taskcount
            $filterarray['task_status'] = "accepted"; 
            $Data['AcceptedTaskcount'] = $this->TasksRepository->getTaskCountbystatus($filterarray); // get Accepted Taskcount
            $filterarray['task_status'] = "cancelled";
            $Data['CanceledTaskcount'] = $this->TasksRepository->getTaskCountbystatus($filterarray); //get Cancelled Task count
            $filterarray['task_status'] = "completed";
            $Data['CompletedTaskcount'] = $this->TasksRepository->getTaskCountbystatus($filterarray); //Get completed Task count
            $Data['Taskscount'] = $this->TasksRepository->getAlltaskscount($filterarray); //GET COUNTER OF ALL TASKS 
            
            $Data['taskReviews'] = $this->UsersRepository->getuserReviews($filterarray); //GET COUNTER OF ALL TASKS 
        } else {
            return redirect()->route('users.index')->with('error', 'No user found!');
        }
        return view('admin.users.userdetails', compact('Data'));
    }

}
