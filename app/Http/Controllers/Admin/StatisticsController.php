<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\TasksRepository;
use App\Http\Repositories\UsersRepository;

//*************************************************//
//@ * @controller   : TasksController
//@ * @todo         : Manage Tasks section
//************************************************//

class StatisticsController extends Controller {

    public function __construct(TasksRepository $TasksRepository, UsersRepository $UsersRepository) {
        $this->middleware('auth.admin');
        $this->TasksRepository = $TasksRepository;
        $this->UsersRepository = $UsersRepository;
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Display all Tasks
    // * @Date   : 1-June-2018
    //************************************************//

    public function index(Request $request, $status = NULL) {

        $Data = array();
        $filterarray = array();
        $filterarray['task_status'] = $status;

        //************* GET TOTAL TASK AND TASK COUNT BY STATUS ***************//
        if ($request->isMethod('post')) {
            $startdate = (($request->input('startdate')) && $request->input('startdate') != "") ? date('Y-m-d', strtotime($request->input('startdate'))) : "";
            $enddate = (($request->input('enddate')) && $request->input('enddate') != "") ? date('Y-m-d', strtotime($request->input('enddate'))) : "";
            $userid = (($request->input('user_id')) && $request->input('user_id') != "") ? $request->input('user_id') : "";
            
            $filterarray['startdate'] = $startdate;
            $filterarray['enddate'] = $enddate;
            $filterarray['user_id'] = $userid;
            
            if (isset($enddate) && $enddate != "" && $enddate < $startdate) {
                return redirect()->route('admin.transactions.index')->with("error", "End Date should be greater than Start Date !");
            } else {
                $TotalTasks = $this->TasksRepository->getAlltaskscount($filterarray);

                if ($status == "archive") {
                    //*********** GET ALL ARCHIEVED TASKS *********//
                    $TasksByStatus = $this->TasksRepository->getArchievedTasks($filterarray);
                    $TasksByStatusCount = $this->TasksRepository->getArchievedTaskscount($filterarray);
                } elseif ($status == "noshow") {
                    $TasksByStatus = $this->TasksRepository->getnoshowTasks($filterarray);
                    $TasksByStatusCount = $this->TasksRepository->getnoshowTaskscount($filterarray);
                } elseif ($status == "deleted") {
                    $TasksByStatus = $this->TasksRepository->getdeletedTasks($filterarray);
                    $TasksByStatusCount = $this->TasksRepository->getdeletedTaskscount($filterarray);
                } else {
                    $TasksByStatus = $this->TasksRepository->getAlltasksBystatus($filterarray);
                    $TasksByStatusCount = $this->TasksRepository->getTaskCountbystatus($filterarray);
                }
                $Data['startdate'] = $request->input('startdate');
                $Data['enddate'] = $request->input('enddate');
                $Data['userid'] = $userid;
            }
        } else {
            $TotalTasks = $this->TasksRepository->getAlltaskscount();
            if ($status == "archive") {
                //*********** GET ALL ARCHIEVED TASKS *********//
                $TasksByStatus = $this->TasksRepository->getArchievedTasks();
                $TasksByStatusCount = $this->TasksRepository->getArchievedTaskscount();
            } elseif ($status == "noshow") {
                $TasksByStatus = $this->TasksRepository->getnoshowTasks();
                $TasksByStatusCount = $this->TasksRepository->getnoshowTaskscount();
            } elseif ($status == "deleted") {
                $TasksByStatus = $this->TasksRepository->getdeletedTasks();
                $TasksByStatusCount = $this->TasksRepository->getdeletedTaskscount();
            } else {
                $TasksByStatus = $this->TasksRepository->getAlltasksBystatus($filterarray);
                $TasksByStatusCount = $this->TasksRepository->getTaskCountbystatus($filterarray);
            }
        }
        $Users = $this->UsersRepository->getAllUsers(); // Get all users for dropdown
        $Data['Users'] = $Users;

        $Data['TotalTasks'] = $TotalTasks;
        $Data['TasksByStatus'] = $TasksByStatus;
        $Data['TasksByStatusCount'] = $TasksByStatusCount;
        $Data['status'] = $status;
        return view('admin.statistics.index', compact('Data'));
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Delete the record of Task
    // * @Date   : 6-June-2018
    //************************************************//

    public function delete($id = NULL, $status = NULL) {
        
        $Tasks = $this->TasksRepository->deleteTask($id);
        if ($Tasks) {
            return redirect()->route('statistics.index', ($status != 'all') ? $status : "")->with('success', 'Task deleted successfully !');
        } else {
            return redirect()->route('statistics.index', ($status != 'all') ? $status : "")->with('error', 'Failed to delete Task !');
        }
    }

}
