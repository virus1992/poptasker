<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ResponseTrait;
use App\Http\Repositories\TransactionHistoryRepository;
use App\Http\Repositories\UsersRepository;
use App\TransactionHistory;

//*************************************************//
//@ * @controller   : TransactionsController
//@ * @todo         : Manage Transactions
//************************************************//

class TransactionsController extends Controller {

    use ResponseTrait;

    public function __construct(TransactionHistoryRepository $TransactionHistoryRepository, UsersRepository $UsersRepository) {
        $this->middleware('auth.admin');
        $this->TransactionHistoryRepository = $TransactionHistoryRepository;
        $this->UsersRepository = $UsersRepository;
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Display All Transactions
    // * @Date   : 8-June-2018
    //************************************************//

    public function index(Request $request) {
        
        $filter = array();
        if ($request->isMethod('post')) {
            $startdate = (($request->input('startdate')) && $request->input('startdate') != "") ? date('Y-m-d', strtotime($request->input('startdate'))) : "";
            $enddate = (($request->input('enddate')) && $request->input('enddate') != "") ? date('Y-m-d', strtotime($request->input('enddate'))) : "";
            $tast_status = (($request->input('tast_status')) && $request->input('tast_status') != "") ? $request->input('tast_status') : "";
            $userid = (($request->input('userid')) && $request->input('userid') != "") ? $request->input('userid') : "";
            $filter['startdate'] = $startdate;
            $filter['enddate'] = $enddate;
            $filter['tast_status'] = $tast_status;
            $filter['userid'] = $userid;
            
            if (isset($enddate) && $enddate != "" && $enddate < $startdate) {
                return redirect()->route('admin.transactions.index')->with("error", "End Date should be greater than Start Date !");
            } else {
                $Transactions = $this->TransactionHistoryRepository->getAllTransactions($filter);
                $Transactions['Data'] = $Transactions;
                $Transactions['startdate'] = $request->input('startdate');
                $Transactions['enddate'] = $request->input('enddate');
                $Transactions['status'] = $tast_status;
                $Transactions['userid'] = $userid;
            }
        } else {
            $Transactions = $this->TransactionHistoryRepository->getAllTransactions();
            $Transactions['Data'] = $Transactions;
        }
        $Users = $this->UsersRepository->getAllUsers();
        $Transactions['Users'] = $Users;
        return view('admin.transactions.index', compact('Transactions'));
    }

    //*************************************************//
    // * @name   : getRefundableTask
    // * @todo   : Get record of refundable task
    // * @Date   : 5-July-2018
    //************************************************//

    public function getRefundableTask(Request $request) {
        $data = $request->all();
        if ($data['transactionid'] != "") {
            $Transactiondetail = TransactionHistory::select('amount', 'id', 'refund_amount')->where('id', $data['transactionid'])->first()->toArray();
            if ($Transactiondetail) {
                $Transactiondetail['balance'] = $Transactiondetail['amount'] - $Transactiondetail['refund_amount'];
                $Transactiondetail['extrarefunded'] = ($Transactiondetail['balance'] < 0) ?  $Transactiondetail['refund_amount'] - $Transactiondetail['amount'] : 0;
                return $this->responseJson('success', 'Record fetched successfully', 200, $Transactiondetail);
            } else {
                return $this->responseJson('error', "No record found", 400, []);
            }
        } else {
            return $this->responseJson('error', "Task id not found", 400, []);
        }
    }
    
    //*************************************************//
    // * @name   : refundamountUrl
    // * @todo   : Save refund details
    // * @Date   : 5-July-2018
    //************************************************//

    public function refundamount(Request $request) {
        $data = $request->all();
        if ($data['transactionid'] != "") {
            $Transaction = TransactionHistory::where(['id' => $data['transactionid']])->first();
            $Transaction->refund_reason = $data['refund_reason'];
            $Transaction->refund = 1;
            $Transaction->refund_amount = $Transaction['refund_amount'] + $data['refund_amount'];
            $Transaction = $Transaction->save();
            $Transactiondetail = TransactionHistory::select('amount', 'id', 'refund_amount', 'refund_reason')->where('id', $data['transactionid'])->first()->toArray();
            if ($Transactiondetail) {
                $Transactiondetail['balance'] = $Transactiondetail['amount'] - $Transactiondetail['refund_amount'];
                return $this->responseJson('success', 'Amount Refunded successfully', 200, $Transactiondetail);
            } else {
                return $this->responseJson('error', "No record found", 400, []);
            }
        }
    }

}
