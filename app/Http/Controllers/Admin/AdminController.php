<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Repositories\UsersRepository;
use App\Http\Repositories\TasksRepository;
use App\Http\Repositories\TransactionHistoryRepository;
use App\AdminUsers;
use App\Tasks;

//*************************************************//
//@ * @controller   : AdminController
//@ * @todo         : Manage Basic Admin panel with login logout dashboard
//************************************************//

class AdminController extends Controller {

    public function __construct(UsersRepository $UsersRepository, TasksRepository $TasksRepository, TransactionHistoryRepository $TransactionHistoryRepository) {
        $this->UsersRepository = $UsersRepository;
        $this->TasksRepository = $TasksRepository;
        $this->TransactionHistoryRepository = $TransactionHistoryRepository;
        $this->middleware('auth.admin', [
            'except' => [
                'createAccount',
            ]
        ]);
    }

    public function createAccount() {
        if (AdminUsers::all()->isNotEmpty()) {
            echo 'Admin account is already setup.';
            return;
        }

        AdminUsers::create([
            'username' => 'admin',
            'email' => 'admin@poptasker.com',
            'password' => bcrypt('123456'),
        ]);

        echo 'Admin account created successfully.';
    }

    //*************************************************//
    // * @name   : dashboard
    // * @todo   : Manage counters of all sections
    // * @Date   : 4-June-2018
    //************************************************//

    public function dashboard() {
//        dd(Auth::guard('admin')->user()); exit;

        $Data = array();
        $filterarray = [];
        $Userscount = $this->UsersRepository->getAlluserscount(); //GET COUNTERS OF ALL USERS
        $Taskscount = $this->TasksRepository->getAlltaskscount(); //GET COUNTERS OF ALL TASKS
        $InactiveUsercount = $this->UsersRepository->getInactiveusersCount();
        $filterarray['task_status'] = 'accepted';
        $AcceptedTaskscount = $this->TasksRepository->getTaskCountbystatus($filterarray); //GET COUNTERS OF ALL Accepted TASKS
        $filterarray['task_status'] = 'completed';
        $CompletedTaskscount = $this->TasksRepository->getTaskCountbystatus($filterarray); //GET COUNTERS OF ALL Completed TASKS
        $Payments = $this->TransactionHistoryRepository->getTotalPayment(); //GET TOTAL PAYMENTS
        $Payouts = $this->TransactionHistoryRepository->getTotalpayoutamount(); //GET TOTAL PAYMENTS
        
        $TaskPoster = Tasks::getTaskPosterCount(); //GET TOTAL COUNT OF TASK POSTER
        $TaskSeeker = Tasks::getTaskSeekerCount(); // GET TOTAL COUNT OF TASK SEEKER
        
        $InactiveUserCount = 

        $Data['Userscount'] = $Userscount;
        $Data['InactiveUsercount'] = $InactiveUsercount;
        $Data['Taskscount'] = $Taskscount;
        $Data['AcceptedTaskscount'] = $AcceptedTaskscount;
        $Data['CompletedTaskscount'] = $CompletedTaskscount;
        $Data['Payments'] = $Payments;
        $Data['Payouts'] = $Payouts;
        $Data['TaskPoster'] = $TaskPoster;
        $Data['TaskSeeker'] = $TaskSeeker;

        return view('admin.dashboard', compact('Data'));
    }

}
