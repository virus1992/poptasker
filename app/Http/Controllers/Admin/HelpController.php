<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\HelpRepository;
use App\Http\Services\ResponseTrait;

//*************************************************//
//@ * @controller   : HelpController
//@ * @todo         : Manage User queries
//************************************************//

class HelpController extends Controller {
    
    use ResponseTrait;
    
    public function __construct(HelpRepository $HelpRepository) {
        $this->middleware('auth.admin');
        $this->HelpRepository = $HelpRepository;
    }
    
    //*************************************************//
    // * @name   : index
    // * @todo   : Display All Users queries
    // * @Date   : 13-June-2018
    //************************************************//
    
    public function index() {
        $Helps = $this->HelpRepository->getAllHelps();
        
        return view('admin.help.index', compact('Helps'));
    }
    
    //*************************************************//
    // * @name   : changehelpstatus
    // * @todo   : Change the status of help
    // * @Date   : 13-June-2018
    //************************************************//

    public function changehelpstatus(Request $request) {
        $data = $request->all();
        $HelpStatus = $this->HelpRepository->changehelpstatus($data);
        if($HelpStatus) {
            return $this->responseJson('success', "Status updated successfully", 200, $data);
        } else {
            return $this->responseJson('success', "Failed to update status", 400, []);
        }
    }
}
