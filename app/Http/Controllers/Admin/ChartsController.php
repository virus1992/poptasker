<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Keywords;
use App\TaskKeywords;
use App\Tasks;
use Carbon\Carbon;
use App\Http\Repositories\TasksRepository;

//*************************************************//
//@ * @controller   : ChartsController
//@ * @todo         : Manage Charts
//************************************************//

class ChartsController extends Controller {

    public function __construct(TasksRepository $TasksRepository) {
        $this->middleware('auth.admin');
        $this->TasksRepository = $TasksRepository;
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Display All Charts
    // * @Date   : 1-June-2018
    //************************************************//

    public function index() {

        return view('admin.charts.index');
    }

    public function getCategorywiseTask(Request $request) {
        $data = $request->all();
        $filter = '';
        if ($data['from_date'] && $data['to_date']) {
            $filter = $data;
        }
        $getTaskKeywords = TaskKeywords::getKeywordWiseData($filter);
        $final_keyword = [];
        foreach ($getTaskKeywords as $keyword) {
            if (!array_key_exists($keyword['keyword_id'], $final_keyword)) {
                $final_keyword[$keyword['keyword_id']]['total'] = 1;
                $final_keyword[$keyword['keyword_id']]['keyword'] = $keyword['keyword'];
            } else {
                $final_keyword[$keyword['keyword_id']]['total'] = $final_keyword[$keyword['keyword_id']]['total'] + 1;
            }
        }
        $arr = [];
        foreach ($final_keyword as $keyword) {
            $arr[] = [
                'name' => $keyword['keyword'],
                'y' => $keyword['total'],
            ];
        }
        return json_encode($arr);
    }

    public function getJobseekerPoster(Request $request) {
        $data = $request->all();
        $filter = '';
        if ($data['from_date'] && $data['to_date']) {
            $filter = $data;
        }
        $task_poster = Tasks::getTaskPosterCount($filter);
        $task_seeker = Tasks::getTaskSeekerCount($filter);

        $arr = [[
        'name' => 'Task Poster',
        'y' => $task_poster,
            ], [
                'name' => 'Task Seeker',
                'y' => $task_seeker,
        ]];

        return json_encode($arr);
    }

    public function getTasksByStatus(Request $request) {

        $startdate = (($request->input('from_date')) && $request->input('from_date') != "") ? date('Y-m-d', strtotime($request->input('from_date'))) : "";
        $enddate = (($request->input('to_date')) && $request->input('to_date') != "") ? date('Y-m-d', strtotime($request->input('to_date'))) : "";


        $status = ['archive', 'pending', 'accepted', 'cancelled', 'completed', 'noshow', 'deleted'];
        $array = [];
        foreach ($status as $status) :
            if ($status == "archive") {
                $TasksByStatusCount = $this->TasksRepository->getArchievedTaskscount($startdate, $enddate);
                $TotalTasks = $TasksByStatusCount['count'];
            } elseif ($status == 'noshow') {
                $TasksByStatusCount = $this->TasksRepository->getnoshowTaskscount($startdate, $enddate);
                $TotalTasks = $TasksByStatusCount['count'];
            } elseif ($status == 'deleted') {
                $TasksByStatusCount = $this->TasksRepository->getdeletedTaskscount($startdate, $enddate);
                $TotalTasks = $TasksByStatusCount['count'];
            } elseif ($status == 'pending' || $status == 'accepted' || $status == 'cancelled' || $status == 'completed') {
                $TasksByStatusCount = $this->TasksRepository->getTaskCountbystatus($status, $startdate, $enddate);
                $TotalTasks = $TasksByStatusCount['count'];
            }
            $array[] = [
                'name' => ucfirst($status),
                'y' => $TotalTasks,
            ];
        endforeach;

        return json_encode($array);
    }

    public function getTransaction(Request $request) {
        $data = $request->all();
        $filterarray = [];
        $filterarray['year'] = (isset($data['year'])) ? $data['year'] : "";
        if ($data['select'] == 'week' || $data['select'] == '') {
            $transaction = Tasks::getTransactionbyWeek($filterarray);
        }
        if ($data['select'] == 'month') {
            $transaction = Tasks::getTransactionbyMonth($filterarray);
        }
        if ($data['select'] == 'quarterly') {
            $transaction = Tasks::getTransactionquarterly($filterarray);
        }
        if ($data['select'] == 'yearly') {
            $transaction = Tasks::getTransactionyearly($filterarray);
        }
        $arr = [];
        foreach ($transaction[0] as $key => $val) {
            $arr[] = [$key, $val];
        }
        return json_encode($arr);
    }

    public function getCategoryRatings(Request $request) {
        $data = $request->all();
        $filter = '';
        if ($data['from_date'] && $data['to_date']) {
            $filter = $data;
        }
        $getTasks = TaskKeywords::getKeywordRatingData($filter);
        $final_keyword = [];
        foreach ($getTasks as $task) {
            if ($task['review_id']) {
                $keywords = TaskKeywords::getKeywordbyTask($task['id']);
                foreach ($keywords as $keyword) {
                    if (!array_key_exists($keyword['keyword_id'], $final_keyword)) {
                        $final_keyword[$keyword['keyword_id']]['total_rating'] = $task['rating'];
                        $final_keyword[$keyword['keyword_id']]['total'] = 1;
                        $final_keyword[$keyword['keyword_id']]['keyword'] = $keyword['keyword'];
                    } else {
                        $final_keyword[$keyword['keyword_id']]['total'] = $final_keyword[$keyword['keyword_id']]['total'] + 1;
                        $final_keyword[$keyword['keyword_id']]['total_rating'] = $final_keyword[$keyword['keyword_id']]['total_rating'] + $task['rating'];
                    }
                }
            }
        }
        $arr = [];
        foreach ($final_keyword as $keyword) {
            $arr[] = [
                'name' => $keyword['keyword'],
                'y' => $keyword['total_rating'] / $keyword['total'],
            ];
        }
        return json_encode($arr);
    }

}
