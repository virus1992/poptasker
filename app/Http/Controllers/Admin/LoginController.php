<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\CommonTrait;
use App\AdminUsers;
use Illuminate\Http\Request;
use Validator;
use DB;
use URL;

class LoginController extends Controller {

    use CommonTrait;
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest.admin')->except('logout');
    }

    public function showLoginForm() {
        return view('admin.login');
    }
    
    public function username()
    {
	return 'username';
    }
    
    public function logout() {
        
        Auth::logout();
        $this->guard()->logout();

//        $request->session()->invalidate();

        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    protected function validatormanually(array $data) {
        return Validator::make($data, [
                    'password' => 'required|string|min:6',
                    'confirm_password' => 'required|same:password',
        ]);
    }

    public function verifyEmail($token) {
        $user_email = $this->decrypt_string($token);
        $user = Admin::getUserFromEmail($user_email, $token);
        if ($user) {
            $user->is_active = 1;
            $user->remember_token = null;
            $user->save();
            $this->userService->saveDeviceInfo($user->id);
            echo 'Your Email has been Verified!!';
        } else {
            echo 'Not Authorised...........';
        }
    }


}
