<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\CmsRepository;
use App\Cms;
use Validator;

//*************************************************//
//@ * @controller   : CmsController
//@ * @todo         : Manage Cms
//************************************************//

class CmsController extends Controller {

    public function __construct(CmsRepository $CmsRepository) {
        $this->middleware('auth.admin');
        $this->CmsRepository = $CmsRepository;
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Display All Cms
    // * @Date   : 11-June-2018
    //************************************************//

    public function index() {
        $Cms = $this->CmsRepository->getAllCms();

        return view('admin.cms.index', compact('Cms'));
    }
    
    //*************************************************//
    // * @name   : create
    // * @todo   : Add cms
    // * @Date   : 11-June-2018
    //************************************************//
    
    public function create(Request $request) {

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                        'page_title' => 'required',
                        'page_description' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect('admin/cms/create')
                            ->withErrors($validator)
                            ->withInput();
            } else {
                $Cms = Cms::create($request->all());
                if ($Cms) {
                    return redirect()->route('cms.index')
                                    ->with('success', 'Cms created successfully.');
                } else {
                    return redirect()->route('cms.index')
                                    ->with('error', 'Cms failed to create.');
                }
            }
        }
        return view('admin.cms.create');
    }
    
    //*************************************************//
    // * @name   : edit
    // * @todo   : Edit cms
    // * @Date   : 11-June-2018
    //************************************************//

    public function edit($id = NULL, Request $request) {
        
        $Cms = Cms::find($id);
        if ($request->isMethod('post')) {
            
            $validator = Validator::make($request->all(), [
                        'page_title' => 'required',
                        'page_description' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect('admin/cms/edit/'. $id)
                            ->withErrors($validator)
                            ->withInput();
            } else {
                $Cms->page_title= $request->get('page_title');
                $Cms->page_description= $request->get('page_description');
                $Cms = $Cms->save();
                if ($Cms) {
                    return redirect()->route('cms.index')
                                    ->with('success', 'Cms updated successfully.');
                } else {
                    return redirect()->route('cms.index')
                                    ->with('error', 'Cms failed to update.');
                }
            }
        }
        
        return view('admin.cms.edit',compact('Cms'));
    }
    
    //*************************************************//
    // * @name   : edit
    // * @todo   : Delete cms
    // * @Date   : 11-June-2018
    //************************************************//
    
    public function delete($id = NULL) {
        $Cms = $this->CmsRepository->deleteCms($id);
        if($Cms) {
            return redirect()->route('cms.index')->with('success', 'Cms deleted successfully !');
        } else {
            return redirect()->route('cms.index')->with('error', 'Failed to delete Cms !');
        }
    }

}
