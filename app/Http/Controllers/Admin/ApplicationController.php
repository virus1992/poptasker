<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\ApplicationRepository;
use App\Tasks;

class ApplicationController extends Controller {

    public function __construct(ApplicationRepository $ApplicationRepository) {
        $this->middleware('auth.admin');
        $this->ApplicationRepository = $ApplicationRepository;
    }

    public function index($taskid = NULL, Request $request) {
        $Data = [];
        $filterarray = [];

        $tasktitle = Tasks::select('title')->where(['id' => $taskid])->get()->first()->toArray();

        if ($request->isMethod('post')) {
            $filterarray['startdate'] = (($request->input('startdate')) && $request->input('startdate') != "") ? date('Y-m-d', strtotime($request->input('startdate'))) : "";
            $filterarray['enddate'] = (($request->input('enddate')) && $request->input('enddate') != "") ? date('Y-m-d', strtotime($request->input('enddate'))) : "";
            $applications = $this->ApplicationRepository->getApplicationsbytask($taskid, $filterarray);
            $Data['startdate'] = $request->input('startdate');
            $Data['enddate'] = $request->input('enddate');
        } else {
            $applications = $this->ApplicationRepository->getApplicationsbytask($taskid);
        }
        $Data['task_title'] = ($tasktitle['title']) ? $tasktitle['title'] : "";
        $Data['Applications'] = $applications;
        return view('admin.application.index', compact('Data'));
    }

}
