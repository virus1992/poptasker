<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\TransactionHistoryRepository;
use App\Http\Services\ResponseTrait;
use App\TransactionHistory;
use App\PayoutPaidDetails;
use DB;

//*************************************************//
//@ * @controller   : PayoutsController
//@ * @todo         : Manage All Payouts
//************************************************//

class PayoutsController extends Controller {

    use ResponseTrait;

    public function __construct(TransactionHistoryRepository $TransactionHistoryRepository) {
        $this->TransactionHistoryRepository = $TransactionHistoryRepository;
        $this->middleware('auth.admin');
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : Get Record of all payouts
    // * @Date   : 13-June-2018
    //************************************************//

    public function index(Request $request) {

        $Data = array();
        $filterarray = [];
        if ($request->isMethod('post')) {
            $filterarray['from_month'] = $from_month = (($request->input('from_month')) && $request->input('from_month') != "") ? $request->input('from_month') : "";
            $filterarray['to_month'] = $to_month = (($request->input('to_month')) && $request->input('to_month') != "") ? $request->input('to_month') : "";
            if ($from_month > $to_month) {
                return redirect()->route('payouts.index')->with("error", "To Month should be greater than From Month !");
            } else {
                $Payouts = $this->TransactionHistoryRepository->getAllpayouts($filterarray);
            }
            $Data['from_month'] = $request->input('from_month');
            $Data['to_month'] = $request->input('to_month');
        } else {
            $Payouts = $this->TransactionHistoryRepository->getAllpayouts();
        }
        $TotalPayments = $this->TransactionHistoryRepository->getTotalPayment(); //GET TOTAL PAYMENTS
        $TotalPayouts = $this->TransactionHistoryRepository->getTotalpayoutamount(); //GET TOTAL PAYMENTS
        
        $Data['Payouts'] = $Payouts;
        $Data['TotalPayments'] = $TotalPayments;
        $Data['TotalPayouts'] = $TotalPayouts;

        return view('admin.payouts.index', compact('Data'));
    }

    //*************************************************//
    // * @name   : monthlypayoutDetails
    // * @todo   : Get Record of monthly payable amount Details
    // * @Date   : 9-July-2018
    //************************************************//


    public function monthlypayoutDetails(Request $request) {

        $data = $request->all();
        $filterarray = [];
        $filterarray['userid'] = ($data['user_id']) ? $data['user_id'] : "";
        $filterarray['monthyear'] = ($data['month_year']) ? $data['month_year'] : "";

        $monthlypayoutDetails = $this->TransactionHistoryRepository->getMonthlypayoutDetails($filterarray);
        if ($monthlypayoutDetails) {
            return $this->responseJson('success', 'Record fetched successfully', 200, $monthlypayoutDetails);
        } else {
            return $this->responseJson('error', "No record found", 400, []);
        }
    }

    public function savePaidamountdetails(Request $request) {

        $data['user_id'] = $request->input('payout_user_id');
        $data['amount'] = $request->input('amount');
        $data['comment'] = $request->input('comments');
        $data['created_at'] = date('Y-m-d');
        $data['payout_month'] = ($request->input('month_year')) ? date('Y-m', strtotime($request->input('month_year'))) : "";
        $PayableAmount = $request->input('PayableAmount');

        $resultarray = [];
        $PayoutPaidDetails = PayoutPaidDetails::create($data);
        if ($PayoutPaidDetails) {
            $ToatlMonthlypaid = PayoutPaidDetails::select(DB::raw('SUM(amount) as paid_amount'), 'created_at as paid_date')->where([['payout_month', $PayoutPaidDetails['payout_month']], ['user_id', $PayoutPaidDetails['user_id']]])->get()->first();
            if (isset($ToatlMonthlypaid['paid_amount']) && $ToatlMonthlypaid['paid_amount'] == $PayableAmount) {
                DB::table('transaction_history')
                        ->where([['type', 'earned'], ['user_id', $PayoutPaidDetails['user_id']]])
                        ->whereMonth('created_at', date('m', strtotime($PayoutPaidDetails['payout_month'])))
                        ->whereYear('created_at', date('Y', strtotime($PayoutPaidDetails['payout_month'])))
                        ->update(['is_paid' => 1]);
            }
            $resultarray['ToatlpaidByMonth'] = ($ToatlMonthlypaid['paid_amount']) ? $ToatlMonthlypaid['paid_amount'] : 0;
            $resultarray['paidamount'] = $data['amount'];
            $resultarray['PayableAmount'] = $PayableAmount;
            $resultarray['is_paid'] = ($resultarray['PayableAmount'] - $resultarray['ToatlpaidByMonth'] == 0) ? 1 : 0;
            return $this->responseJson('success', "Amount Paid Successfully", 200, $resultarray);
        } else {
            return $this->responseJson('error', "Failed", 400, []);
        }
    }

    public function changepayoutstatus(Request $request) {

        $data = $request->all();
        $result = TransactionHistory::where('user_id', $data['user_id'])
                ->where(DB::raw('YEAR(created_at)'), '=', date('Y', strtotime($data['month_year'])))
                ->where(DB::raw('MONTH(created_at)'), '=', date('m', strtotime($data['month_year'])))
                ->update(['is_paid' => $data['is_paid']]);

        if ($result) {
            return $this->responseJson('success', "Status updated successfully", 200, $data);
        } else {
            return $this->responseJson('error', "Failed to update status", 400, []);
        }
    }

}
