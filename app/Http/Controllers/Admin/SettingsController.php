<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\AdminUsers;
use App\Settings;
use Validator;

class SettingsController extends Controller {

    public function __construct() {
        $this->middleware('auth.admin');
    }

    public function index(Request $request) {
        $Settings = Settings::first();
        if ($request->isMethod('post')) {
            if($request->get('Save') == "Settings") {
                if (!empty($Settings)) {
                    $Settings = Settings::find($Settings->id);
                    $Settings->contact_no = $request->get('contact_no');
                    $Settings->email = $request->get('email');
                    $Settings->commision = $request->get('commision');
                    $Settings->updated_at = date('Y-m-d H:i:s');
                    $Settings = $Settings->save();
                    if ($Settings) {
                        return redirect()->route('settings.index')->with('success', 'Settings updated successfully.');
                    } else {
                        return redirect()->route('settings.index')->with('error', 'Settings failed to update.');
                    }
                } else {
                    $Settings = Settings::create();
                    $Settings->contact_no = $request->get('contact_no');
                    $Settings->email = $request->get('email');
                    $Settings->commision = $request->get('commision');
                    $Settings->created_at = date('Y-m-d H:i:s');
                    $Settings = $Settings->save();
                    if ($Settings) {
                        return redirect()->route('settings.index')->with('success', 'Settings updated successfully.');
                    } else {
                        return redirect()->route('settings.index')->with('error', 'Failed to update settings');
                    }
                }
            } else {
                $validator = Validator::make($request->all(), [
                            'password' => 'required|min:6',
                            'confirm_password' => 'required|min:6|same:password',
                ]);
                if ($validator->fails()) { 
                    return redirect()->route('settings.index')->withErrors($validator)->withInput();
                } else {
                    $AdminUsers = AdminUsers::where('email', '=', Auth::guard('admin')->user()->email)->first();
                    $AdminUsers = AdminUsers::find($AdminUsers->id);
                    $AdminUsers->password = bcrypt($request->get('password'));
                    $AdminUsers->updated_at = date('Y-m-d H:i:s');
                    $AdminUsers = $AdminUsers->save();
                    if ($AdminUsers) {
                        return redirect()->route('settings.index')->with('success', 'Password Changed successfully.');
                    } else {
                        return redirect()->route('settings.index')->with('error', 'Failed to update password.');
                    }
                }
                
            }
            
        }

        return view('admin.settings.index', compact('Settings'));
    }

}
