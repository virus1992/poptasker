<?php

namespace App\Http\Services;

use Mail;
use App\Mail\Registration;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Login;

trait NotificationTrait {

    public function sendNotification($user = [], $task = [], $notification_type = '', $notification_message = '', $message = '') {
        $tokens = Login::getFCMToken(['user_id' => $user->id]);
        if (count($tokens) > 0) {
            foreach ($tokens as $device) {
                if (strtolower($device['device_type']) == 'ios') {
                    $data = self::payload($user, $task, $notification_type, $notification_message, $message, $device['device_type']);
                    $downstreamResponse = FCM::sendTo($device['fcm_token'], $data['option'], $data['notification'], $data['data']);
                } else {
                    $data = self::payload($user, $task, $notification_type, $notification_message, $message, $device['device_type']);
                    $downstreamResponse = FCM::sendTo($device['fcm_token'], $data['option'], null, $data['data']);
                }
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
            }
        }
    }

    public static function payload($user, $task, $notification_type, $notification_message, $message, $device_type) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder($notification_message);

        $noti_data = [
            'type' => $notification_type,
            'title' => \Config::get('constants.SITE_NAME'),
            'message' => $message,
            'body' => $message,
            'details' => [
                'user_id' => $user->id,
                'task_id' => $task->id,
            ]
        ];
        if (strtolower($device_type) == 'ios') {
            $notificationBuilder->setTitle('Poptasker')
                    ->setBody($noti_data['message'])
                    ->setSound('default');
        } else {
            $notificationBuilder->setBody('Poptasker')
                    ->setSound('default');
        }
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($noti_data);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        return ['data' => $data, 'option' => $option, 'notification' => $notification];
    }

}

//            $downstreamResponse->numberModification();
//return Array - you must remove all this tokens in your database
//            $downstreamResponse->tokensToDelete();
//return Array (key : oldToken, value : new token - you must change the token in your database )
//            $downstreamResponse->tokensToModify();
//return Array - you should try to resend the message to the tokens in the array
//            $downstreamResponse->tokensToRetry();