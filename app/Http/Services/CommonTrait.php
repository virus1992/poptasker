<?php

namespace App\Http\Services;

use App\Users;
use Auth;

trait CommonTrait {

    public function encrypt_string($thetext) {
        $uniqid = uniqid();
        $output = $uniqid . "$#$" . $thetext;
        $output = base64_encode($output);
        $output = urlencode($output);
        return $output;
    }

    public function decrypt_string($thetext) {
        $output = base64_decode(urldecode($thetext));
        $output = explode("$#$", $output);
        return $output[1];
    }

    public function generateRandomString() {
        return str_random(30);
    }

    public function generatePassword() {
        return str_random(8);
    }

    public function generateAuthToken() {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }

    public static function convertTimestamp($string) {
        $time = explode(":", $string);

        $hour = $time[0] * 60 * 60 * 1000;
        $minute = $time[1] * 60 * 1000;
        $second = $time[2] * 1000;

        $result = $hour + $minute + $second;

        return $result;
    }

}
