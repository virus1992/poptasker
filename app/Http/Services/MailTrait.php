<?php

namespace App\Http\Services;

use Mail;
use App\Mail\Registration;
use App\Mail\ResetPassword;
use App\Mail\AwardTask;
use App\Mail\CancellTask;
use App\Mail\NewApplication;
use App\Mail\TaskReminder;
use App\Mail\ApplicationSeeker;
use App\Mail\AwardedTask;
use App\Mail\TaskAdded;
use App\Mail\TaskCompletedposter;
use App\Mail\TaskCompletedseeker;

trait MailTrait {

    public function sendEmail($array_mail) {
        if ($array_mail['type'] == 'registration') {
            Mail::to($array_mail['to'])->send(new Registration($array_mail['data']));
        }
        if ($array_mail['type'] == 'reset-password') {
            Mail::to($array_mail['to'])->send(new ResetPassword($array_mail['data']));
        }
        if ($array_mail['type'] == 'award-task') {
            Mail::to($array_mail['to'])->send(new AwardTask($array_mail['data']));
        }
        if ($array_mail['type'] == 'cancel-task') {
            Mail::to($array_mail['to'])->send(new CancellTask($array_mail['data']));
        }
        if ($array_mail['type'] == 'new-application') {
            Mail::to($array_mail['to'])->send(new NewApplication($array_mail['data']));
        }
        if ($array_mail['type'] == 'task-reminder') {
            Mail::to($array_mail['to'])->send(new TaskReminder($array_mail['data']));
        }
        if ($array_mail['type'] == 'application-seeker') {
            Mail::to($array_mail['to'])->send(new ApplicationSeeker($array_mail['data']));
        }
        if ($array_mail['type'] == 'awarded-task') {
            Mail::to($array_mail['to'])->send(new AwardedTask($array_mail['data']));
        }
        if ($array_mail['type'] == 'task-added') {
            Mail::to($array_mail['to'])->send(new TaskAdded($array_mail['data']));
        }
        if ($array_mail['type'] == 'task-complete-seeker') {
            Mail::to($array_mail['to'])->send(new TaskCompletedseeker($array_mail['data']));
        }
        if ($array_mail['type'] == 'task-complete-poster') {
            Mail::to($array_mail['to'])->send(new TaskCompletedposter($array_mail['data']));
        }
    }

}
