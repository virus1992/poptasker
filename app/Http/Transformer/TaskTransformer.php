<?php

namespace App\Http\Transformer;

use URL;
use Carbon\Carbon;
use Auth;
use App\UserDetails;

class TaskTransformer {

    public function transformList($tasks) {
        $var = [];
        $var = $tasks->map(function ($item) {
            return [
                'task_id' => $item->taskid ?? '',
                'title' => $item->title ?? '',
                'description' => $item->description ?? '',
                'keywords' => $item->keywords ?? '',
                'address' => $item->address ?? '',
                'start_date' => $item->start_date ?? '',
                'end_date' => $item->end_date ?? '',
                'start_time' => $item->start_time ?? '',
                'end_time' => $item->end_time ?? '',
                'working_hours' => $item->working_hours ?? '',
                'wage' => $item->wage ?? '',
                'employer_id' => $item->employer_id ?? '',
                'employee_id' => $item->employee_id ?? '',
                'employer' => $item->employerDetails->firstname . ' ' . $item->employerDetails->lastname ?? '',
                'employee' => ($item->employee_id) ? $item->employeeDetails->firstname . ' ' . $item->employeeDetails->lastname ?? '' : "",
                'status' => $item->status ?? '',
                'posted' => ($item->employer_id == Auth::id()) ? TRUE : FALSE,
                'pictures' => (($item->pictures) ? $item->pictures : array()),
                'is_deleted' => ($item->is_deleted) ? $item->is_deleted : 0,
                'is_cancelled' => ($item->is_cancelled) ? $item->is_cancelled : 0,
            ];
        });

        return ['has_page' => $tasks->hasMorePages(), 'current_page' => $tasks->currentPage(), 'listing' => $var];
    }

    public function transformTask($item) {
        return [
            'task_id' => $item->id ?? $item->task_id,
            'title' => $item->title ?? '',
            'description' => $item->description ?? '',
            'keywords' => $item->keywords ?? '',
            'address' => $item->address ?? '',
            'start_date' => $item->start_date ?? '',
            'end_date' => $item->end_date ?? '',
            'start_time' => $item->start_time ?? '',
            'end_time' => $item->end_time ?? '',
            'working_hours' => $item->working_hours ?? '',
            'wage' => $item->wage ?? '',
            'employer_id' => $item->employer_id ?? '',
            'employee_id' => $item->employee_id ?? '',
            'employer' => $item->employerDetails->firstname . ' ' . $item->employerDetails->lastname ?? '',
            'employee' => ($item->employee_id) ? $item->employeeDetails->firstname . ' ' . $item->employeeDetails->lastname ?? '' : "",
            'status' => $item->status ?? '',
            'posted' => ($item->employer_id == Auth::id()) ? TRUE : FALSE,
            'pictures' => (($item->pictures) ? $item->pictures : array()),
            'is_applied' => $item->is_applied ?? '',
            'is_start' => $item->is_start ?? '',
            'profile_picture_employee' => ($item->employee_id) ? UserDetails::getProfilepic($item->employeeDetails->profilepic) : "",
            'created_at' => ($item->created_at) ? Carbon::parse($item->created_at)->format('Y-m-d') : '',
            'updated_at' => ($item->updated_at) ? Carbon::parse($item->updated_at)->format('Y-m-d') : '',
        ];
    }

    public function transformListNearBy($tasks, $data = '') {
        $var = [];
        $var = $tasks->map(function ($item) {
            return [
                'task_id' => $item->task_id ?? '',
                'title' => $item->title ?? '',
                'description' => $item->description ?? '',
                'keywords' => $item->keywords ?? '',
                'address' => $item->address ?? '',
                'start_date' => $item->start_date ?? '',
                'end_date' => $item->end_date ?? '',
                'start_time' => $item->start_time ?? '',
                'end_time' => $item->end_time ?? '',
                'latitude' => $item->task_lat ?? '',
                'longitude' => $item->task_long ?? '',
                'working_hours' => $item->working_hours ?? '',
                'task_lat' => $item->task_lat ?? '',
                'task_long' => $item->task_long ?? '',
                'wage' => $item->wage ?? '',
                'employer_id' => $item->employer_id ?? '',
                'employee_id' => $item->employee_id ?? '',
                'employer' => $item->employerDetails->firstname . ' ' . $item->employerDetails->lastname ?? '',
                'employee' => ($item->employee_id) ? $item->employeeDetails->firstname . ' ' . $item->employeeDetails->lastname ?? '' : "",
                'status' => $item->status ?? '',
                'posted' => ($item->employer_id == Auth::id()) ? TRUE : FALSE,
                'pictures' => (($item->pictures) ? $item->pictures : array()),
                'distance' => $item->distance ?? '',
                'is_applied' => $item->is_applied ?? ''
            ];
        });
        if (strtolower($data['page']) == 'all') {
            return $var;
        } else {
            return ['has_page' => $data['hasMorePages'], 'current_page' => $data['currentPage'], 'listing' => $var];
        }
    }

    public function transformTaskNoReview($item) {
        return [
            'task_id' => $item->id ?? '',
            'title' => $item->title ?? '',
            'description' => $item->description ?? '',
            'employer_id' => $item->employer_id ?? '',
            'employee_id' => $item->employee_id ?? '',
            'employer' => $item->employerDetails->firstname . ' ' . $item->employerDetails->lastname ?? '',
            'employee' => ($item->employee_id) ? $item->employeeDetails->firstname . ' ' . $item->employeeDetails->lastname ?? '' : "",
            'status' => $item->status ?? '',
            'empoyee_pic' => ($item->employee_id) ? UserDetails::getProfilepic($item->employeeDetails->profilepic) : "",
//            'keywords' => $item->keywords ?? '',
//            'address' => $item->address ?? '',
//            'start_date' => $item->start_date ?? '',
//            'end_date' => $item->end_date ?? '',
//            'start_time' => $item->start_time ?? '',
//            'end_time' => $item->end_time ?? '',
//            'working_hours' => $item->working_hours ?? '',
//            'wage' => $item->wage ?? '',
//            'posted' => ($item->employer_id == Auth::id()) ? TRUE : FALSE,
//            'pictures' => (($item->pictures) ? $item->pictures : array()),
//            'is_applied' => $item->is_applied ?? '',
//            'is_start' => $item->is_start ?? '',
//            'created_at' => ($item->created_at) ? Carbon::parse($item->created_at)->format('Y-m-d') : '',
//            'updated_at' => ($item->updated_at) ? Carbon::parse($item->updated_at)->format('Y-m-d') : '',
        ];
    }

}
