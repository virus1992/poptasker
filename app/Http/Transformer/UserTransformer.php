<?php

namespace App\Http\Transformer;

use URL;
use Carbon\Carbon;
use App\UserDetails;
use Auth;

class UserTransformer {

    public function transformLogin($user) {

        return [
            'user_id' => $user->id,
            'firstname' => $user->userDetails->firstname ?? '',
            'lastname' => $user->userDetails->lastname ?? '',
            'email' => $user->email ?? '',
            'phone' => $user->userDetails->phone ?? '',
            'profilepic' => (!empty($user->userDetails->profile_pic)) ? UserDetails::getProfilepic($user->userDetails->profile_pic) : "",
            'api_token' => $user->api_token ?? ''
        ];
    }

    public function transformUserDetails($user) {

        return [
            'firstname' => $user->firstname ?? '',
            'lastname' => $user->lastname ?? '',
            'phone' => $user->phone ?? '',
            'summary' => $user->summary ?? '',
            'education' => $user->education ?? '',
            'skills' => $user->skills ?? '',
            'passions' => $user->passions ?? '',
            'address' => $user->address ?? '',
            'linkedin' => $user->linkedin ?? '',
            'profilepic' => (!empty($user->profilepic)) ? UserDetails::getProfilepic($user->profilepic) : "",
        ];
    }

    public function transformFavorite($users) {
        $var = [];
        $var = $users->map(function ($user) {
            return [
                'user_id' => $user->user_id ?? '',
                'firstname' => $user->firstname ?? '',
                'lastname' => $user->lastname ?? '',
                'phone' => $user->phone ?? '',
                'summary' => $user->summary ?? '',
                'education' => $user->education ?? '',
                'skills' => $user->skills ?? '',
                'passions' => $user->passions ?? '',
                'address' => $user->address ?? '',
                'linkedin' => $user->linkedin ?? '',
                'profilepic' => (!empty($user->profilepic)) ? UserDetails::getProfilepic($user->profilepic) : "",
                'endorsements' => $user->endorsements,
                'ratings' => ($user->ratings > 0) ? ($user->ratings / $user->totalratings) : 0,
                'completedtasks' => ($user->completedjobs) ? $user->completedjobs : 0,
                'is_favorite' => $user->is_favorite ?? ''
            ];
        });

        return ['has_page' => $users->hasMorePages(), 'current_page' => $users->currentPage(), 'listing' => $var];
    }

    public function transformUserMessages($users) {
        $var = [];
        $var = $users->map(function ($user) {
            return [
                'task_id' => $user->task_id ?? '',
                'title' => $user->title ?? '',
                'from_id' => $user->from_id ?? '',
                'to_id' => $user->to_id ?? '',
                'conversation_id' => $user->conversation_id ?? '',
                'address' => $user->address,
                'picture' => ($user->to_id == Auth::id()) ? UserDetails::getProfilepicFromId($user->from_id) : UserDetails::getProfilepicFromId($user->to_id),
            ];
        });

        return ['has_page' => $users->hasMorePages(), 'current_page' => $users->currentPage(), 'listing' => $var];
    }

    public function transformHistory($tasks) {
        $var = [];
        $var = $tasks->map(function ($item) {
            return [
                'task_id' => $item->id ?? '',
                'title' => $item->title ?? '',
                'description' => $item->description ?? '',
                'address' => $item->address ?? '',
                'start_date' => Carbon::parse($item->start_date)->format('Y-m-d') ?? '',
                'end_date' => Carbon::parse($item->end_date)->format('Y-m-d') ?? '',
                'start_time' => Carbon::parse($item->start_time)->format('h:i a') ?? '',
                'end_time' => Carbon::parse($item->end_time)->format('h:i a') ?? '',
                'working_hours' => $item->working_hours ?? '',
                'wage' => $item->amount ?? '',
                'type' => $item->type ?? '',
                'is_paid' => $item->is_paid ?? '',
            ];
        });

        return ['has_page' => $tasks->hasMorePages(), 'current_page' => $tasks->currentPage(), 'listing' => $var];
    }

    public function transformCards($cards) {
        $var = [];
        $var = $cards->map(function ($item) {
            return [
                'card_id' => $item->id ?? '',
                'user_id' => $item->user_id ?? '',
                'card_no' => $item->card_id ?? '',
                'last_four_digit' => $item->last_four_digit ?? '',
                'card_type' => $item->card_type ?? '',
                'card_holder_name' => $item->card_holder_name ?? '',
            ];
        });
        return $var;
    }

}
