<?php

namespace App\Http\Transformer;

use URL;
use Carbon\Carbon;
use Auth;
use App\Tasks;
use App\UserDetails;

class ApplicationTransformer {

    public function transformList($tasks) {
        $var = [];
        $var = $tasks->map(function ($item) {
            return [
                'task_id' => $item->task_id ?? '',
                'userid' => $item->user_id ?? '',
                'firstname' => $item->firstname ?? '',
                'lastname' => $item->lastname ?? '',
                'summary' => $item->summary ?? '',
                'address' => $item->address ?? '',
                'education' => $item->education ?? '',
                'skills' => $item->skills ?? '',
                'passions' => $item->passions ?? '',
                'phone' => $item->phone ?? '',
                'endorsements' => $item->endorsements,
                'ratings' => ($item->ratings > 0) ? ($item->ratings / $item->totalratings) : 0,
                'completedtasks' => ($item->completedjobs) ? $item->completedjobs : 0,
                'linkedin' => $item->linkedin ?? '',
                'is_favorite' => $item->is_favorite ?? '',
                'profilepic' => ($item->profilepic) ? UserDetails::getProfilepic($item->profilepic) : '',
                'applicationdate' => Carbon::parse($item['applicationdate'])->format('Y-m-d H:i')
            ];
        });

        return ['has_page' => $tasks->hasMorePages(), 'current_page' => $tasks->currentPage(), 'listing' => $var];
    }

    public function transformUser($item) {
        return [
            'user_id' => $item->user_id ?? '',
            'firstname' => $item->firstname ?? '',
            'lastname' => $item->lastname ?? '',
            'summary' => $item->summary ?? '',
            'address' => $item->address ?? '',
            'education' => $item->education ?? '',
            'skills' => $item->skills ?? '',
            'passions' => $item->passions ?? '',
            'phone' => $item->phone ?? '',
            'endorsements' => $item->endorsements ?? 0,
            'ratings' => ($item->ratings) ? ($item->ratings / $item->totalratings) : 0,
            'completedtasks' => ($item->completedjobs) ? $item->completedjobs : 0,
            'linkedin' => $item->linkedin ?? '',
            'profilepic' => ($item->profilepic) ? UserDetails::getProfilepic($item->profilepic) : '',
            'applicationdate' => Carbon::parse($item->applicationdate)->format('Y-m-d H:i'),
//            'reviews' => (is_array($item->reviews) && count($item->reviews) > 0) ? $item->reviews : [],
        ];
    }

    public function transformListApplication($tasks) {
        $var = [];
        $var = $tasks->map(function ($item) {
            return [
                'task_id' => $item->id ?? '',
                'title' => $item->title ?? '',
                'totalapplications' => $item->total_application ?? 0,
                'is_reviewed' => $item->is_reviewed ?? '',
                'status' => $item->status ?? ''
            ];
        });

        return ['has_page' => $tasks->hasMorePages(), 'current_page' => $tasks->currentPage(), 'listing' => $var];
    }

    public function transformUserReviews($tasks) {
        $var = [];
        $var = $tasks->map(function ($item) {
            return [
                'task_id' => $item->task_id ?? '',
                'employer_id' => $item->employer_id ?? '',
                'firstname' => $item->firstname ?? '',
                'lastname' => $item->lastname ?? '',
                'comments' => $item->comments ?? '',
                'rating' => $item->rating ?? 0,
                'review_date' => Carbon::parse($item->updated_at)->format('Y-m-d H:i'),
                'profilepic' => ($item->profilepic) ? UserDetails::getProfilepic($item->profilepic) : '',
            ];
        });

        return ['has_page' => $tasks->hasMorePages(), 'current_page' => $tasks->currentPage(), 'listing' => $var];
    }

}
