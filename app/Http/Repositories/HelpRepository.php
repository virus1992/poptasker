<?php

namespace App\Http\Repositories;

use App\Help;

class HelpRepository {

    //*************************************************//
    // * @name   : getAllUsers
    // * @todo   : Get list of all users
    // * @Date   : 13-June-2018
    //************************************************//

    public function getAllHelps() {
        $Helps = Help::select('help.id', 'help.email', 'help.description', 'help.is_resolved', 'user_details.firstname', 'user_details.lastname')
                        ->leftJoin('user_details', 'help.user_id', '=', 'user_details.user_id')
                        ->orderBy('help.id', 'DESC')
                        ->groupBy('help.id')
                        ->get()->toArray();
        return $Helps;
    }

    //*************************************************//
    // * @name   : changehelpstatus
    // * @todo   : Change status of help
    // * @Date   : 13-June-2018
    //************************************************//

    public function changehelpstatus($data = array()) {

        $is_resolved = (isset($data['is_resolved'])) ? $data['is_resolved'] : 0;
        if ($data['helpid'] != "") {
            $Help = Help::where(['id' => $data['helpid']])->first();
            $Help->is_resolved = $data['is_resolved'];
            $Help = $Help->save();
            return $Help;
        } else {
            return false;
        }
    }

}
