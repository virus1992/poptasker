<?php

namespace App\Http\Repositories;

use App\Users;
use App\Tasks;
use App\UserDetails;
use DB;
use App\Application;
use App\Reviews;

class UsersRepository {

    //*************************************************//
    // * @name   : getAllUsers
    // * @todo   : Get list of all users
    // * @Date   : 4-June-2018
    //************************************************//

    public function getAllUsers() {
        $Users = Users::select('users.id as id', 'users.email as users_email', 'users.is_deleted as is_deleted', 'users.is_confirm as is_confirm', 'users.is_active', 'user_details.id as user_details_id', 'user_details.firstname as firstname', 'user_details.lastname as lastname', 'user_details.phone as phone', 'user_details.profilepic as profilepic')
                        ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')->orderBy('users.id', 'DESC')
                        ->get()->toArray();

        return $Users;
    }

    //*************************************************//
    // * @name   : getAlluserscount
    // * @todo   : Get Total count of all users
    // * @Date   : 4-June-2018
    //************************************************//

    public function getAlluserscount() {
        $Userscount = Users::select('users.id')->get()->count();
        return $Userscount;
    }

    //*************************************************//
    // * @name   : deleteUser
    // * @todo   : Delete Users 
    // * @Date   : 4-June-2018
    //************************************************//

    public function deleteUser($id = NULL) {

        $User = Users::where(['id' => $id])->first();
        $User->is_deleted = 1;
        $User = $User->save();
        return $User;
    }

    //*************************************************//
    // * @name   : changeuserstatus
    // * @todo   : Change status of users
    // * @Date   : 26-June-2018
    //************************************************//

    public function changeuserstatus($data = array()) {

        $is_confirm = (isset($data['is_confirm'])) ? $data['is_confirm'] : 0;
        if ($data['userid'] != "") {
            $User = Users::where(['id' => $data['userid']])->first();
            $User->is_confirm = $data['is_confirm'];
            $User = $User->save();
            return $User;
        } else {
            return false;
        }
    }

    //*************************************************//
    // * @name   : getInactiveusersCount
    // * @todo   : GET count of Inactive Users
    // * @Date   : 26-June-2018
    //************************************************//


    public function getInactiveusers() {
        $InactiveUsers = Users::select('users.id as id', 'users.email as users_email', 'users.is_deleted as is_deleted', 'users.is_confirm as is_confirm', 'users.is_active', 'user_details.id as user_details_id', 'user_details.firstname as firstname', 'user_details.lastname as lastname', 'user_details.phone as phone', 'user_details.profilepic as profilepic')
                        ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                        ->leftJoin('tasks', 'tasks.employer_id', '=', 'users.id')
                        ->leftJoin('applications', 'applications.user_id', '=', 'users.id')
                        ->where([['tasks.employer_id', "=", NULL], ['applications.user_id', "=", NULL]])
                        ->groupBy('users.id')
                        ->get()->toArray();
        return $InactiveUsers;
    }

    //*************************************************//
    // * @name   : getInactiveusersCount
    // * @todo   : GET count of Inactive Users
    // * @Date   : 26-June-2018
    //************************************************//


    public function getInactiveusersCount() {
        $InactiveUsers = Users::select('applications.user_id', DB::raw("count(DISTINCT(users.id)) as count"))->leftJoin('tasks', 'tasks.employer_id', '=', 'users.id')->leftJoin('applications', 'applications.user_id', '=', 'users.id')->where([['tasks.employer_id', "=", NULL], ['applications.user_id', "=", NULL]])->get()->first()->toArray();

        return $InactiveUsers;
    }

    //*************************************************//
    // * @name   : getJobPoster
    // * @todo   : get Record of Job Posters
    // * @Date   : 26-June-2018
    //************************************************//


    public function getJobPoster() {
        $TaskPosters = Users::select('tasks.employer_id', 'users.id as id', 'users.email as users_email', 'users.is_deleted as is_deleted', 'users.is_confirm as is_confirm', 'users.is_active', 'user_details.id as user_details_id', 'user_details.firstname as firstname', 'user_details.lastname as lastname', 'user_details.phone as phone', 'user_details.profilepic as profilepic')
                        ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                        ->leftJoin('tasks', 'users.id', '=', 'tasks.employer_id')->where('tasks.employer_id', '<>', NULL)
                        ->groupBy('users.id')->get()->toArray();
        return $TaskPosters;
    }

    //*************************************************//
    // * @name   : getJobSeeker
    // * @todo   : get Record of Job Seekers
    // * @Date   : 26-June-2018
    //************************************************//

    public function getJobSeeker() {
        $posters = Tasks::select('employer_id')->groupBy('employer_id')->get()->toArray();
        $applications = Application::select('user_id')->groupBy('user_id')->get()->toArray();


        $users = [];
        $postersusers = [];
        foreach ($applications as $user) {
            $users[] = $user['user_id'];
        }
        foreach ($posters as $user) {
            $postersusers[] = $user['employer_id'];
        }
        $JobSeeker = Users::select('tasks.employee_id', 'users.id as id', 'users.email as users_email', 'users.is_deleted as is_deleted', 'users.is_confirm as is_confirm', 'users.is_active', 'user_details.id as user_details_id', 'user_details.firstname as firstname', 'user_details.lastname as lastname', 'user_details.phone as phone', 'user_details.profilepic as profilepic')
                        ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                        ->leftJoin('tasks', 'users.id', '=', 'tasks.employee_id')->whereIn('users.id', $users)->whereNotIn('users.id', $postersusers)
                        ->groupBy('users.id')->get()->toArray();

        return $JobSeeker;
    }

    //*************************************************//
    // * @name   : getJobSeeker
    // * @todo   : get Record of Job Seekers
    // * @Date   : 26-June-2018
    //************************************************//


    public function getuserdetailsbyid($id = NULl) {
        $Users = Users::select('*')
                        ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                        ->where('users.id', $id)
                        ->get()->first();
        return $Users;
    }

    public function getuserReviews($filter = array()) {
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        $reviews = Reviews::select('user_details.user_id as userid','tasks.title as tasktitle','user_details.firstname', 'user_details.lastname', 'user_details.profilepic as commenterpic','task_pictures.picture as taskpicture' ,'reviews.*')
                        ->leftJoin('tasks', 'tasks.id', '=', 'reviews.task_id')
                        ->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'reviews.task_id')
                        ->join('user_details', 'reviews.employer_id', '=', 'user_details.user_id')
                        ->groupBy('reviews.review_id')
                        ->where(['reviews.employee_id' => $userid])->get()->toArray();
        return $reviews;
    }

}
