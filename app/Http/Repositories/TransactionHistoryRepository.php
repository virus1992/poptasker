<?php

namespace App\Http\Repositories;

use App\TransactionHistory;
use App\Users;
use App\PayoutPaidDetails;
use DB;

class TransactionHistoryRepository {

    //*************************************************//
    // * @name   : getAllTransactions
    // * @todo   : Get list of all Transactions
    // * @Date   : 8-June-2018
    //************************************************//

    public function getAllTransactions($filter = []) {
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $task_status = (isset($filter['tast_status'])) ? $filter['tast_status'] : "";
        $userid = (isset($filter['userid'])) ? $filter['userid'] : "";
        
        $Query = TransactionHistory::select('tasks.status', 'transaction_history.id', 'tasks.title as task_title', 'tasks.description as task_description', 'user_details.firstname', 'user_details.lastname', 'transaction_history.type as trans_type', 'transaction_history.amount', 'tasks.status as task_status', 'transaction_history.refund', 'transaction_history.refund_amount', 'refund_reason', 'user_details.phone', 'users.email')
                ->leftJoin('tasks', 'tasks.id', '=', 'transaction_history.task_id')
                ->leftJoin('users', 'users.id', '=', 'transaction_history.user_id')
                ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                ->groupBy('transaction_history.id');

        if($userid != "") {
            $Query = $Query->where('transaction_history.user_id', $userid);
        }
        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('transaction_history.created_at', '>=', $startdate)->where('transaction_history.created_at', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('transaction_history.created_at', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('transaction_history.created_at', '<=', $enddate);
        } else {
            $Query = $Query;
        }
        if ($task_status != "") {
            $Query = $Query->where('tasks.status', $task_status);
        }
        $Transactions = $Query->get()->toArray();
        return $Transactions;
    }

    //*************************************************//
    // * @name   : getTotalPayment
    // * @todo   : Get total amount of payments
    // * @Date   : 12-June-2018
    //************************************************//

    public function getTotalPayment() {

        $Query1 = TransactionHistory::select(DB::raw('SUM(transaction_history.amount) as total_amount'))
                ->leftJoin('tasks', 'tasks.id', '=', 'transaction_history.task_id')
                ->where('transaction_history.type', 'spend');

        $Payments = $Query1->get()->first()->toArray();

        $Query2 = TransactionHistory::select(DB::raw('SUM(transaction_history.refund_amount) as refunded_amount'))
                ->leftJoin('tasks', 'tasks.id', '=', 'transaction_history.task_id')
                ->where('transaction_history.type', 'spend');

        $Refundamount = $Query2->get()->first()->toArray();

        $Payments = $Payments['total_amount'] - $Refundamount['refunded_amount'];
        return $Payments;
    }

    //*************************************************//
    // * @name   : getTotalpayoutamount
    // * @todo   : Get total amount of payout
    // * @Date   : 12-June-2018
    //************************************************//

    public function getTotalpayoutamount() {
        $Payout = TransactionHistory::select(DB::raw('SUM(transaction_history.amount) as total_amount'))
                        ->leftJoin('tasks', 'tasks.id', '=', 'transaction_history.task_id')
                        ->where([['transaction_history.type', "earned"], ['tasks.status', 'completed']])->get()->first()->toArray();
        return $Payout;
    }

    //*************************************************//
    // * @name   : getAllpayouts
    // * @todo   : Get Record of all payouts
    // * @Date   : 12-June-2018
    //************************************************//

    public function getAllpayouts($filter = []) {
        $from_month = (isset($filter['from_month'])) ? $filter['from_month'] : "";
        $to_month = (isset($filter['to_month'])) ? $filter['to_month'] : "";

        $Query = TransactionHistory::select('transaction_history.id', 'transaction_history.user_id', DB::raw('SUM(transaction_history.amount) as total_amount'), 'user_details.firstname', 'user_details.lastname', 'transaction_history.type', 'transaction_history.is_paid', 'transaction_history.created_at')
                ->groupBy(DB::raw('YEAR(transaction_history.created_at)'))
                ->groupBy(DB::raw('MONTH(transaction_history.created_at)'))
                ->groupBy('transaction_history.user_id')
                ->leftJoin('tasks', 'tasks.id', '=', 'transaction_history.task_id')
                ->leftJoin('users', 'users.id', '=', 'transaction_history.user_id')
                ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                ->where([['transaction_history.type', 'earned'], ['tasks.status', 'completed']]);

        if ($from_month != "" && $to_month != "") {
            $Query = $Query->whereMonth('transaction_history.created_at', '>=', $from_month)->whereMonth('transaction_history.created_at', '<=', $to_month);
        } elseif ($from_month != "" && $to_month == "") {
            $Query = $Query->whereMonth('transaction_history.created_at', '>=', $from_month);
        } elseif ($from_month == "" && $to_month != "") {
            $Query = $Query->whereMonth('transaction_history.created_at', '<=', $to_month);
        }

        $Payouts = $Query->get()->toArray();
        if (!empty($Payouts)) {
            foreach ($Payouts as $key => $value) :
                $PaidMonthDetails = PayoutPaidDetails::select(DB::raw('SUM(amount) as paid_amount'), 'created_at as paid_date')->where([['payout_month', date('Y-m', strtotime($value['created_at']))], ['user_id', $value['user_id']]])->get()->first();
                $Query2 = TransactionHistory::select(DB::raw('SUM(amount) as total_amount'))
                        ->where([['type', 'earned'], ['user_id', $value['user_id']], ['is_paid', '1']]);
                if ($from_month != "" && $to_month != "") {
                    $Query2 = $Query2->whereMonth('created_at', '>=', $from_month)->whereMonth('created_at', '<=', $to_month);
                } elseif ($from_month != "" && $to_month == "") {
                    $Query2 = $Query2->whereMonth('created_at', '>=', $from_month);
                } elseif ($from_month == "" && $to_month != "") {
                    $Query2 = $Query2->whereMonth('created_at', '<=', $to_month);
                }
                $Payout_tilltoday = $Query2->get()->first()->toArray();
                $Payouts[$key]['Payout_tilltoday'] = $Payout_tilltoday;
                $Payouts[$key]['PaidthisMonth'] = ($PaidMonthDetails['paid_amount'] != "") ? $PaidMonthDetails['paid_amount'] : 0;
            endforeach;
        }

        return $Payouts;
    }

    public function getMonthlypayoutDetails($filter = []) {
        $userid = (isset($filter['userid'])) ? $filter['userid'] : "";
        $month = (isset($filter['monthyear'])) ? date('m', strtotime($filter['monthyear'])) : "";
        $year = (isset($filter['monthyear'])) ? date('Y', strtotime($filter['monthyear'])) : "";

        $result = array();
        $PayoutDetails = TransactionHistory::select('transaction_history.id', 'transaction_history.user_id', DB::raw('SUM(transaction_history.amount) as total_amount'), 'user_details.firstname', 'user_details.lastname', 'transaction_history.type', 'transaction_history.created_at')
                        ->groupBy(DB::raw('YEAR(transaction_history.created_at)'))
                        ->groupBy(DB::raw('MONTH(transaction_history.created_at)'))
                        ->groupBy('transaction_history.user_id')
                        ->leftJoin('tasks', 'tasks.id', '=', 'transaction_history.task_id')
                        ->leftJoin('users', 'users.id', '=', 'transaction_history.user_id')
                        ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                        ->where([['transaction_history.type', 'earned'], ['tasks.status', 'completed'], ['transaction_history.user_id', $userid]])
                        ->whereMonth('transaction_history.created_at', $month)
                        ->whereYear('transaction_history.created_at', $year)
                        ->get()->first();

        if ($PayoutDetails) {
            $result['Name'] = $PayoutDetails['firstname'] . " " . $PayoutDetails['lastname'];
            $result['Payableamount'] = (isset($PayoutDetails['total_amount']) && $PayoutDetails['total_amount'] != "") ? $PayoutDetails['total_amount'] : 0;
            $result['Monthyear'] = date('M Y', strtotime($PayoutDetails['created_at']));
            $result['user_id'] = $PayoutDetails['user_id'];
            $PaidamountDetails = PayoutPaidDetails::select(DB::raw('SUM(amount) as paid_amount'), 'created_at as paid_date')->where([['payout_month', date('Y-m', strtotime($filter['monthyear']))], ['user_id', $userid]])->get()->first();
            if ($PaidamountDetails) {
                $result['paid_amount'] = $PaidamountDetails['paid_amount'];
            } else {
                $result['paid_amount'] = 0;
            }
            $result['unpaid_balance'] = $result['Payableamount'] - $result['paid_amount'];
        }

        return $result;
    }

    public function savePaidamountdetails() {
        
    }

}
