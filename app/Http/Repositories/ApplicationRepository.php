<?php

namespace App\Http\Repositories;

use App\Application;
use DB;

class ApplicationRepository {

    //*************************************************//
    // * @name   : getApplicationsbytask
    // * @todo   : Get list of applications of task
    // * @Date   : 4-July-2018
    //************************************************//
    
    public function getApplicationsbytask ($taskid = NULL, $filter = array()) {

        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        
        $Query =  Application::select('user_details.firstname', 'user_details.lastname', 'user_details.education', 'user_details.phone','user_details.address' ,'users.email', 'applications.*')
                        ->where(['applications.task_id' => $taskid])
                        ->join('user_details', 'applications.user_id', '=', 'user_details.user_id')
                        ->join('users', 'applications.user_id', '=', 'users.id');
        
        if ($startdate != "" && $enddate != "") {
            $Query = $Query->whereDate('applications.created_at', '>=', $startdate)->whereDate('applications.created_at', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->whereDate('applications.created_at', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->whereDate('applications.created_at', '<=', $enddate);
        } else {
            $Query = $Query;
        }
        
        $application = $Query->get()->toArray();
        
        return $application;
    }

}
