<?php

namespace App\Http\Repositories;

use App\Cms;

class CmsRepository {

    //*************************************************//
    // * @name   : getAllCms
    // * @todo   : Get list of all Cms
    // * @Date   : 11-June-2018
    //************************************************//

    public function getAllCms() {
        $Cms = Cms::select('id','page_title', 'page_description', 'is_deleted')->get()->toArray();
        return $Cms;
    }

    //*************************************************//
    // * @name   : deleteCms
    // * @todo   : Delete Cms
    // * @Date   : 11-June-2018
    //************************************************//

    public function deleteCms($id = NULL) {
        
        $Cms = Cms::where(['id' => $id])->first();
        $Cms->is_deleted = 1;
        $Cms = $Cms->save();
        return $Cms;
    }
    
    public function getcmsrecord($keword = NULL) {
        $Cms = Cms::select('id','page_title', 'page_description')->where('keyword', $keword)->get()->first();
        $Cms = (!empty($Cms)) ? $Cms->toArray() : [] ;
        return $Cms;
    }

}
