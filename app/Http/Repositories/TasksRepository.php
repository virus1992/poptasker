<?php

namespace App\Http\Repositories;

use App\Tasks;
use DB;

class TasksRepository {

    //*************************************************//
    // * @name   : getAlltaskscount
    // * @todo   : Get Total count of all tasks
    // * @Date   : 4-June-2018
    //************************************************//

    public function getAlltaskscount($filter = array()) {
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";

        $Query = Tasks::select('*');
        if ($userid != "") {
            $Query = $Query->where('employer_id', $userid);
        }

        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('start_date', '>=', $startdate)->where('end_date', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('start_date', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('end_date', '<=', $enddate);
        } else {
            $Query = $Query;
        }
        $Taskscount = $Query->get()->count();
        return $Taskscount;
    }

    //*************************************************//
    // * @name   : getAlltasksBystatus
    // * @todo   : Get Data of all tasks by status
    // * @Date   : 6-June-2018
    //************************************************//

    public function getAlltasksBystatus($filter = array()) {
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $status = (isset($filter['task_status'])) ? $filter['task_status'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        
        if ($status == "") {
            $Query = Tasks::select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.address', 'tasks.wage', 'task_pictures.picture', 'tasks.is_deleted', 'tasks.start_date', 'tasks.start_time', 'tasks.end_date', 'tasks.end_time')->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'tasks.id')->groupBy('tasks.id')->orderBy('tasks.id', 'DESC');
        } else {
            $Query = Tasks::select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.address', 'tasks.wage', 'task_pictures.picture', 'tasks.is_deleted', 'tasks.start_date', 'tasks.start_time', 'tasks.end_date', 'tasks.end_time')->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'tasks.id')->groupBy('tasks.id')->where([['tasks.status', $status], ['tasks.no_show', 0], ['is_deleted', 0]])->orderBy('tasks.id', 'DESC');
        }
        if ($userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        if ($status == 'pending') {
            $Query = $Query->where('tasks.end_date', '>=', date('Y-m-d'));
        }
        
        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('tasks.end_date', '<=', $enddate);
        }
        $Tasks = $Query->get()->toArray();
        return $Tasks;
    }

    //*************************************************//
    // * @name   : getTaskCountbystatus
    // * @todo   : Get count of all tasks by status
    // * @Date   : 6-June-2018
    //************************************************//

    public function getTaskCountbystatus($filter = array()) {

        $status = (isset($filter['task_status'])) ? $filter['task_status'] : "";
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        if ($status == "") {
            $Query = Tasks::select(DB::raw("count(DISTINCT(tasks.id)) as count"), DB::raw("count(applications.application_id) as TotalApplication"))->leftJoin('applications', 'applications.task_id', '=', 'tasks.id');
        } else {
            $Query = Tasks::select(DB::raw("count(DISTINCT(tasks.id)) as count"), DB::raw("count(applications.application_id) as TotalApplication"))->leftJoin('applications', 'applications.task_id', '=', 'tasks.id')->where([['status', $status], ['tasks.no_show', 0], ['is_deleted', 0]]);
        }
        if ($userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        if ($status == 'pending') {
            $Query = $Query->where('tasks.end_date', '>=', date('Y-m-d'));
        }
        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('tasks.end_date', '<=', $enddate);
        } else {
            $Query = $Query;
        }
        $TaskCount = $Query->get()->first()->toArray();
        return $TaskCount;
    }

    //*************************************************//
    // * @name   : getArchievedTasks
    // * @todo   : Get Data of all tasks
    // * @Date   : 6-June-2018
    //************************************************//

    public function getArchievedTasks($filterarray = array()) {
        $startdate = (isset($filterarray['startdate'])) ? $filterarray['startdate'] : "";
        $enddate = (isset($filterarray['enddate'])) ? $filterarray['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        
        $Query = Tasks::select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.address', 'tasks.wage', 'task_pictures.picture', 'tasks.is_deleted', 'tasks.start_date', 'tasks.start_time', 'tasks.end_date', 'tasks.end_time')->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'tasks.id')->groupBy('tasks.id')->where('tasks.end_date', '<', date('Y-m-d'))->where([['tasks.status', 'pending'], ['tasks.no_show', 0], ['is_deleted', 0]])->orderBy('tasks.id', 'DESC');

        if (isset($userid) && $userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        if ($startdate != "" && $enddate != "") {
            $Tasks = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate)->get()->toArray();
        } elseif ($startdate != "" && $enddate == "") {
            $Tasks = $Query->where('tasks.start_date', '>=', $startdate)->get()->toArray();
        } elseif ($startdate == "" && $enddate != "") {
            $Tasks = $Query->where('tasks.end_date', '<=', $enddate)->get()->toArray();
        } else {
            $Tasks = $Query->get()->toArray();
        }
        return $Tasks;
    }

    //*************************************************//
    // * @name   : getArchievedTaskscount
    // * @todo   : Get count of Archieved Tasks
    // * @Date   : 6-June-2018
    //************************************************//

    public function getArchievedTaskscount($filter = array()) {
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";

        $Query = Tasks::select(DB::raw("count(DISTINCT(tasks.id)) as count"), DB::raw("count(applications.application_id) as TotalApplication"))->leftJoin('applications', 'applications.task_id', '=', 'tasks.id')->where('tasks.end_date', '<', date('Y-m-d'))->where([['tasks.status', 'pending'], ['tasks.no_show', 0], ['is_deleted', 0]]);
        if (isset($userid) && $userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }

        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('tasks.end_date', '<=', $enddate);
        } else {
            $Query = $Query;
        }

        $TaskCount = $Query->get()->first()->toArray();
        return $TaskCount;
    }

    //*************************************************//
    // * @name   : getnoshowTasks
    // * @todo   : Get Tasks with no show status
    // * @Date   : 27-June-2018
    //************************************************//

    public function getnoshowTasks($filterarray = array()) {
        $startdate = (isset($filterarray['startdate'])) ? $filterarray['startdate'] : "";
        $enddate = (isset($filterarray['enddate'])) ? $filterarray['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        
        $Query = Tasks::select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.address', 'tasks.wage', 'task_pictures.picture', 'tasks.is_deleted', 'tasks.start_date', 'tasks.start_time', 'tasks.end_date', 'tasks.end_time')->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'tasks.id')->groupBy('tasks.id')->where([['tasks.no_show', 1], ['tasks.is_deleted', 0]])->orderBy('tasks.id', 'DESC');

        if (isset($userid) && $userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        
        if ($startdate != "" && $enddate != "") {
            $Tasks = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate)->get()->toArray();
        } elseif ($startdate != "" && $enddate == "") {
            $Tasks = $Query->where('tasks.start_date', '>=', $startdate)->get()->toArray();
        } elseif ($startdate == "" && $enddate != "") {
            $Tasks = $Query->where('tasks.end_date', '<=', $enddate)->get()->toArray();
        } else {
            $Tasks = $Query->get()->toArray();
        }
        return $Tasks;
    }

    //*************************************************//
    // * @name   : getnoshowTaskscount
    // * @todo   : Get Tasks count with no show status
    // * @Date   : 27-June-2018
    //************************************************//

    public function getnoshowTaskscount($filter = array()) {
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";

        $Query = Tasks::select(DB::raw("count(DISTINCT(tasks.id)) as count"), DB::raw("count(applications.application_id) as TotalApplication"))->leftJoin('applications', 'applications.task_id', '=', 'tasks.id')->where([['tasks.no_show', 1], ['tasks.is_deleted', 0]]);

        if ($userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('tasks.end_date', '<=', $enddate);
        } else {
            $Query = $Query;
        }

        $TaskCount = $Query->get()->first()->toArray();
        return $TaskCount;
    }

    //*************************************************//
    // * @name   : getdeletedTasks
    // * @todo   : Get Deleted Tasks
    // * @Date   : 28-June-2018
    //************************************************//

    public function getdeletedTasks($filterarray = array()) {

        $startdate = (isset($filterarray['startdate'])) ? $filterarray['startdate'] : "";
        $enddate = (isset($filterarray['enddate'])) ? $filterarray['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        
        $Query = Tasks::select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.address', 'tasks.wage', 'task_pictures.picture', 'tasks.is_deleted', 'tasks.start_date', 'tasks.start_time', 'tasks.end_date', 'tasks.end_time')->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'tasks.id')->where('tasks.is_deleted', 1)->groupBy('tasks.id')->orderBy('tasks.id', 'DESC');

        if ($userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        if ($startdate != "" && $enddate != "") {
            $Tasks = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate)->get()->toArray();
        } elseif ($startdate != "" && $enddate == "") {
            $Tasks = $Query->where('tasks.start_date', '>=', $startdate)->get()->toArray();
        } elseif ($startdate == "" && $enddate != "") {
            $Tasks = $Query->where('tasks.end_date', '<=', $enddate)->get()->toArray();
        } else {
            $Tasks = $Query->get()->toArray();
        }
        return $Tasks;
    }

    //*************************************************//
    // * @name   : getdeletedTaskscount
    // * @todo   : Get count of Deleted Tasks
    // * @Date   : 27-June-2018
    //************************************************//

    public function getdeletedTaskscount($filter = array()) {
        $startdate = (isset($filter['startdate'])) ? $filter['startdate'] : "";
        $enddate = (isset($filter['enddate'])) ? $filter['enddate'] : "";
        $userid = (isset($filter['user_id'])) ? $filter['user_id'] : "";
        $Query = Tasks::select(DB::raw("count(DISTINCT(tasks.id)) as count"), DB::raw("count(applications.application_id) as TotalApplication"))->leftJoin('applications', 'applications.task_id', '=', 'tasks.id')->where('tasks.is_deleted', 1);

        if ($userid != "") {
            $Query = $Query->where('tasks.employer_id', $userid);
        }
        if ($startdate != "" && $enddate != "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate)->where('tasks.end_date', '<=', $enddate);
        } elseif ($startdate != "" && $enddate == "") {
            $Query = $Query->where('tasks.start_date', '>=', $startdate);
        } elseif ($startdate == "" && $enddate != "") {
            $Query = $Query->where('tasks.end_date', '<=', $enddate);
        } else {
            $Query = $Query;
        }

        $TaskCount = $Query->get()->first()->toArray();
        return $TaskCount;
    }

    //*************************************************//
    // * @name   : deleteTask
    // * @todo   : Delete Task 
    // * @Date   : 6-June-2018
    //************************************************//

    public function deleteTask($id = NULL) {

        $Tasks = Tasks::where(['id' => $id])->first();
        $Tasks->is_deleted = 1;
        $Tasks = $Tasks->save();
        return $Tasks;
    }

    public function getTasksReviews($filterarray) {
        $userid = (isset($filterarray['user_id'])) ? $filterarray['user_id'] : "";
        $Query = Tasks::select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.address', 'tasks.wage', 'task_pictures.picture', 'tasks.is_deleted', 'tasks.start_date', 'tasks.start_time', 'tasks.end_date', 'tasks.end_time')
                ->leftJoin('task_pictures', 'task_pictures.task_id', '=', 'tasks.id')
                ->groupBy('tasks.id')
                ->orderBy('tasks.id', 'DESC');
    }

}
