<?php

namespace App\Http\Middleware;

use Closure;
use App\Users;
use App\Http\Services\ResponseTrait;
use App\Http\Services\CommonTrait;

class ApiAuth {

    use ResponseTrait;
    use CommonTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
//       $this->checkAuth($request);
        $response = $next($request);
        if ($response->headers->get('content-type') == 'application/json') {
            return $this->responseJson('error', \Config::get('constants.NOT_AUTHORIZED'), 400);
        }
    }

}
