<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;
use Auth;

class UserDetails extends Model {

    public $table = 'user_details';
    protected $fillable = [
        'firstname', 'lastname', 'user_id', 'education', 'summary', 'skills', 'passions', 'phone', 'address',
        'profilepic', 'linkedin', 'created_at', 'updated_at'
    ];

    public static function addUserdetails($userid, $data) {
        $user_details = UserDetails::create([
                    'user_id' => $userid,
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname']
        ]);
        return $user_details;
    }

    public static function getProfilepic($value) {
        return (!empty($value) && (file_exists(public_path() . '/../' . \Config::get('constants.IMAGE_PATH') . '/profilepic/' . $value))) ? URL::to('/storage/app/public/profilepic') . '/' . $value : "";
    }

    public static function editUserdetails($userid, $data) {
        $user_details = UserDetails::where(['user_id' => $userid])->first();
        $user_details->firstname = $data['firstname'];
        $user_details->lastname = $data['lastname'];
        $user_details->summary = $data['summary'];
        $user_details->education = $data['education'];
        $user_details->skills = $data['skills'];
        $user_details->passions = $data['passions'];
        $user_details->phone = $data['phone'];
        $user_details->address = $data['address'];
        $user_details->linkedin = $data['linkedin'];
        if ($data['profilepic']) {
            $user_details->profilepic = $data['profilepic'];
        }
        $user_details->save();
        return $user_details;
    }

    public static function saveUserLocation($data) {
        $saveLocation = UserDetails::where(['user_id' => Auth::id()])->first();
        $saveLocation->latitude = $data['latitude'];
        $saveLocation->longitude = $data['longitude'];
        $saveLocation->save();
        return $saveLocation;
    }

    public static function getProfilepicFromId($id) {
        $user = UserDetails::where(['user_id' => $id])->first();
        return self::getProfilepic($user->profilepic);
    }

}
