<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use URL;

class Messages extends Model {

    private $firebaseRepository;
    public $table = 'messages';
    protected $fillable = [
        'from_id', 'to_id', '', 'conversation_id', 'status', 'task_id'];

    public static function getAllUserConversations($user) {
        $messages = Messages::select('tasks.title', 'tasks.address', 'messages.from_id', 'messages.to_id', 'messages.conversation_id', 'messages.task_id')
                ->whereRaw('(messages.from_id =' . $user . ' OR messages.to_id =' . $user . ') AND messages.status =1')
                ->join('tasks', 'messages.task_id', '=', 'tasks.id')
                ->paginate(\Config::get('constants.PAGINATE'));
        return $messages;
    }

    public static function updateMessages($task) {
        $message = Messages::where(['task_id' => $task->id, 'from_id' => $task->employer_id, 'to_id' => $task->employee_id])->first();
        if ($message) {
            $message->status = 0;
            $message->save();
        }
    }

}
