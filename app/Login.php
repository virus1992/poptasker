<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model {

    public $table = 'login';
    protected $fillable = [
        'user_id', 'fcm_token', 'device_id', 'created_at', 'updated_at', 'device_type'
    ];

    public static function getUser($device, $user_id) {
        $user = Login::where(['user_id' => $user_id, 'device_id' => $device])->first();
        return $user;
    }

    public static function getFCMToken($condition = '') {
        $fcmtoken = Login::select('fcm_token','device_type');
        if ($condition) {
            $fcmtoken->where($condition);
        }
        $fcmtoken->where(['is_login' => 1]);
        $fcms = $fcmtoken->get()->toArray();
        return $fcms;
    }

    public static function checkDeviceStatus($user_id) {
        $check = Login::where(['user_id' => $user_id, 'is_login' => 1])->count();
        return $check;
    }

}
