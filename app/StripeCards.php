<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\StripeUser;

class StripeCards extends Model {

    protected $table = 'stripe_cards';
    public $stripe;
    public $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'cust_id', 'card_id', 'last_four_digit', 'card_type', 'card_holder_name',
        'card_status'
    ];

    public static function checkForAvailableCards($user_id) {
        $check = StripeCards::where(['user_id' => $user_id, 'card_status' => 1])->first();
        return $check;
    }

    public static function addCardDetails($card, $data) {
        $createCard = StripeCards::updateOrCreate([
                    'user_id' => Auth::id(),
                    'cust_id' => $card['customer'],
                    'card_id' => $card['id']], [
                    'user_id' => Auth::id(),
                    'cust_id' => $card['customer'],
                    'card_id' => $card['id'],
                    'last_four_digit' => $card['last4'],
                    'card_type' => $card['brand'],
                    'card_holder_name' => $data['name_on_card']
        ]);
        StripeUser::updateCustomerid($card['customer']);
        return $createCard;
    }

    public static function getUserSavedCards($user) {
        $getCards = StripeCards::where(['user_id' => $user, 'card_status' => 1])->get();
        return $getCards;
    }

}
