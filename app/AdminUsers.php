<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class AdminUsers extends Authenticatable {
    protected $guard = 'admin';
    
     protected $fillable = ['email', 'password'];
}
