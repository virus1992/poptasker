<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model {

    public $table = 'user_wallet';
    protected $fillable = [
        'user_id', 'amount'
    ];

    public static function addUpdateUserWallet($user_id, $amount) {
        $user = UserWallet::where(['user_id' => $user_id])->first();
        if ($user) {
            $user->amount = $user->amount + $amount;
            $user->save();
        } else {
            $user = UserWallet::create(['user_id' => $user_id, 'amount' => $amount]);
        }
        return $user;
    }

}
