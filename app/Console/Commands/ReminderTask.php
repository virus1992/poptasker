<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\TaskReminder;
use Carbon\Carbon;
use App\Users;
use App\Http\Services\NotificationTrait;

class ReminderTask extends Command {

    use NotificationTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:task_seeker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder to Applicant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $date = Carbon::now()->addDay();
        $selectdate = $date->format('Y-m-d');
        $tasks = \DB::table('tasks')
                        ->where(['start_date' => $selectdate])
                        ->where(['status' => 'accepted', 'is_deleted' => 0])
                        ->get()->toArray();

        foreach ($tasks as $task) {
            $user = Users::find($task->employee_id);
            $array_mail['data'] = ['user_seeker' => $user->userDetails,
                'task' => $task];
            Mail::to($user->email)->send(new TaskReminder($array_mail['data']));
            $noti_type = 'task-reminder';
            $noti_message = "This is a reminder that your appointment for {{$task->title}} on {{$task->start_date}} at {{$task->start_time}} is coming up";
            $message = "This is a reminder that your appointment for {{$task->title}} on {{$task->start_date}} at {{$task->start_time}} is coming up";
            $this->sendNotification($user, $task, $noti_type, $noti_message, $message);
        }
    }

}
