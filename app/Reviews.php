<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reviews extends Model {

    public $table = 'reviews';
    public $primaryKey = 'review_id';
    protected $fillable = [
        'employer_id', 'employee_id', 'comments', 'rating', 'task_id'
    ];

    public static function getApplicantReviews($employee_id) {
        $reviews = Reviews::select('user_details.firstname', 'user_details.lastname', 'user_details.profilepic', 'reviews.*')
                        ->where(['employee_id' => $employee_id])
                        ->join('user_details', 'reviews.employer_id', '=', 'user_details.user_id')->get()->toArray();
        return $reviews;
    }

    public static function createReview($data) {
        $review = Reviews::updateOrCreate([
                    'employer_id' => Auth::id(),
                    'employee_id' => $data['employee_id'],
                    'task_id' => $data['task_id']], [
                    'rating' => $data['rating'],
                    'comments' => $data['comments'],
                        ]
        );
        return $review;
    }

    public static function getUserReviews($employee_id) {
        $reviews = Reviews::select('user_details.firstname', 'user_details.lastname', 'user_details.profilepic', 'reviews.*')
                        ->where(['employee_id' => $employee_id])
                        ->join('user_details', 'reviews.employer_id', '=', 'user_details.user_id')->paginate(\Config::get('constants.PAGINATE'));
        return $reviews;
    }

}
