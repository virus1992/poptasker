<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use App\Login;
use DB;

class Users extends Authenticatable {

    use Notifiable;

    public $table = 'users';
    protected $fillable = [
        'email', 'password', 'fb_token', 'google_token',
        'created_at', 'updated_at', 'api_token', 'is_confirm',
        'is_active', 'is_deleted'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userDetails() {
        return $this->hasOne('App\UserDetails', 'user_id', 'id');
    }

    public function devices() {
        return $this->hasMany('App\Login', 'user_id', 'id');
    }

    public static function updateToken($data) {
        $userdata = Users::where(['id' => $data['user_id'], 'is_deleted' => 0])->first();
        $userdata->api_token = $data['api_token'];
        $userdata->save();
        return $userdata;
    }

    public static function addFcmToken($data) {
        Login::updateOrCreate(['user_id' => $data['user_id'], 'device_id' => $data['device_id']], $data);
    }

    public static function getFacebookToken($data) {
        $user = Users::where(['fb_token' => $data['fb_token'], 'is_deleted' => 0])->first();
        return $user;
    }

    public static function getGoogleToken($data) {
        $user = Users::where(['google_token' => $data['google_token'], 'is_deleted' => 0])->first();
        return $user;
    }

    public static function checkUserEmail($data) {
        $user = Users::where(['email' => $data['email'], 'is_deleted' => 0])->first();
        return $user;
    }

    public static function getUserFromEmail($email, $token) {
        $user = Users::where(['email' => $email, 'remember_token' => $token, 'is_deleted' => 0])->first();
        return $user;
    }

    public static function getUserFromToken($token) {
        $user = Users::where(['remember_token' => $token, 'is_deleted' => 0])->first();
        return $user;
    }

    public static function getUserDetails($user_id) {
        $application = Users::select('user_details.*', 'users.email', DB::raw("(SELECT count(endorsements.id) FROM endorsements
                                WHERE endorsements.employee_id = users.id AND is_endorse = 1)as endorsements"), DB::raw("(SELECT SUM(reviews.rating) FROM reviews
                                WHERE reviews.employee_id = users.id) as ratings"), DB::raw("(SELECT COUNT(reviews.review_id) FROM reviews
                                WHERE reviews.employee_id = users.id) as totalratings"))
                        ->where(['users.id' => $user_id])
                        ->where(['users.is_deleted' => 0])
                        ->join('user_details', 'users.id', '=', 'user_details.user_id')->first();
        return $application;
    }

    public static function checkSocialLogin($data) {
        $check = Users::select()
                ->where(['fb_token' => $data['fb_token']])
                ->where(['is_deleted' => 0])
                ->first();
        return $check;
    }

    public static function resetPassword($user_id, $password) {
        $check = Users::select()
                ->where(['id' => $user_id])
                ->where(['is_deleted' => 0])
                ->first();
        $check->password = bcrypt($password);
        $check->remember_token = null;
        $check->save();
        return $check;
    }

}
