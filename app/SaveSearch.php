<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveSearch extends Model {

    public $table = 'save_search';
    public $primaryKey = 'search_id';
    protected $fillable = [
        'user_id', 'wage', 'distance', 'working_hours', 'keywords'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public static function saveSearchFilter($data, $user_id) {
        $save = SaveSearch::updateOrCreate([
                    'user_id' => $user_id
                        ], ['wage' => $data['wage'],
                    'distance' => $data['distance'],
                    'working_hours' => $data['working_hours'],
                    'keywords' => $data['keywords']]);
        return $save;
    }

    public static function getUserFilters($user_id) {
        $filters = SaveSearch::where(['user_id' => $user_id])->first();
        if ($filters) {
            return $filters->toArray();
        }
    }

    public static function getFiltersByUser($id) {
        $get = SaveSearch::select('wage', 'distance', 'working_hours', 'keywords')->where(['user_id' => $id])->first();
        return $get;
    }

}
