<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
use URL;
use Auth;
use App\Keywords;
use App\SaveSearch;
use App\Http\Services\CommonTrait;
use App\Users;
use App\Application;
use App\UserCancelTask;

class Tasks extends Model {

    use CommonTrait;

    public $table = 'tasks';
    public $primaryKey = 'id';
    protected $fillable = [
        'title', 'description', 'address',
        'task_lat',
        'task_long',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'wage',
        'employer_id',
        'is_deleted',
        'total_application',
        'working_hours',
        'created_at',
        'updated_at'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public function taskPictures() {
        return $this->hasMany('App\TaskPictures', 'task_id', 'id');
    }

    public function taskkeywords() {
        return $this->hasMany('App\TaskKeywords', 'task_id', 'id');
    }

    public function applicationStatus() {
        return $this->hasOne('App\Application', 'task_id', 'employee_id');
    }

    public function getApplications() {
        return $this->hasMany('App\Application', 'task_id', 'id');
    }

    public function employerDetails() {
        return $this->hasOne('App\UserDetails', 'user_id', 'employer_id');
    }

    public function employeeDetails() {
        return $this->hasOne('App\UserDetails', 'user_id', 'employee_id');
    }

    public static function getImage($value) {
        return (!empty($value) && (file_exists(public_path() . '/../' . \Config::get('constants.IMAGE_PATH') . '/taskpictures/' . $value))) ? URL::to('/storage/app/public/taskpictures') . '/' . $value : "";
    }

    public static function getImageThumb($value) {
        return (!empty($value) && (file_exists(public_path() . '/../' . \Config::get('constants.IMAGE_PATH') . '/taskpictures/tmp/' . $value))) ? URL::to('/storage/app/public/taskpictures') . '/tmp/' . $value : "";
    }

    public function scopeDeletetask($query) {
        return $query->where('is_deleted', self::IS_FALSE);
    }

    public function scopeCompletedtask($query) {
        return $query->where('status', 'completed');
    }

    public function scopePendingtask($query) {
        return $query->where('status', "pending");
    }

    public static function createTask($data) {

        $data['start_date'] = Carbon::parse($data['start_date'])->format('Y-m-d');
        $data['start_time'] = Carbon::parse($data['start_time'])->format('H:i:s');
        $data['end_date'] = Carbon::parse($data['end_date'])->format('Y-m-d');
        $data['end_time'] = Carbon::parse($data['end_time'])->format('H:i:s');
        $data['working_hours'] = self::convertTimestamp($data['end_time']) - self::convertTimestamp($data['start_time']);
        $data['working_hours'] = $data['working_hours'] / 100000 / 36;
        $data['created_at'] = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('Y-m-d H:i:s');
        $data['updated_at'] = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('Y-m-d H:i:s');
        $addTask = Tasks::create($data);
        return $addTask;
    }

    public static function getAllActivities($status = '') {

        if ($status && $status != 'all') {
            if ($status == 'posted') {
                $tasks = Tasks::deletetask()->select('*', 'tasks.id as taskid')->where(['employer_id' => Auth::id()])->orderBy('tasks.id', 'desc')->paginate(\Config::get('constants.PAGINATE'));
            } else if ($status == 'pending') {
                $tasks = Tasks::deletetask()->select('*', 'tasks.id as taskid')
                                ->join('applications', 'applications.task_id', '=', 'tasks.id')
                                ->orderBy('tasks.id', 'desc')
                                ->where(['tasks.status' => $status, 'applications.user_id' => Auth::id(), 'applications.application_status' => 'accepted'])->paginate(\Config::get('constants.PAGINATE'));
            } elseif ($status == 'cancelled') {
                $tasks = Tasks::select('*', 'tasks.id as taskid')
                        ->join('user_cancel_job', 'user_cancel_job.task_id', '=', 'tasks.id')
                        ->where(['user_cancel_job.user_id' => Auth::id()])
                        ->paginate(\Config::get('constants.PAGINATE'));
            } else if ($status == 'deleted') {
                $tasks = Tasks::select('*', 'tasks.id as taskid')
                        ->where(['is_deleted' => 1])
                        ->whereRaw('(employee_id=' . Auth::id() . ' OR employer_id=' . Auth::id() . ')')
                        ->paginate(\Config::get('constants.PAGINATE'));
            } else {
                $tasks = Tasks::select('*', 'tasks.id as taskid')->where(['status' => $status, 'employee_id' => Auth::id()])->orderBy('tasks.id', 'desc')->paginate(\Config::get('constants.PAGINATE'));
            }
        } else {
            $tasks = Tasks::select('*', 'tasks.id as taskid')
                            ->selectRaw('CASE WHEN EXISTS (SELECT user_cancel_job.task_id from user_cancel_job where user_cancel_job.user_id=' . Auth::id() . ' AND user_cancel_job.task_id = applications.task_id) THEN 1 ELSE 0 END as is_cancelled')
                            ->leftjoin('user_cancel_job', 'user_cancel_job.task_id', '=', 'tasks.id')
                            ->leftjoin('applications', 'applications.task_id', '=', 'tasks.id')
//                            ->groupBy('applications.task_id')
                            ->groupBy('tasks.id')
                            ->whereRaw('(tasks.employee_id=' . Auth::id() . ' OR tasks.employer_id=' . Auth::id() . ') OR (applications.user_id=' . Auth::id() . ')')->orderBy('tasks.id', 'desc')->paginate(\Config::get('constants.PAGINATE'));
        }
//        print_r($tasks);die;
        foreach ($tasks as $task) {
            $pictures = self::getTaskPictures($task);
            $keywords = self::getTaskKeywords($task);
            $task->pictures = ($pictures) ? $pictures : "";
            $task->keywords = ($keywords) ? $keywords : "";
        }
        return $tasks;
    }

    public static function getTaskPictures($task) {
        if ($task->taskid) {
            $tasknew = Tasks::find($task->taskid);
            $pictures = $tasknew->taskPictures->toArray();
        } else {
            $pictures = $task->taskPictures->toArray();
        }
        $images = [];
        foreach ($pictures as $pics) {
            $images[] = ['thumb' => self::getImageThumb($pics['picture']), 'image' => self::getImage($pics['picture'])];
        }
        return $images;
    }

    public static function getTaskKeywords($task) {
        if ($task->taskid) {
            $tasknew = Tasks::find($task->taskid);
            $keywords = $tasknew->taskKeywords;
        } else {
            $keywords = $task->taskKeywords;
        }
        $words = [];
        foreach ($keywords as $word) {
            $getword = Keywords::find($word->keyword_id);
            $words[] = $getword->keyword;
        }
        $kywrds = implode(', ', $words);
        return $kywrds;
    }

    public static function getPostedApplications() {
        $applications = Tasks::where(['employer_id' => Auth::id()])
                        ->orderBy('id', 'desc')->paginate(\Config::get('constants.PAGINATE'));
        return $applications;
    }

    public static function getCompletedJobs($user_id) {
        $jobs = Tasks::completedtask()->where(['employee_id' => $user_id])->count();
        return $jobs;
    }

    public static function getNearestJobs($data = '', $offset, $limit) {
        $user = Auth()->user()->userDetails;
        $circle_radius = \Config::get('constants.EARTH_RADIUS');
        $coupontotal = [];
        $lat = $user->latitude;
        $lng = $user->longitude;
        $current_date = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('Y-m-d');
        $current_time = Carbon::parse(now())->setTimezone(\Config::get('constants.TIMEZONE'))->format('H:i:s');
        $filters = SaveSearch::getUserFilters($user->user_id);
        $tasklist = Tasks::deletetask()->groupBy('task_id')
                ->select('*', 'tasks.id as taskid')
                ->selectRaw('CASE WHEN EXISTS (SELECT applications.application_id from applications where applications.user_id=' . $user->user_id . ' AND applications.task_id = tasks.id) THEN 1 ELSE 0 END as is_applied')
                ->join('task_keywords', 'task_keywords.task_id', '=', 'tasks.id')
                ->selectRaw('((' . $circle_radius . ' * acos(cos(radians(' . $lat . ')) * cos(radians(task_lat)) * cos(radians(task_long) - radians(' . $lng . ')) + sin(radians(' . $lat . ')) * sin(radians(task_lat)))) ) as distance')
                ->where('tasks.employer_id', '!=', Auth::id())
                ->where('tasks.employee_id', '=', null)
                ->where(['tasks.status' => 'pending'])
                ->orderBy('tasks.id', 'desc');
        if (strtolower($data['page']) != 'all') {
            $tasklist->skip($offset);
            $tasklist->take($limit);
        }
        $tasklist->whereRaw('IF (start_date = "' . $current_date . '",start_time >= "' . $current_time . '", start_date >="' . $current_date . '")');
        if ($filters) {
            $filter_keywords = explode(',', $filters['keywords']);
            $wages = explode('-', $filters['wage']);
            $working_hours = explode('-', $filters['working_hours']);
            $distances = $filters['distance'];
            $tasklist->whereBetween('tasks.wage', [$wages[0], $wages[1]]);
            $tasklist->whereBetween('tasks.working_hours', [$working_hours[0], $working_hours[1]]);
            $tasklist->having('distance', '<', $distances);
            if ($filter_keywords && count($filter_keywords) > 0) {
                $tasklist->whereIn('task_keywords.keyword_id', $filter_keywords);
            }
            $taskfinal = $tasklist->get();
//            $taskfinal = self::applyFilters($distances, $tasks);
        } else {
            $taskfinal = $tasklist->get();
        }
        foreach ($taskfinal as $task) {
            $pictures = self::getTaskPictures($task);
            $keywords = self::getTaskKeywords($task);
            $task->pictures = ($pictures) ? $pictures : "";
            $task->keywords = ($keywords) ? $keywords : "";
        }
        return $taskfinal;
    }

    public static function applyFilters($filters, $tasks) {
        foreach ($tasks as $key => $task) {
            if ($task->distance > $filters) {
                $tasks->forget($key);
            }
        }
        return $tasks;
    }

    public static function getTaskPosterCount($filter = '') {

        $data = Tasks::select('employer_id')->groupBy('employer_id');
        if ($filter) {
            $data->whereBetween('created_at', [Carbon::parse($filter['from_date'])->format('Y-m-d H:i:s'), Carbon::parse($filter['to_date'])->format('Y-m-d H:i:s')]);
        } else {
            $data->whereBetween('created_at', [Carbon::now()->addMonths(-1)->format('Y-m-d H:i:s'), Carbon::now()->format('Y-m-d H:i:s')]);
        }
        $poster = $data->get()->toArray();

        return count($poster);
    }

    public static function getTaskSeekerCount($filter = '') {
        $data1 = Tasks::select('employer_id')->groupBy('employer_id');
        $data2 = Application::select('user_id')->groupBy('user_id');
        if ($filter) {
            $data1->whereBetween('created_at', [Carbon::parse($filter['from_date'])->format('Y-m-d H:i:s'), Carbon::parse($filter['to_date'])->format('Y-m-d H:i:s')]);
        } else {
            $data1->whereBetween('created_at', [Carbon::now()->addMonths(-1)->format('Y-m-d H:i:s'), Carbon::now()->format('Y-m-d H:i:s')]);
        }
        $poster = $data1->get()->toArray();
        $applications = $data2->get()->toArray();

        $users = [];
        $posterusers = [];
        foreach ($applications as $user) {
            $users[] = $user['user_id'];
        }

        foreach ($poster as $user) {
            $posterusers[] = $user['employer_id'];
        }

        $data = Users::whereIn('id', $users)->whereNotIn('id', $posterusers);
        if ($filter) {
            $data->whereBetween('created_at', [Carbon::parse($filter['from_date'])->format('Y-m-d H:i:s'), Carbon::parse($filter['to_date'])->format('Y-m-d H:i:s')]);
        } else {
            $data->whereBetween('created_at', [Carbon::now()->addMonths(-1)->format('Y-m-d H:i:s'), Carbon::now()->format('Y-m-d H:i:s')]);
        }
        $seeker = $data->get()->toArray();
        return count($seeker);
    }

    public static function getAllTasks($filter = '') {
        $data = Tasks::select('*');
        if ($filter) {
            $data->whereBetween('created_at', [Carbon::parse($filter['from_date'])->format('Y-m-d H:i:s'), Carbon::parse($filter['to_date'])->format('Y-m-d H:i:s')]);
        } else {
            $data->whereBetween('created_at', [Carbon::now()->addMonths(-1)->format('Y-m-d H:i:s'), Carbon::now()->format('Y-m-d H:i:s')]);
        }
        $details = $data->get()->toArray();
        return $details;
    }

    public static function getTransactionbyWeek($filter = array()) {
        $year = (isset($filter['year']) && $filter['year'] != "") ? $filter['year'] : date('Y');
        $tasks = Tasks::select(DB::raw("COALESCE(SUM(case when  (week(created_at)-week(DATE_FORMAT(created_at ,'%Y-%m-01')))+1=1 then (wage) else 0 end),0) week1,
COALESCE(SUM(case when   (week(created_at)-week(DATE_FORMAT(created_at ,'%Y-%m-01')))+1=2 then (wage) else 0 end),0) week2,
COALESCE(SUM(case when   (week(created_at)-week(DATE_FORMAT(created_at ,'%Y-%m-01')))+1=3 then (wage) else 0 end),0) week3,
COALESCE(SUM(case when   (week(created_at)-week(DATE_FORMAT(created_at ,'%Y-%m-01')))+1=4 then (wage) else 0 end),0) week4,
COALESCE(SUM(case when   (week(created_at)-week(DATE_FORMAT(created_at ,'%Y-%m-01')))+1=5 then (wage) else 0 end),0) week5,
COALESCE(SUM(case when   (week(created_at)-week(DATE_FORMAT(created_at ,'%Y-%m-01')))+1=6 then (wage) else 0 end),0) week6"))
                        ->where('status', 'completed')->whereYear('created_at', $year)
                        ->get()->toArray();
        return $tasks;
    }

    public static function getTransactionbyMonth($filter = '') {
        $year = (isset($filter['year']) && $filter['year'] != "") ? $filter['year'] : date('Y');
        $tasks = Tasks::select(DB::raw("COALESCE(SUM(case when month(created_at)=1 then wage else 0 end),0) jan,
COALESCE(SUM(case when month(created_at)=2 then wage else 0 end),0) feb,
COALESCE(SUM(case when month(created_at)=3 then wage else 0 end),0) mar,
COALESCE(SUM(case when month(created_at)=4 then wage else 0 end),0) april,
COALESCE(SUM(case when month(created_at)=5 then wage else 0 end),0) may,
COALESCE(SUM(case when month(created_at)=6 then wage else 0 end),0) june,
COALESCE(SUM(case when month(created_at)=7 then wage else 0  end),0) july,
COALESCE(SUM(case when month(created_at)=8 then wage else 0 end),0) aug,
COALESCE(SUM(case when month(created_at)=9 then wage else 0 end),0) sep,
COALESCE(SUM(case when month(created_at)=10 then wage else 0 end),0) oct,
COALESCE(SUM(case when month(created_at)=11 then wage else 0 end),0) nov,
COALESCE(SUM(case when month(created_at)=12 then wage else 0 end),0) dece"))
                        ->where('status', 'completed')->whereYear('created_at', $year)
                        ->get()->toArray();
        return $tasks;
    }

    public static function getTaskByTaskid($task_id) {
        $task = Tasks::where(['id' => $task_id, 'employee_id' => Auth::id()])->first();
        return $task;
    }

    public static function getTaskById($task_id) {
        $task = Tasks::select('tasks.*', DB::raw('CASE WHEN EXISTS (SELECT applications.application_id from applications where applications.user_id = ' . Auth::id() . ' AND applications.task_id = tasks.id) THEN 1 ELSE 0 END as is_applied'))->where(['id' => $task_id])->first();
        if ($task) {
            $pictures = self::getTaskPictures($task);
            $keywords = self::getTaskKeywords($task);
            $task->pictures = ($pictures) ? $pictures : "";
            $task->keywords = ($keywords) ? $keywords : "";
        }
        return $task;
    }

    public static function getNotReviewedTask($user_id) {
        $task = Tasks::where(['employer_id' => $user_id, 'status' => 'completed', 'is_reviewed' => 0])
                ->orderBy('updated_at', 'desc')
                ->first();
        return $task;
    }

    public static function getTransactionquarterly($filter = '') {
        $year = (isset($filter['year']) && $filter['year'] != "") ? $filter['year'] : date('Y');
        $tasks = Tasks::select(DB::raw("COALESCE(SUM(case when month(created_at) IN (1,2,3) then wage else 0 end),0) jan_mar,
COALESCE(SUM(case when month(created_at) IN (4,5,6) then wage else 0 end),0) apr_jun,
COALESCE(SUM(case when month(created_at) IN (7,8,9) then wage else 0 end),0) jul_sep,
COALESCE(SUM(case when month(created_at) IN (10,11,12) then wage else 0 end),0) oct_dec"))
                        ->where('status', 'completed')->whereYear('created_at', $year)
                        ->get()->toArray();
        return $tasks;
    }

    public static function getTransactionyearly($filter = '') {

        $year = (isset($filter['year']) && $filter['year'] != "") ? $filter['year'] : date('Y');
        $tasks = Tasks::select(DB::raw("SUM(wage) as year"))
                        ->where('status', 'completed')->whereYear('created_at', $year)
                        ->get()->toArray();
        if (!empty($tasks)) {
            foreach ($tasks as $key => $value) :
                $tasks[$key][$year] = (isset($value['year']) && $value['year'] != "") ? $value['year'] : 0;
                unset($tasks[$key]['year']);
            endforeach;
        }
        return $tasks;
    }

}
