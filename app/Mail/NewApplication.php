<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewApplication extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->markdown('emails.application')
                        ->subject('Congratulations! ' . $this->data['user_seeker']->firstname . ' has applied for ' . $this->data['task']->title . '.Check out ' . $this->data['user_seeker']->firstname . '\'s profile quickly!')
                        ->with(['data' => $this->data]);
    }

}
