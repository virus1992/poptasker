<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskReminder extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->markdown('emails.taskreminder')
                        ->subject('Task Reminder!Your task is scheduled on ' . $this->data['task']->start_date . ' at ' . $this->data['task']->start_time . ' for ' . $this->data['task']->title . '.')
                        ->with(['data' => $this->data]);
    }

}
