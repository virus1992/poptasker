<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
  protected $fillable = ['page_title', 'page_description'];
}
