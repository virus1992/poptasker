<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutPaidDetails extends Model {

    protected $fillable = ['user_id', 'amount', 'comment', 'created_at', 'payout_month'];

}
