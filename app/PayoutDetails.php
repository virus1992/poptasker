<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutDetails extends Model {

    public $table = 'payout_details';
    protected $fillable = [
        'account_no', 'user_id', 'bank_name', 'param1', 'param2', 'param3', 'param4'];

    public static function addPayoutDetails($userid, $data) {
        $add = PayoutDetails::updateOrCreate(
                        ['user_id' => $userid], [
                    'user_id' => $userid,
                    'account_no' => $data['account_no'],
                    'bank_name' => $data['bank_name'],
                    'param1' => ($data['param1']) ? $data['param1'] : "",
                    'param2' => ($data['param2']) ? $data['param2'] : "",
                    'param3' => ($data['param3']) ? $data['param3'] : "",
                    'param4' => ($data['param4']) ? $data['param4'] : ""
        ]);

        return $add;
    }

    public static function getPayoutDetails($userid) {
        $payout = PayoutDetails::where(['user_id' => $userid])->first();
        if ($payout) {
            return $payout->toArray();
        } else {
            return $payout;
        }
    }

}
