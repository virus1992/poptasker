<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Help extends Model {

    public $table = 'help';
    public $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'description', 'email', 'status'
    ];

    public static function addQuery($query, $user_id) {
        $submit = Help::create([
                    'user_id' => $user_id,
                    'email' => $query['email'],
                    'description' => $query['query']
        ]);
        return $submit;
    }

}
