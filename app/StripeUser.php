<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Stripe\Stripe;
use App\StripeCards;
use Auth;
use App\UserWallet;
use App\Transactions;

class StripeUser extends Model {

    protected $table = 'stripe_user';
    public $stripe;
    public $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'cust_id', 'user_id', 'billing_name', 'billing_address', 'billing_city',
        'billing_state', 'billing_country', 'billing_zip'
    ];

    public function __construct() {

        $this->stripe = new Stripe(\Config::get('constants.STRIPE_SECRET'), \Config::get('constants.STRIPE_VERSION'));
    }

    /**
     * Get the user detail record associated with the customer id.
     */
    public function userStripe() {
        return $this->hasOne('App\Users', 'id', 'user_id');
    }

    public static function createStripeUser($id, $data) {

        $stripeuser = StripeUser::updateOrCreate(
                        ['user_id' => $id], ['user_id' => $id,
                    'billing_name' => $data['billing_name'],
                    'billing_address' => $data['billing_address'],
                    'billing_city' => $data['billing_city'],
                    'billing_state' => $data['billing_state'],
                    'billing_country' => $data['billing_country'],
                    'billing_zip' => $data['billing_zip']]
        );
        return $stripeuser;
    }

    public function createToken($data, $user, $stripe) {
        $token = $this->stripe->tokens()->create([
            'card' => [
                'number' => $data['card_number'],
                'exp_month' => $data['exp_month'],
                'cvc' => $data['cvv'],
                'exp_year' => $data['exp_year']
            ],
        ]);
        if ($stripe->cust_id) {
            $customerid = $stripe->cust_id;
        } else {
            $customerid = $this->createCustomer($user);
        }
        if ($customerid) {
            $this->storeCard($customerid, $token, $data);
        }
    }

    public static function getCustomerDetails($id) {
        $customer = StripeUser::select('*')
                ->where('user_id', $id)
                ->first();
        return $customer;
    }

//create customer
    public function createCustomer($data) {
        $customer = $this->stripe->customers()->create([
            'email' => $data->email,
        ]);
        if (!empty($customer)) {
            $customerid = $customer['id'];
            return $customerid;
        }
        return false;
    }

// store the card of particular customer
    public function storeCard($customerid, $token, $data) {

        $card = $this->stripe->cards()->create($customerid, $token['id']);
        $storecard = StripeCards::addCardDetails($card, $data);
        return $storecard;
    }

//create subcription
    public function createSubcription($plan_id) {

        $stripeid = $this->stripe_id;
        $subscription = $this->stripe->subscriptions()->create($stripeid, ['plan' => $plan_id]);
        $result = Subscription::saveSubcriptionPlan($subscription, $this->user_id);
        if ($result) {
            return true;
        }
        return false;
    }

    public static function findCustomer($email) {
        $stripe = new StripeUser();
        $customers = $stripe->stripe->customers()->all(['limit' => 15]);

        foreach ($customers['data'] as $customer) {
            if ($customer['email'] == $email) {
                self::deleteCustomer($customer['id']);
            }
        }
    }

//delete a customer with existing id
    public static function deleteCustomer($customerid) {
        $stripe = new StripeUser();
        $customer = $stripe->stripe->customers()->delete($customerid);
    }

//Delete Current Card for existing stripe customer
    public static function deleteCard($customerid, $card_id) {
        $stripe = new StripeUser();
        $card = $stripe->stripe->cards()->delete($customerid, $card_id);
        return $card;
    }

//Create New Card and update details to database
    public static function createCard($customerid, $data, $stripe_id) {
        $stripe = New StripeUser();
        $dateexplode = explode('/', $data['card_expiry']);
        $token = $stripe->stripe->tokens($customerid)->create([
            'card' => [
                'number' => $data['card_no'],
                'exp_month' => $dateexplode[0],
                'cvc' => $data['card_cvv'],
                'exp_year' => $dateexplode[1],
            ],
        ]);
        if ($token && array_key_exists('id', $token)) {
            $card = $stripe->updateCard($customerid, $token, $stripe_id);
            return $card;
        } else {
            return $token;
        }
    }

// store the card of particular customer
    public function updateCard($customerid, $token, $stripe_id) {

        $card = $this->stripe->cards()->create($customerid, $token['id']);
        $data = array('card_id' => $card['id'], 'card_brand' => $card['brand'], 'card_last_four' => $card['last4']);
        $addCard = StripeUser::where('stripe_id', $customerid)->first();
        $addCard->card_id = $card['id'];
        $addCard->card_brand = $card['brand'];
        $addCard->card_last_four = $card['last4'];
        $addCard->save();
        return $card;
    }

    public static function cancelSubscription($data) {
        $stripe = New StripeUser();
        $subscription = $stripe->stripe->subscriptions()->cancel($data['stripe_id'], $data['subscription_id']);
        if ($subscription['status'] == 'canceled') {
            $subupdate = \App\Subscription::select('*')
                    ->where('user_id', $data['user_id'])
                    ->first();
            $subupdate->sub_id = '';
            $subupdate->save();
            return 1;
        }
        return 1;
    }

    public static function updateSubscription($data) {
        $stripe = New StripeUser();
        $subscription = $stripe->stripe->subscriptions()->update($data['stripe_id'], $data['sub_id'], [
            'plan' => $data['plan_id'], 'trial_end' => 'now',
        ]);
        \App\Subscription::updateSubcriptionPlan($subscription, $data['user_id']);

        return TRUE;
    }

    public static function makePayment($user_id, $amount) {
        $user = StripeUser::where(['user_id' => $user_id])->first();
        if ($user) {
            $stripe = New StripeUser();
            $payment = $stripe->stripe->charges()->create([
                'customer' => $user->cust_id,
                'currency' => 'USD',
                'amount' => $amount,
            ]);
            if ($payment['status'] == 'succeeded') {
                UserWallet::addUpdateUserWallet($user_id, $amount);
                Transactions::addTransaction($payment);
            }
            return $payment;
        } else {
            return FALSE;
        }
    }

    public static function changeSubscription($data) {
        $stripe = New StripeUser();
        $subscription = $stripe->stripe->subscriptions()->create($data['stripe_id'], [
            'plan' => $data['plan_id'], 'trial_end' => 'now',
        ]);
        \App\Subscription::updateSubcriptionPlan($subscription, $data['user_id']);
        return TRUE;
    }

    public static function chargeVendor($vendor, $amount, $description = '') {
        $stripe = New StripeUser();
        $charge = $stripe->stripe->charges()->create([
            'customer' => $vendor['stripe_id'],
            'currency' => 'USD',
            'amount' => $amount,
            'description' => $description
        ]);

        return $charge;
    }

    public static function updateCustomerid($cust_id) {
        $user = StripeUser::where(['user_id' => Auth::id()])->first();
        $user->cust_id = $cust_id;
        $user->save();
    }

}
