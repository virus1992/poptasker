<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {

    public $table = 'settings';
    public $primaryKey = 'id';
    protected $fillable = [
        'email', 'contact_no', 'commision', 'param1', 'param2', 'param3'
    ];

    const IS_TRUE = 1;
    const IS_FALSE = 0;

    public static function getSettings() {
        $settings = Settings::first();
        return $settings;
    }

}
