<!DOCTYPE html>    
<html lang="en" class="no-scroll">
    <head>

        <title>Poptasker</title>

        <!-- FontAwesome -->
        <link href="fonts/fontawesome/font-awesome.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    </head>

    <body style="max-width: 600px;margin: 0 auto;font-family: 'Lato', sans-serif;font-size: 14px;background: #f8f8f8;"> 
        <div class="wrapper" style="background: #f1f1f1;">
            <div class="header" style="padding: 15px 0px;display: inline-block;width: 100%;background: #FFF;">

                <div class="logo" style="text-align: center;">
                    <?php $pathToFile = \Config::get('app.url') . \Config::get('constants.IMAGE_PATH') . 'images/logo_1.png' ?>
                    <a href="<?php echo \Config::get('app.url'); ?>"><img src="<?php echo $pathToFile; ?>" style="max-width: 100px;" /></a>
                </div>

            </div>
            @yield('content')
            <div class="footer" style="display: inline-block;width: 100%;border-top: 1px solid #e8e8e8;text-align: center;padding-top: 20px;padding-bottom: 20px;">
                <p style="color: #1a1a1a;font-size: 12px;">Email sent by Poptasker<br>&copy; 2017 Poptasker All rights reserved</p>
            </div>
        </div>
    </body>
</html>
