@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Payouts')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Payouts</h3>
                </div>
                <div class="ibox-content">
                    <div class="f-to-t-form">
                        <form role="form" class="form-inline" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <p>From: </p>
                                    </div>
                                    @php 
                                    $month = ['01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December']
                                    @endphp
                                    <div class="form-group">
                                        <select name="from_month" class="form-control" id="PayoutFromMonth">
                                            <option value="">Select Month</option>
                                            @foreach($month as $key => $value)
                                            <option value="{{ $key }}" {{ (isset($Data['from_month']) && $Data['from_month'] == $key) ? "selected" : "" }}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p>To:</p>
                                    </div>
                                    <div class="form-group">
                                        <select name="to_month" class="form-control" id="PayoutToMonth">
                                            <option value="">Select Month</option>
                                            @foreach($month as $key => $value)
                                            <option value="{{ $key }}" {{ (isset($Data['to_month']) && $Data['to_month'] == $key) ? "selected" : "" }} >{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="SearchTrans" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>																												
                        </form>
                    </div>
                    <div class="stats-top-form">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <p><span>Total Payments</span> <span id="jobs-applied">$ {{ ($Data['TotalPayments'] != "") ? $Data['TotalPayments'] : 0 }}</span></p>
                            </div>  
                            <div class="col-md-4 text-center">
                                <p><span>Total Payouts</span> <span id="jobs-posted">$ {{ ($Data['TotalPayouts']['total_amount'] != "") ? $Data['TotalPayouts']['total_amount'] : 0 }}</span></p>
                            </div>  
                            <div class="col-md-4 text-center">
                                <p><span>Earnings</span> <span id="total-jobs">$ {{ $Data['TotalPayments'] - $Data['TotalPayouts']['total_amount'] }}</span></p>
                            </div>
                        </div>                                        
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-payouts" >
                            <thead>
                                <tr>
                                    <th>S No</th>
                                    <th>Name</th>
                                    <th>Month</th>
                                    <th width="15%" class="text-right">Monthly Payable($)</th>
                                    <th width="15%" class="text-right">Monthly Paid($)</th>
                                    <th class="text-right">Till Date ($)</th>
                                    <th width="12%">Status</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @isset($Data['Payouts'])
                                @if(!empty($Data['Payouts']))
                                @foreach($Data['Payouts'] as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value['firstname'] . " " . $value['lastname'] }}</td>
                                    <td>{{ (isset($value['created_at']) && $value['created_at'] != "") ? date('M Y', strtotime($value['created_at'])) : "" }}</td>
                                    <td class="text-right"><span class="amount">{{ (isset($value['total_amount']) && $value['total_amount'] != "") ? $value['total_amount'] : 0 }}</span></td>
                                    <td class="text-right"><span class="amount" id="MonthlyPaidamount-{{ $key }}">{{ (isset($value['PaidthisMonth']) && $value['PaidthisMonth'] != "") ? $value['PaidthisMonth'] : 0 }}</span></td>
                                    <td class="text-right"><span class="amount">{{ (isset($value['Payout_tilltoday']['total_amount']) && $value['Payout_tilltoday']['total_amount'] != "") ? $value['Payout_tilltoday']['total_amount'] : 0 }}</span></td>
                                    <td class="status">
                                        <span id="payouts-status-{{ $key }}" class="text {{ ($value['is_paid'] == 1) ? 'active' : '' }}">{{ ($value['is_paid'] == 1) ? 'Paid' : 'Unpaid' }}</span>
                                        @if($value['is_paid'] == 0)
                                        <button class="btn btn-primary PayAmountPopup" data-rowid="{{ $key }}" data-toggle="modal" data-target="#PayAmountModal" type="button" data-url="{{ route('payout.monthlypayoutDetails') }}" data-user_id="{{ $value['user_id'] }}" data-month="{{ $value['created_at'] }}" id="PayAmountMonthly-{{ $key }}">Pay</button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- MODAL FOR PAID AMOUNT --}}
<div id="PayAmountModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payout Amount</h4>
            </div>
            <form method="post" id="SavePayoutform">
                {{ csrf_field() }}
                <input type="hidden" id="savePaidamountdetailsURl" data-url="{{ route('payouts.savePaidamountdetails') }}">
                <input type="hidden" name="month_year" id="payout_month_year"/>
                <input type="hidden" name="payout_user_id" id="payout_user_id"/>
                <input type="hidden" name="BalanceAmount" id="BalanceAmount"/>
                <input type="hidden" name="PayableAmount" id="PayableAmount"/>
                <input type="hidden" id="rowid"/>
                <div class="modal-body">
                    <p class="text-center"><strong><span id="payout_user_name_text"></span><span id="payout_month_yeartext"></span></strong></p>
                    <p><strong>Payable : </strong><span id="PayableAmountTxt"></span></p>
                    <p><strong>Paid</strong> : <span id="PaidAmount"></span></p>
                    <p><strong>Balance</strong> : <span id="BalanceAmountTxt"></span></p>
                    <br/>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3">Amount</label>
                            <div class="col-md-9">
                                <input type="text" name="amount" class="form-control" required id="paid_amount">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3">Comments</label>
                            <div class="col-md-9">
                                <textarea name="comments" class="form-control" id="comments"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="SavePayout">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/payouts.js') }}"></script>
@endsection


