@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Transactions')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Transactions</h3>
                </div>
                <div class="ibox-content">
                    <div class="f-to-t-form">
                        <form role="form" class="form-inline" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" id="getRefundableTaskUrl" data-url="{{ route('transaction.getRefundableTask') }}">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>From: </label>
                                        <input class="form-control datepicker" name="startdate" autocomplete="off" id="startdate" type="text" placeholder="mm/dd/YYY" value="{{ (isset($Transactions['startdate'])) ? $Transactions['startdate'] : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>To</label>
                                        <input class="form-control datepicker" name="enddate" autocomplete="off" id="enddate" type="text" placeholder="mm/dd/YYY" value="{{ (isset($Transactions['enddate'])) ? $Transactions['enddate'] : "" }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Task Status</label>
                                        <select class="form-control" name="tast_status">
                                            <option value="">Select Status</option>
                                            <option value="pending" {{ (isset($Transactions['status']) && $Transactions['status'] == "pending") ? "selected" : "" }}>Pending</option>
                                            <option value="accepted" {{ (isset($Transactions['status']) && $Transactions['status'] == "accepted") ? "selected" : "" }}>Accepted</option>
                                            <option value="completed" {{ (isset($Transactions['status']) && $Transactions['status'] == "completed") ? "selected" : "" }} >Completed</option>
                                            <option value="cancelled" {{ (isset($Transactions['status']) && $Transactions['status'] == "cancelled") ? "selected" : "" }} >Cancelled</option>
                                            <option value="inprogress" {{ (isset($Transactions['status']) && $Transactions['status'] == "inprogress") ? "selected" : "" }} >In Progress</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Users</label>
                                        <select class="form-control" name="userid">
                                            <option value="">Select Users</option>
                                            @if(isset($Transactions['Users']) && !empty($Transactions['Users']))
                                            @foreach($Transactions['Users'] as $k1 => $v1)
                                            <option value="{{ $v1['id'] }}" {{ (isset($Transactions['userid']) && $Transactions['userid'] == $v1['id']) ? "selected" : "" }}>{{ $v1['firstname'] . " " . $v1['lastname'] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="SearchTrans" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>																												
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-transactions">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Task Name</th>
                                    <th>Task Description</th>
                                    <th>Task Poster Name</th>
                                    <th>Task Poster Email / Phone</th>
                                    <th>Task Status</th>
                                    <th>Type</th>
                                    <th class="text-right">Amount ($)</th>
                                    <th class="text-right">Refunded Amount ($)</th>
                                    <th class="text-right">Reason for refund</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($Transactions['Data']) && !empty($Transactions['Data']))
                                @foreach($Transactions['Data'] as $key => $value)
                                <tr>
                                    <td>{{ $key + 1  }}</td>
                                    <td>{{ $value['task_title']  }}</td>
                                    <td>{{ $value['task_description']  }}</td>
                                    <td>{{ $value['firstname'] . " " . $value['lastname']  }}</td>
                                    <td>{{ $value['email'] . " / " . $value['phone']  }}</td>
                                    <td>{{ (isset($value['task_status'])) ? ucfirst($value['task_status']) : ""  }}</td>
                                    <td>{{ (isset($value['trans_type'])) ? ucfirst($value['trans_type']) : ""  }}</td>
                                    <td class="text-right"><span class="amount">{{ $value['amount']  }}</span></td>
                                    <td class="text-right"><span class="refund_amount-{{ $value['id'] }}">{{ $value['refund_amount']  }}</span></td>
                                    <td><span class="refund_reason-{{ $value['id'] }}">{{ $value['refund_reason']  }}</span></td>
                                    <td>
                                        <span id="RefundTxt-{{ $value['id'] }}">
                                            @if($value['task_status'] != "completed" && $value['trans_type'] == "spend")
                                            @if($value['amount'] - $value['refund_amount'] <= '0')
                                            <span class="text-refunded">Refunded</span>
                                            @else
                                            <button type="button" class="btn btn-info btn-sm RefundPopup" data-toggle="modal" data-target="#refundModal" id="refundModal-{{ $value['id'] }}" data-id="{{ $value['id'] }}">Refund</button>
                                            @endif
                                            @endif
                                        </span>
                                    </td>
                                </tr>
                                @endforeach
                                @endif                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- MODAL FOR REFUND AMOUNT --}}
<div id="refundModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Refund Amount</h4>
            </div>
            <form method="post" id="form">
                {{ csrf_field() }}
                <input type="hidden" id="refundamountUrl" data-url="{{ route('transactions.refundamount') }}">
                <input type="hidden" id="RefundableTaskid">
                <div class="modal-body">
                    <p><strong> Total : </strong><span id="TaskAmount"></span></p>
                    <p><strong>Refunded Amount</strong> : <span id="RefundedAmount"></span></p>
                    <p><strong>Balance</strong> : <span id="BalanceAmount"></span></p>
                    <p><strong>Extra Refunded Amount</strong> : <span id="ExtraRefundedAmount"></span></p>
                    <br/>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3">Refund Amount</label>
                            <div class="col-md-9">
                                <input type="text" name="refund_amount" class="form-control" required id="refund_amount">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3">Reason of refund</label>
                            <div class="col-md-9">
                                <textarea name="refund_reason" class="form-control" id="refund_reason"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="SaveRefund">Refund</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>
</div>

@endsection

@section('styles')
<link href="{{ asset('css/admin/plugins/datepicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/transactions.js') }}"></script>
@endsection