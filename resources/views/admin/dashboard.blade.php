@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Dashboard')

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-4">
            <a href="{{route('users.index') }}">
                <div class="ibox ibox-green float-e-margins">
                    <div class="ibox-title navy-bg">
                        <h5>Users</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ (isset($Data['Userscount'])) ? $Data['Userscount'] : 0 }}</h1>
                        <small>Total No. of Users</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <div class="ibox ibox-blue float-e-margins">
                <div class="ibox-title">
                    <h5>Task Seekers Vs Task Posters</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <a href="{{ route("users.index1", "taskseeker") }}">
                            <div class="col-md-6">
                                <h1 class="no-margins">{{ (!empty($Data['TaskSeeker'])) ? $Data['TaskSeeker'] : 0 }}</h1>
                                <small>Total Task Seekers</small>
                            </div>
                        </a>
                        <a href="{{ route("users.index1", "taskposter") }}">
                            <div class="col-md-6">
                                <h1 class="no-margins">{{ (!empty($Data['TaskPoster'])) ? $Data['TaskPoster'] : 0 }}</h1>
                                <small>Total Task Posters</small>
                            </div>    
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <a href="{{route('users.index1', 'inactice') }}">
                <div class="ibox ibox-inactive float-e-margins">
                    <div class="ibox-title">
                        <h5>Total Inactive Users</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ (!empty($Data['InactiveUsercount']['count'])) ? $Data['InactiveUsercount']['count'] : 0 }}</h1>
                        <small>Total Inactive Users</small>
                    </div>
                </div>    
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{ route('statistics.index', "") }}">
                <div class="ibox ibox-brown float-e-margins">
                    <div class="ibox-title">
                        <h5>Tasks</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ (isset($Data['Taskscount'])) ? $Data['Taskscount'] : 0 }}</h1>
                        <small>Total No. of Tasks</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{ route('statistics.index', 'accepted') }}">
                <div class="ibox ibox-green float-e-margins">
                    <div class="ibox-title">
                        <h5>Accepted Tasks</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ (isset($Data['AcceptedTaskscount']['count'])) ? $Data['AcceptedTaskscount']['count'] : 0 }}</h1>
                        <small>Total No. of Accepted Tasks</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{ route('statistics.index', 'completed') }}">
                <div class="ibox ibox-blue float-e-margins">
                    <div class="ibox-title">
                        <h5>Completed Tasks</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ (isset($Data['CompletedTaskscount']['count'])) ? $Data['CompletedTaskscount']['count'] : 0 }}</h1>
                        <small>Total No. of Completed Tasks</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{route('transactions.index')}}">
                <div class="ibox ibox-orange float-e-margins">
                    <div class="ibox-title">
                        <h5>Payments</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">$ {{ (isset($Data['Payments'])) ? $Data['Payments'] : 0 }}</h1>
                        <small>Total Payments Received</small>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{ route('payouts.index') }}">
                <div class="ibox ibox-yellow float-e-margins">
                    <div class="ibox-title">
                        <h5>Payouts</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">$ {{ (isset($Data['Payouts'])) ? $Data['Payouts']['total_amount'] : 0 }}</h1>
                        <small>Total Payouts</small>
                    </div>
                </div>    
            </a>
        </div>
        <div class="col-lg-4">
            <a href="{{ route('payouts.index') }}">
                <div class="ibox ibox-purple float-e-margins">
                    <div class="ibox-title">
                        <h5>Earnings (15 % commision)</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">&#36; {{ $Data['Payments'] -  $Data['Payouts']['total_amount'] }}</h1>
                        <small>Total Earnings</small>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
@endsection
