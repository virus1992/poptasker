@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Statistics')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Statistics</h3>
                </div>
                <div class="ibox-content">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="{{ (!isset($Data['status']) && $Data['status'] == "") ? "active" : "" }}"><a href="{{ route("statistics.index", "") }}"> All </a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "archive") ? "active" : "" }}"><a href="{{ route("statistics.index", "archive") }}"> Archive </a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "pending") ? "active" : "" }}"><a href="{{ route("statistics.index", "pending") }}">Pending</a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "accepted") ? "active" : "" }}"><a href="{{ route("statistics.index", "accepted") }}">Accepted</a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "cancelled") ? "active" : "" }}"><a href="{{ route("statistics.index", "cancelled") }}">Cancelled</a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "completed") ? "active" : "" }}"><a href="{{ route("statistics.index", "completed") }}">Completed</a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "noshow") ? "active" : "" }}"><a href="{{ route("statistics.index", "noshow") }}">No Show</a></li>
                            <li class="{{ (isset($Data['status']) && $Data['status'] == "deleted") ? "active" : "" }}"><a href="{{ route("statistics.index", "deleted") }}">Deleted</a></li>                        
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active">
                                <div class="panel-body">
                                    <div class="f-to-t-form">
                                        <form role="form" class="form-inline" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" id="#SearchTransUrl" data-url="asset('admin.Transactions.index')">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>From</label>
                                                        <input class="form-control datepicker" placeholder="mm/dd/YYYY" name="startdate" autocomplete="off" id="startdate" type="text" value="{{ (isset($Data['startdate'])) ? $Data['startdate'] : "" }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>To</label>
                                                        <input class="form-control datepicker" placeholder="mm/dd/YYYY" name="enddate" autocomplete="off" id="enddate" type="text" value="{{ (isset($Data['enddate'])) ? $Data['enddate'] : "" }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Users</label>
                                                        <select class="form-control" name="user_id">
                                                            <option value="">Select Users</option>
                                                            @if(isset($Data['Users']) && !empty($Data['Users']))
                                                            @foreach($Data['Users'] as $k1 => $v1)
                                                            <option value="{{ $v1['id'] }}" {{ (isset($Data['userid']) && $Data['userid'] == $v1['id']) ? "selected" : "" }}>{{ $v1['firstname'] . " " . $v1['lastname'] }}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" id="SearchTrans" class="btn btn-primary">Search</button>
                                                    </div>
                                                </div>
                                            </div>																												
                                        </form>
                                    </div>
                                    <div class="stats-top-form">
                                        <div class="row">
                                            <div class="col-md-4 text-center">
                                                <p><span>No. of Applications</span> <span id="jobs-applied">{{ ($Data['TasksByStatusCount']['TotalApplication'] != "") ? $Data['TasksByStatusCount']['TotalApplication'] : 0 }}</span></p>
                                            </div>  
                                            <div class="col-md-4 text-center">
                                                <p><span>No. of Tasks Posted</span> <span id="jobs-posted">{{ $Data['TasksByStatusCount']['count'] }}</span></p>
                                            </div>  
                                            <div class="col-md-4 text-center">
                                                <p><span>Total Tasks</span> <span id="total-jobs">{{ $Data['TotalTasks'] }}</span></p>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-statistics" >
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th width="13%">Task Picture</th>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Address</th>
                                                    <th>Earnings</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th class="nosort">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($Data['TasksByStatus']) && !empty($Data['TasksByStatus']))
                                                @foreach($Data['TasksByStatus'] as $key => $value)
                                                <tr class="{{ (isset($value['is_deleted']) && $value['is_deleted'] == 1) ? 'deleted-row' : "" }}">
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>
                                                        @if($value['picture'] != "")
                                                        @if(file_exists(storage_path('app/public/taskpictures/tmp/' . $value['picture'])))
                                                        <div class="table-img">
                                                            <img alt="image" class="img-circle" src="{{ URL::to('/storage/app/public/taskpictures/tmp/') . '/' . $value['picture'] }}" />
                                                        </div>
                                                        @endif
                                                        @endif
                                                    </td>
                                                    <td>{{ $value['title'] }}</td>
                                                    <td>{{ $value['description'] }}</td>
                                                    <td>{{ $value['address'] }}</td>
                                                    <td>{{ $value['wage'] }}</td>
                                                    <td>
                                                        {{ ($value['start_date'] != "") ? date('m/d/Y', strtotime($value['start_date'])) : "" }}<br/> 
                                                    </td>
                                                    <td>
                                                        {{ ($value['end_date'] != "") ? date('m/d/Y', strtotime($value['end_date'])) : "" }}<br/> 
                                                    </td>
                                                    <td class="text-center action-btn2" width="25%">
                                                        <a href="{{ route('application.index', $value['id']) }}" class="btn btn-view btn-sm" title="View Applicants">View Applicants</a>
                                                        @if(isset($value['is_deleted']) && $value['is_deleted'] == 0)
                                                        <a href="javascript:void(0)" data-url="{{ route('statistics.delete', [$value['id'], ($Data['status']) ? $Data['status'] : 'all']) }}" class="delete_item btn btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        @endif                                                        
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('css/admin/plugins/datepicker/datepicker3.css') }}" rel="stylesheet">
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/statistics.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/custom-delete.js') }}"></script>
@endsection