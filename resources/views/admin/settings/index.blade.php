@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Settings')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Settings</h3>
                </div>
                <div class="ibox-content">
                    <div class="cms-form-section">
                        <form method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" name="contact_no" value="{{ (isset($Settings->contact_no)) ? $Settings->contact_no : "" }}" class="form-control" placeholder="Enter Phone">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" value="{{ (isset($Settings->email)) ? $Settings->email : "" }}" class="form-control" placeholder="Enter Email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Commision</label>
                                        <input type="text" name="commision" value="{{ (isset($Settings->commision)) ? $Settings->commision : "" }}" class="form-control" placeholder="Enter Commision">
                                    </div>
                                </div>
                            </div>
                            <div class="send-btn2">
                                <button type="submit" class="btn btn-primary" name="Save" value="Settings">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Change Password</h3>
                </div>
                <div class="ibox-content">
                    <div class="cms-form-section">
                        <form method="post" class="FormValidate">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="confirm_password" class="form-control" placeholder="Re enter your Password">
                                    </div>
                                </div>
                            </div>
                            <div class="send-btn2">
                                <button type="submit" class="btn btn-primary" name="Save" value="PASSWORD_RESET">Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/custom/custom-validation.js') }}"></script>
@endsection