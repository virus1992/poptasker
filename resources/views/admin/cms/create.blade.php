@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Cms')

@section('content')



<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox">
                <div class="ibox-title cutom-page-title">
                        <h3 class="custom-page-heading">{{ (Route::currentRouteName() == "cms.edit") ? "Edit" : "Add" }} Cms</h3>
                    </div>
                <div class="ibox-content">
                    <div class="cms-form-section">
                        <form method="post">
                            @csrf
                            
                            <div class="form-group">
                                <label>Page Title:</label>
                                <input type="text" name="page_title" value="{{ (isset($Cms->page_title)) ? $Cms->page_title : "" }}" class="form-control" placeholder="Enter Page Title">
                            </div>
                            <div class="form-group">
                                <label>Page Description:</label>
                                <textarea class="form-control summernote" name="page_description">{{ (isset($Cms->page_description)) ? $Cms->page_description : "" }}</textarea>
                            </div>
                            <div class="send-btn2">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="{{ route('cms.index') }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')
<link href="{{ asset('css/admin/plugins/summernote/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/tooltip.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/summernote/summernote.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/custom-summernote.js') }}"></script>

@endsection