@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Cms')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Cms</h3>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('cms.create') }}" class="btn btn-warning pull-right">Add +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-cms" >
                                    <thead>
                                        <tr>
                                            <th width="10%">S No</th>
                                            <th>Page Title</th>
                                            <th width="15%" class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($Cms)
                                            @if(!empty($Cms))
                                                @foreach($Cms as $key => $value)
                                                    <tr class="{{ (isset($value['is_deleted']) && $value['is_deleted'] == 1) ? 'deleted-row' : "" }}">
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $value['page_title'] }}</td>
                                                        <td class="text-center">
                                                            @if(isset($value['is_deleted']) && $value['is_deleted'] == 0)
                                                            <a href="{{ url('admin/cms/edit/' . $value['id'])  }}" class="btn btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                            <a href="javascript:void(0)" data-url="{{ url('admin/cms/delete/' .  $value['id']) }}" class="delete_item btn btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/cms.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/custom-delete.js') }}"></script>
@endsection