<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>
        <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/media.css') }}" rel="stylesheet">

        @yield('styles')
    </head>
    <body class="gray-bg">
        @yield('content')

        <script type="text/javascript" src="{{ asset('js/admin/jquery-2.1.1.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
        
        @yield('scripts')
    </body>
</html>
