<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
        {{-- CSRF Token --}} 
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/admin/animate.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/admin/style.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/admin/media.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/admin/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/custom.css') }}" rel="stylesheet">

        <link href="{{ asset('css/admin/plugins/switchery/switchery.css') }}" rel="stylesheet">

        @yield('styles')

    </head>
    <body>
        <div id="wrapper">
            
            {{-- INCLUDE SIDE BAR  --}}
            @include('admin.includes.sidebar')

            <div id="page-wrapper" class="gray-bg">
                {{-- INCLUDE HEADER --}}
                @include('admin.includes.header')
                
                
                {{-- INCLUDE BODY CONTENT --}}
                @yield('content')

                {{-- INCLUDE FOOTER --}}
                @include('admin.includes.footer')
            </div>

        </div>

        {{-- MAIN SCRIPTS --}}
        <script type="text/javascript" src="{{ asset('js/admin/jquery-2.1.1.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/jquery.validate.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

        {{-- Flot --}}

        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.spline.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.resize.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.pie.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.symbol.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.time.js') }}"></script>

        {{-- Peity --}}

        <script type="text/javascript" src="{{ asset('js/admin/plugins/peity/jquery.peity.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/demo/peity-demo.js') }}"></script>

        {{-- Custom and plugin javascript --}}
        <script type="text/javascript" src="{{ asset('js/admin/inspinia.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/pace/pace.min.js') }}"></script>

        {{-- jQuery UI --}}

        <!--<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>-->

        {{-- Jvectormap --}}
        <script type="text/javascript" src="{{ asset('js/admin/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

        {{-- EayPIE --}}

        <script type="text/javascript" src="{{ asset('js/admin/plugins/easypiechart/jquery.easypiechart.js') }}"></script>

        {{-- Sparkline --}}
        <script type="text/javascript" src="{{ asset('js/admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

        {{-- Sparkline demo data  --}}
        <script type="text/javascript" src="{{ asset('js/admin/demo/sparkline-demo.js') }}"></script>

        {{-- Data Table --}}

        <script type="text/javascript" src="{{ asset('js/admin/plugins/dataTables/datatables.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/admin/plugins/sweetalert/sweetalert.min.js') }}"></script>

        <script src="{{ asset('js/admin/plugins/switchery/switchery.js') }}"></script>

        @yield('scripts')
        <script>
            $(document).ready(function () {
                $("#alert-message").fadeTo(1000, 500).slideUp(500, function () {
                    $("#alert-message").alert('close');
                });
                
                $('.chart').easyPieChart({
                    barColor: '#f8ac59',
                    scaleLength: 5,
                    lineWidth: 4,
                    size: 80
                });

                $('.chart2').easyPieChart({
                    barColor: '#1c84c6',
                    scaleLength: 5,
                    lineWidth: 4,
                    size: 80
                });

                var data2 = [
                    [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
                    [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
                    [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
                    [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
                    [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
                    [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
                    [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
                    [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
                ];

                var data3 = [
                    [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
                    [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
                    [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
                    [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
                    [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
                    [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
                    [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
                    [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
                ];


                var dataset = [
                    {
                        label: "Number of orders",
                        data: data3,
                        color: "#1ab394",
                        bars: {
                            show: true,
                            align: "center",
                            barWidth: 24 * 60 * 60 * 600,
                            lineWidth: 0
                        }

                    }, {
                        label: "Payments",
                        data: data2,
                        yaxis: 2,
                        color: "#1C84C6",
                        lines: {
                            lineWidth: 1,
                            show: true,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.2
                                    }, {
                                        opacity: 0.4
                                    }]
                            }
                        },
                        splines: {
                            show: false,
                            tension: 0.6,
                            lineWidth: 1,
                            fill: 0.1
                        }
                    }
                ];


                var options = {
                    xaxis: {
                        mode: "time",
                        tickSize: [3, "day"],
                        tickLength: 0,
                        axisLabel: "Date",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Arial',
                        axisLabelPadding: 10,
                        color: "#d5d5d5"
                    },
                    yaxes: [{
                            position: "left",
                            max: 1070,
                            color: "#d5d5d5",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Arial',
                            axisLabelPadding: 3
                        }, {
                            position: "right",
                            clolor: "#d5d5d5",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: ' Arial',
                            axisLabelPadding: 67
                        }
                    ],
                    legend: {
                        noColumns: 1,
                        labelBoxBorderColor: "#000000",
                        position: "nw"
                    },
                    grid: {
                        hoverable: false,
                        borderWidth: 0
                    }
                };

                function gd(year, month, day) {
                    return new Date(year, month - 1, day).getTime();
                }

                var previousPoint = null, previousLabel = null;

                var mapData = {
                    "US": 298,
                    "SA": 200,
                    "DE": 220,
                    "FR": 540,
                    "CN": 120,
                    "AU": 760,
                    "BR": 550,
                    "IN": 200,
                    "GB": 120
                };
            });
        </script>
    </body>
</html>
