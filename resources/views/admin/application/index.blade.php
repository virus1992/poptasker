@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Cms')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Applications <span class="task-title">Task Name -  {{ ($Data['task_title']) ? $Data['task_title'] : "" }}</span></h3>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="f-to-t-form">
                                <form role="form" class="form-inline" method="post">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <p>From: </p>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control datepicker" placeholder="mm/dd/YYYY" name="startdate" autocomplete="off" id="startdate" type="text" value="{{ (isset($Data['startdate'])) ? $Data['startdate'] : "" }}">
                                            </div>
                                            <div class="form-group">
                                                <p>To:</p>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control datepicker" placeholder="mm/dd/YYYY" name="enddate" autocomplete="off" id="enddate" type="text" value="{{ (isset($Data['enddate'])) ? $Data['enddate'] : "" }}">
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary submit">Search</button>
                                            </div>
                                        </div>
                                    </div>																												
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-application" >
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th>Education</th>
                                            <th>Application Date</th>
                                            <th>Application Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($Data['Applications']) && !empty($Data['Applications']))
                                        @foreach($Data['Applications'] as $key => $value)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $value['firstname'] . " " . $value['lastname'] }}</td>
                                            <td>{{ $value['phone'] }}</td>
                                            <td>{{ $value['email'] }}</td>
                                            <td>{{ $value['address'] }}</td>
                                            <td>{{ $value['education'] }}</td>
                                            <td>{{ ($value['created_at']) ? date('m/d/Y H:i A', strtotime($value['created_at'])) : "" }}</td>
                                            <td>{{ ($value['application_status']) ? ucfirst($value['application_status']) : "" }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('styles')
<link href="{{ asset('css/admin/plugins/datepicker/datepicker3.css') }}" rel="stylesheet">
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/application.js') }}"></script>
@endsection