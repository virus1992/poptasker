@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Help')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Help</h3>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-status" >
                            <thead>
                                <tr>
                                    <th width="10%">S No</th>
                                    <th width="20%">Email</th>
                                    <th width="18%">Name</th>
                                    <th>Query</th>
                                    <th width="12%">Status</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @isset($Helps)
                                    @if(!empty($Helps))
                                        @foreach($Helps as $key => $value)
                                            <tr class="{{ (isset($value['is_deleted']) && $value['is_deleted'] == 1) ? 'deleted-row' : "" }}">
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $value['email'] }}</td>
                                                <td>{{ $value['firstname'] . " " . $value['lastname'] }}</td>
                                                <td>{{ $value['description'] }}</td>
                                                <td class="status">
                                                    <span id="help-status-{{ $value['id'] }}" class="text {{ ($value['is_resolved'] == 1) ? 'active' : '' }}">{{ ($value['is_resolved'] == 1) ? 'Resolved' : 'Not Resolved' }}</span>
                                                    <input type="checkbox" data-id="{{ $value['id'] }}" data-url="{{ URL::to('admin/help/changehelpstatus') }}" id="{{ $value['is_resolved'] }}" class="switch-active change-helpstatus"  {{ ($value["is_resolved"] == 1) ? "checked" : "" }} value="{{ $value['is_resolved'] }}" />                                               
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/help.js') }}"></script>
@endsection