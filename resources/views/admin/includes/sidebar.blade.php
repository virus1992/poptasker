<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <img alt="image" class="img-circle" src="{{ asset('img/default-user.png') }}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{ Auth::guard('admin')->user()->first_name . ' ' . Auth::guard('admin')->user()->last_name }}</strong>
                            </span>
                        </span> 
                    </a>
                </div>
                <div class="logo-element">
                    PT
                </div>
            </li>
            <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>                    
            </li>
            <li class="{{ (in_array(Route::currentRouteName(), ['users.index', 'users.index1','users.details'])) ? 'active' : '' }}">
                <a href="{{route('users.index') }}"><i class="fa fa-user"></i> <span class="nav-label">Manage Users</span></a>
            </li>
            
            <li class="{{ (in_array(Route::currentRouteName(), ['statistics.index', 'statistics.index1', 'application.index', 'application.index1'])) ? 'active' : '' }}">
                <a href="{{route('statistics.index', '')}}"><i class="fa fa-area-chart"></i> <span class="nav-label">Statistics</span></a>
            </li>
            <li class="{{ Request::is('admin/charts') ? 'active' : '' }}">
                <a href="{{route('admin.charts.index')}}"><i class="fa fa-bar-chart"></i> <span class="nav-label">Charts</span></a>
            </li>
            <li class="{{ (in_array(Route::currentRouteName(), ['transactions.index', 'transaction.index1'])) ? 'active' : '' }}">
                <a href="{{route('transactions.index')}}"><i class="fa fa-exchange"></i> <span class="nav-label">Transactions</span></a>
            </li>
            <li class="{{ (in_array(Route::currentRouteName(), ['cms.index','cms.create', 'cms.edit'])) ? 'active' : '' }}">
                <a href="{{route('cms.index')}}"><i class="fa fa-file"></i> <span class="nav-label">CMS</span></a>
            </li>
            <li class="{{ (in_array(Route::currentRouteName(), ['help.index'])) ? 'active' : '' }}">
                <a href="{{route('help.index')}}"><i class="fa fa-info-circle"></i> <span class="nav-label">Help</span></a>
            </li>
            <li class="{{ (in_array(Route::currentRouteName(), ['payouts.index', 'payouts.index1'])) ? 'active' : '' }}">
                <a href="{{route('payouts.index')}}"><i class="fa fa-exchange"></i> <span class="nav-label">Payouts</span></a>
            </li>
            <li class="{{ (in_array(Route::currentRouteName(), ['settings.index'])) ? 'active' : '' }}">
                <a href="{{route('settings.index')}}"><i class="fa fa-cog"></i> <span class="nav-label">Settings</span></a>
            </li>
        </ul>
    </div>
</nav>