@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Charts')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Charts</h3>
                </div>
                <div class="ibox-content">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1">Category</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">Poster & Seeker</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3">Tasks State</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-4">Transactions</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-5">Task Rating Per Category</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane fade in active">
                                <div class="panel-body">
                                    <div class="f-to-t-form">
                                        <form role="form" class="form-inline">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>From: </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control category from_date" type="text" placeholder="mm/dd/YYY">
                                                    </div>
                                                    <div class="form-group">
                                                        <p>To:</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control category to_date" type="text" disabled="" placeholder="mm/dd/YYY">
                                                    </div>
                                                </div>
                                            </div>																												
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div>
                                                        <div id="categories" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane fade">
                                <div class="panel-body">
                                    <div class="f-to-t-form">
                                        <form role="form" class="form-inline">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>From: </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control posterseeker from_date" type="text" placeholder="mm/dd/YYY">
                                                    </div>
                                                    <div class="form-group">
                                                        <p>To:</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control posterseeker to_date" type="text" disabled="" placeholder="mm/dd/YYY">
                                                    </div>
                                                </div>
                                            </div>																												
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div>
                                                        <div id="jobseekerposter" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane fade">
                                <div class="panel-body">
                                    <div class="f-to-t-form">
                                        <form role="form" class="form-inline">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>From: </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control tasks from_date" type="text" placeholder="mm/dd/YYY">
                                                    </div>
                                                    <div class="form-group">
                                                        <p>To:</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control tasks to_date" type="text" disabled="" placeholder="mm/dd/YYY">
                                                    </div>
                                                </div>
                                            </div>																												
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div>
                                                        <div id="tasks" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane fade">
                                <div class="panel-body">
                                    <div class="f-to-t-form">
                                        <form role="form" class="form-inline">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>Select: </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="form-control transaction from">
                                                            <option value="week">Weekly</option>
                                                            <option value="month">Monthly</option>
                                                            <option value="quarterly">Quarterly</option>
                                                            <option value="yearly">Yearly</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <p>Select Year:</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="form-control transaction year">
                                                            @for($year = 2017; $year <= date('Y'); $year ++)
                                                            <option value="{{ $year }}" {{ ($year == date('Y')) ? "selected" : "" }}>{{ $year }}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>																												
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div>
                                                        <div id="transaction" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-5" class="tab-pane fade">
                                <div class="panel-body">
                                    <div class="f-to-t-form">
                                        <form role="form" class="form-inline">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>From: </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control categoriesrating from_date" type="text" placeholder="mm/dd/YYY">
                                                    </div>
                                                    <div class="form-group">
                                                        <p>To:</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control categoriesrating to_date" type="text" disabled="" placeholder="mm/dd/YYY">
                                                    </div>
                                                </div>
                                            </div>																												
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div>
                                                        <div id="categoriesrating" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')
<link href="{{ asset('css/admin/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/plugins/datepicker/datepicker3.css') }}" rel="stylesheet">
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/c3/c3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/charts.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
@endsection