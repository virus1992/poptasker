
@extends('admin.layouts.admin-auth')

@section('content')


<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img  alt="Admin profile pic" class="img-circle logo-small" src="{{ asset('img/logo.png') }}" />
        </div>
        <p>Login in. To see it in action.</p>
        <form class="m-t" role="form" action="index.html">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Username" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            <a href="#"><small>Forgot password?</small></a>
            <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{ route('admin.dashboard') }}">Create an account</a>
        </form>
        <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
    </div>
</div>

@endsection