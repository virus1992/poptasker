@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Users')

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="ibox">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">Manage Users</h3>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav user-menu">
                                <li class="{{ (!isset($Data['usertype']) && $Data['usertype'] == "") ? "active" : "" }}"><a href="{{ route("users.index1","") }}"> All </a></li>
                                <li class="{{ (isset($Data['usertype']) && $Data['usertype'] == "taskposter") ? "active" : "" }}"><a href="{{ route("users.index1", "taskposter") }}">Task Poster</a></li>
                                <li class="{{ (isset($Data['usertype']) && $Data['usertype'] == "taskseeker") ? "active" : "" }}"><a href="{{ route("users.index1", "taskseeker") }}">Task Seeker</a></li>
                                <li class="{{ (isset($Data['usertype']) && $Data['usertype'] == "inactice") ? "active" : "" }}"><a href="{{ route("users.index1", "inactice") }}">Inactive Users</a></li>
                            </ul>        
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-users" >
                            <thead>
                                <tr>
                                    <th>S No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Picture</th>
                                    <th>Status</th>
                                    <th>Confirm</th>
                                    <th class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($Data['Users'])
                                @if(!empty($Data['Users']))
                                @foreach($Data['Users'] as $key => $value)
                                <tr class="{{ (isset($value['is_deleted']) && $value['is_deleted'] == 1) ? 'deleted-row' : "" }}">
                                    <td>{{ $key + 1 }}</td>
                                    <td><a title="More Details" href="{{ route('users.details',  $value['id']) }}">{{ $value['firstname'] . $value['lastname'] }}</a></td>
                                    <td>{{ $value['users_email'] }}</td>
                                    <td>{{ $value['phone'] }}</td>
                                    <td>
                                        <div class="table-img">
                                            @if($value['profilepic'] != "")
                                            @if(file_exists(storage_path('app/public/profilepic/tmp/' . $value['profilepic'])))
                                            <img alt="image" class="img-circle" src="{{ URL::to('/storage/app/public/profilepic/tmp/') . '/' . $value['profilepic'] }}" />
                                            @endif
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ ($value['is_deleted'] == 1) ? "Deleted" : "" }}</td>
                                    <td class="status">
                                        <span id="confirm-status-{{ $value['id'] }}" class="text {{ ($value['is_confirm'] == 1) ? 'active' : '' }}">{{ ($value['is_confirm'] == 1) ? 'Active' : 'Inactive' }}</span>
                                        <input type="checkbox" data-id="{{ $value['id'] }}" data-url="{{ URL::to('admin/users/changestatus') }}" id="{{ $value['is_confirm'] }}" class="switch-active"  {{ ($value['is_confirm'] == 1) ? 'checked' : '' }} value="{{ $value['is_confirm'] }}" />                                               
                                    </td>
                                    <td class="text-center">
                                        @if(isset($value['is_deleted']) && $value['is_deleted'] == 0)
                                        <a href="javascript:void(0)" data-url="{{ route('users.delete',  $value['id']) }}" class="delete_item btn btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/users.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/custom/custom-delete.js') }}"></script>
@endsection