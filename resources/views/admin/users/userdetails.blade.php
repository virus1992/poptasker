@extends('admin.layouts.default')

@section('title', 'Pop Tasker - Users')

@section('content')

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="ibox">
                <div class="ibox-title cutom-page-title">
                    <h3 class="custom-page-heading">User Details -  Test User 
                        @if($Data['UserDetails']['profilepic'] != "" && file_exists(storage_path('app/public/profilepic/tmp/' . $Data['UserDetails']['profilepic'])))
                        <img alt="image" class="img-circle pull-right" src="{{ URL::to('/storage/app/public/profilepic/tmp/') . '/' . $Data['UserDetails']['profilepic'] }}" />
                        @else
                        <img alt="image" class="img-circle pull-right" src="{{ asset('img/default-user.png') }}" />
                        @endif
                    </h3>
                </div>
                <div class="ibox-content">
                    <!--<p class="user-type pull-right">Job Seeker</p>-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="user-basic-details table-responsive">
                                <h2>Basic Information</h2>
                                <table class="user-details table">
                                    @if(!empty($Data['UserDetails']))
                                    @php 
                                    $userdetails = $Data['UserDetails'] 
                                    @endphp
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>{{ $userdetails['firstname'] .  " " . $userdetails['lastname'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{{ $userdetails['email'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td>{{ $userdetails['phone'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Address</th>
                                            <td>{{ $userdetails['address'] }}</td>    
                                        </tr>
                                        <tr>
                                            <th>Qualification</th>
                                            <td>{{ $userdetails['education'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Skills</th>
                                            <td>{{ $userdetails['skills'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Passion</th>
                                            <td>{{ $userdetails['passion'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Linkedin Profile</th>
                                            <td>{{ $userdetails['linkedin'] }}</td>
                                        </tr>
                                    </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="users-tasks-details">
                                <h2>Total Task Posted <span class="task-count">{{ $Data['Taskscount'] }}</span></h2>
                                <ul>
                                    <li>Archived <span class="task-count">{{ ($Data['ArchivedtaskCount']['count'] != "") ? $Data['ArchivedtaskCount']['count'] : 0 }}</span></li>
                                    <li>Pending <span class="task-count">{{ ($Data['PendingTaskcount']['count'] != "") ? $Data['PendingTaskcount']['count'] : 0 }}</span></li>
                                    <li>Accepted <span class="task-count">{{ ($Data['AcceptedTaskcount']['count'] != "") ? $Data['AcceptedTaskcount']['count'] : 0 }}</span></li>
                                    <li>Cancelled <span class="task-count">{{ ($Data['CanceledTaskcount']['count'] != "") ? $Data['CanceledTaskcount']['count'] : 0 }}</span></li>
                                    <li>Completed <span class="task-count">{{ ($Data['CompletedTaskcount']['count'] != "") ? $Data['CompletedTaskcount']['count'] : 0 }}</span></li>
                                    <li>No Show <span class="task-count">{{ ($Data['NoShowtaskCount']['count'] != "") ? $Data['NoShowtaskCount']['count'] : 0 }}</span></li>
                                    <li>Deleted <span class="task-count">{{ ($Data['DeletedTaskcount']['count'] != "") ? $Data['DeletedTaskcount']['count'] : 0 }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="users-completed-tasks">
                                <h2>Jobs Done <span class="task-count">{{ ($Data['CompletedTaskcount']['count'] != "") ? $Data['CompletedTaskcount']['count'] : 0 }}</span></h2>
                            </div>        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="users-reviews">
                                <h2>Reviews</h2>
                                <div class="user-reviews-details">
                                    @if(!empty($Data['taskReviews'])) 
                                    @foreach($Data['taskReviews'] as $key => $value)
                                    <section class="section">
                                        <nav class="nav">
                                            @if($value['taskpicture'] != "" && file_exists(storage_path('app/public/taskpictures/tmp/' . $value['taskpicture'])))
                                            <div class="table-img">
                                                <img alt="image" class="userreviews-task-picture" src="{{ URL::to('/storage/app/public/taskpictures/tmp/') . '/' . $value['taskp                                                                icture'] }}" />
                                            </div>
                                            @else
                                            <img alt="image" class="userreviews-task-picture" src="{{ asset('img/default-user.png') }}" />
                                            @endif
                                        </nav>
                                        <article class="article">
                                            <p class="task-name">{{ $value['tasktitle'] }}</p>
                                            <p class="rating">
                                                <span class="total-rating">{{ $value['rating'] }}</span>
                                                <span class="fa fa-star {{ ($value['rating'] >= 1) ? 'checked' : '' }}"></span>
                                                <span class="fa fa-star {{ ($value['rating'] >= 2) ? 'checked' : '' }}"></span>
                                                <span class="fa fa-star {{ ($value['rating'] >= 3) ? 'checked' : '' }}"></span>
                                                <span class="fa fa-star {{ ($value['rating'] >= 4) ? 'checked' : '' }}"></span>
                                                <span class="fa fa-star {{ ($value['rating'] >= 5) ? 'checked' : '' }}"></span>
                                            </p>
                                            <p class="rewiew">{{ $value['comments'] }}</p>
                                            <p class="reviewedby">
                                                <a href="{{ route('users.details',  $value['userid']) }}">
                                                    @if($value['commenterpic'] != "" && file_exists(storage_path('app/public/profilepic/tmp/' . $value['commenterpic'])))
                                                    <img alt="image" class="img-circle" src="{{ URL::to('/storage/app/public/profilepic/tmp/') . '/' . $value['profilepic'] }}" />
                                                    @else
                                                    <img alt="image" class="img-circle" src="{{ asset('img/default-user.png') }}" />
                                                    @endif
                                                    <span>{{ $value['firstname'] . " " . $value['lastname'] }}</span>
                                                </a>
                                                <span class="reviewed_date">{{ ($value['created_at'] != "") ? date('m/d/Y', strtotime($value['created_at'])) : "" }}</span>
                                            </p>
                                        </article>
                                    </section>
                                    @endforeach
                                    @else
                                    <p>No Reviews</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection