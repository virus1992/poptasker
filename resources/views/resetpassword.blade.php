
@extends('admin.layouts.admin-auth')
@section('title', 'Pop Tasker - Reset Password')
@section('content')

<div class="middle-box text-center loginscreen animated fadeInDown">
    @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable" id="alert-message">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @elseif ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <p>{{ $message }}</p>
            </div>
            @endif
    <div>
        <div>
            <img  alt="Admin profile pic" class="img-circle logo-small" src="{{ asset('img/logo.png') }}" />
        </div>
        <p>Password Reset</p>
        <form class="m-t" role="form" action="{{URL::to('reset/password/')}}" method="post" >
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ (isset($user->id)) ? $user->id : ""}}">
            <input type="hidden" name="redirect_url" value="<?php echo URL::to('password/reset/' . $user->remember_token); ?>">
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="">
            </div>
            <button type='submit' class="btn btn-primary block full-width m-b" href="">Reset Password</button>
            <!--<button type="submit" class="btn btn-primary block full-width m-b">Login</button>-->
        </form>
        <p style="color: #1a1a1a;font-size: 12px;">&copy; 2017 Poptasker All rights reserved</p>
    </div>
</div>

@endsection