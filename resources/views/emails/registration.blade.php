@extends('layouts.email.layout')
@section('content')
<div class="main" style="padding: 25px 20px 30px;text-align: center;">
    <div class="main-sec1" style="background: #FFF;border-radius: 5px;padding: 25px 30px;margin-bottom: 30px;box-shadow: 0px 0px 10px 0px rgba(84, 93, 92, 0.15);">
        <h1 style="font-size: 20px;color: #1a1a1a;margin-top: 0px;font-weight: 600;">Hi <?php echo $data['user']->firstname . ' ' . $data['user']->lastname; ?></h1>
        <p style="font-size: 15px; color: #1a1a1a; font-weight: 400;margin: 0;margin-bottom: 5px;">Thanks for signing up with with PopTasker</p>
        <p style="font-size: 15px;font-weight: 400; color: #1a1a1a; margin-bottom: 30px;display: inline-block;width: 100%;">Just one more step,  please click on the button to confirm your account</p>
        <a href="<?php echo URL::to('verify/email/' . $data['confirmation_code']) ?>" style="width: 100%;max-width: 130px;color: #FFF;font-size: 17px; 
           background-color: #311d50; border-radius: 5px;display: block;text-align: center;font-weight: 400;padding: 10px 0;text-decoration: none;    margin: 0 auto;">Verify Email</a>
        <p style="text-align: left; font-size: 15px;font-weight: 400; color: #1a1a1a; margin-bottom: 0;display: inline-block;width: 100%;">Happy Popping,</p>
        <p style="text-align: left; font-size: 15px;font-weight: 400; color: #1a1a1a; margin-bottom: 30px;display: inline-block;width: 100%;">PopTasker</p>
    </div>
</div>
@endsection