@extends('layouts.email.layout')
@section('content')
<div class="main" style="padding: 25px 20px 30px;text-align: center;">
    <div class="main-sec1" style="background: #FFF;border-radius: 5px;padding: 25px 30px;margin-bottom: 30px;box-shadow: 0px 0px 10px 0px rgba(84, 93, 92, 0.15);">
        <h1 style="font-size: 20px;color: #1a1a1a;margin-top: 0px;font-weight: 600;">Hi {{$data['user_seeker']->firstname}}</h1>
        <p style="font-size: 15px; color: #1a1a1a; font-weight: 400;margin: 0;margin-bottom: 5px;">The task {{$data['task']->title}} ,has been deleted.</p>
        <p style="font-size: 15px;font-weight: 400; color: #1a1a1a; margin-bottom: 30px;display: inline-block;width: 100%;">Remember to quickly check out other tasks on PopTasker.</p>
        <p style="text-align: left; font-size: 15px;font-weight: 400; color: #1a1a1a; margin-bottom: 0;display: inline-block;width: 100%;">Happy Popping,</p>
        <p style="text-align: left; font-size: 15px;font-weight: 400; color: #1a1a1a; margin-bottom: 30px;display: inline-block;width: 100%;">PopTasker</p>
    </div>
</div>
@endsection