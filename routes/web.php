<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});


Route::get('password/reset/{token}', 'Auth\LoginController@forgotPassword');
Route::post('reset/password', 'Auth\LoginController@resetPassword');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/verify/email/{token}', 'Auth\LoginController@verifyEmail');

Route::get('/terms_and_conditions', 'CmsController@terms_and_conditions');
Route::get('/privacy_policy', 'CmsController@privacy_policy');
Route::get('/aboutus', 'CmsController@aboutus');



//************** ROUTES FOR ADMIN [STARTS] ****************//
Route::group(['namespace' => 'Admin', 'prefix' => ''], function () {

    //******** @@AUTHETICATIONS@ ***********//
    Route::get('/', 'LoginController@showLoginForm')->name('admin.auth.login');
    Route::get('/login', 'LoginController@showLoginForm')->name('admin.auth.login');
    Route::post('/login', 'LoginController@login')->name('admin.login');
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');

    //******** @@DASHBOARD ROUTES@ ***********//
    Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/register', 'AdminController@dashboard')->name('admin.register');

    //******** @@USERS ROUTES@ ***********//
    Route::get('users', 'UsersController@index')->name('users.index');
    Route::get('users/{usertype}', 'UsersController@index')->name('users.index1');
    Route::post('users/changestatus', 'UsersController@changestatus');
    Route::get('users/delete/{id}', 'UsersController@delete')->name('users.delete');
    Route::get('users/details/{id}', 'UsersController@details')->name('users.details');

    //******** @@STATISTICS ROUTES@@ ***********//
    Route::get('statistics', 'StatisticsController@index')->name('statistics.index');
    Route::post('statistics', 'StatisticsController@index')->name('statistics.index1');
    Route::get('statistics/{status}', 'StatisticsController@index')->name('statistics.index');
    Route::post('statistics/{status}', 'StatisticsController@index')->name('statistics.index');
    Route::get('statistics/delete/{id}/{status}', 'StatisticsController@delete')->name('statistics.delete');
    
    //******** @@APPLOCATIONS ROUTES@@ ***********//
    Route::get('application/{taskid}/', 'ApplicationController@index')->name('application.index');
    Route::post('application/{taskid}/', 'ApplicationController@index')->name('application.index1');

    //******** @@CHARTS ROUTES@@ ***********//
    Route::get('charts', 'ChartsController@index')->name('admin.charts.index');
    Route::post('task/getCategorywiseTask', 'ChartsController@getCategorywiseTask');
    Route::post('task/getJobseekerPoster', 'ChartsController@getJobseekerPoster');
    Route::post('task/getTasksByStatus', 'ChartsController@getTasksByStatus');
    Route::post('task/getTransaction', 'ChartsController@getTransaction');
    Route::post('task/getCategoryRatings', 'ChartsController@getCategoryRatings');

    //******** @@TRANSACTIONS ROUTES@@ ***********//
    Route::get('transactions', 'TransactionsController@index')->name('transactions.index');
    Route::post('transactions', 'TransactionsController@index')->name('transaction.index1');
    Route::post('transactions/getRefundableTask', 'TransactionsController@getRefundableTask')->name('transaction.getRefundableTask');
    Route::post('transactions/refundamount', 'TransactionsController@refundamount')->name('transactions.refundamount');

    //******** @@CMS ROUTES@@ ***********//
    Route::get('cms', 'CmsController@index')->name('cms.index');
    Route::get('cms/create', 'CmsController@create')->name('cms.create');
    Route::post('cms/create', 'CmsController@create')->name('cms.create');
    Route::get('cms/edit/{id}', 'CmsController@edit')->name('cms.edit');
    Route::post('cms/edit/{id}', 'CmsController@edit')->name('cms.edit');
    Route::get('cms/delete/{id}', ['uses' => 'CmsController@delete']);
    
    //******** @@HELP ROUTES@@ ***********//
    Route::get('help', 'HelpController@index')->name('help.index');
    Route::post('help/changehelpstatus', 'HelpController@changehelpstatus');
    
    //******** @@PAYOUTS ROUTES@@ ***********//
    Route::get('payouts', 'PayoutsController@index')->name('payouts.index');
    Route::post('payouts', 'PayoutsController@index')->name('payouts.index1');
    Route::post('payouts/changepayoutstatus', 'PayoutsController@changepayoutstatus');
    Route::post('payouts/monthlypayoutDetails', 'PayoutsController@monthlypayoutDetails')->name('payout.monthlypayoutDetails');
    Route::post('payouts/savePaidamountdetails', 'PayoutsController@savePaidamountdetails')->name('payouts.savePaidamountdetails');
    
    //******** @@SETTINGS ROUTES@@ *****************//
    Route::get('settings', 'SettingsController@index')->name('settings.index');
    Route::post('settings', 'SettingsController@index')->name('settings.index');
});

//************* ROUTES FOR ADMIN [ENDS] ****************//
