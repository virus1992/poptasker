<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api\v1', 'prefix' => 'v1'], function() {
    Route::post('login', 'LoginController@login');
    Route::post('loginwithfb', 'LoginController@loginWithFb');
    Route::post('loginwithgoogle', 'LoginController@loginWithGoogle');
    Route::post('register', 'RegisterController@index');
    Route::post('resetpassword', 'LoginController@resetPassword');
    Route::any('testpayment', 'LoginController@testPayment');
    Route::any('testpayment/cancel', 'LoginController@testPaymentCancel');
    Route::post('socialcheck', 'LoginController@checkSocialLogin');
    Route::group(['middleware' => 'auth:api'], function() {

        /*         * *************************Task Controller************************************** */
        Route::get('keywords', 'TaskController@getKeywords');
        Route::post('createjob', 'TaskController@createTask');
        Route::post('activities', 'TaskController@getActivities');
        Route::post('awardjob', 'TaskController@awardJob');
        Route::post('getapplications', 'TaskController@getApplications');
        Route::post('canceljob', 'TaskController@cancelJob');
        Route::post('completejob', 'TaskController@completeJob');
        Route::post('task/details', 'TaskController@getTaskDetails');
        Route::get('task/notreview', 'TaskController@getTaskNotReviewed');

        /*         * *************************User Controller************************************** */
        Route::post('user/help', 'UserController@submitQuery');
        Route::post('user/profile', 'UserController@getProfile');
        Route::post('user/edit', 'UserController@editProfile');
        Route::post('review', 'UserController@reviewEmployee');
        Route::get('messages', 'UserController@getAllMessages');
        Route::post('addpayout', 'UserController@addPayoutDetails');
        Route::get('getpayout', 'UserController@getPayoutDetails');
        Route::post('employee/profile', 'UserController@getUserProfile');
        Route::post('get/reviews', 'UserController@getUserReviews');

        /*         * *************************Application Controller************************************** */
        Route::post('apply', 'ApplicationController@applyForJob');
        Route::post('getapplicantsfromjob', 'ApplicationController@getApplicantsfromjob');
        Route::post('getapplicant', 'ApplicationController@getApplicant');

        /*         * *************************Stripe Controller************************************** */
        Route::post('check/payment', 'StripeController@checkPaymentMethod');
        Route::post('add/card', 'StripeController@addCard');
        Route::post('make/payment', 'StripeController@makePayment');
        Route::post('transactions', 'StripeController@getTransactionHistory');
        Route::get('getcards', 'StripeController@getSavedCards');
        Route::post('delete/card', 'StripeController@deleteCard');

        /*         * *************************Login Controller************************************** */
        Route::post('user/signout', 'UserController@userLogout');

        /*         * *************************Favorite Controller************************************** */
        Route::post('user/favorite', 'FavoriteController@favoriteEmployee');
        Route::post('user/getfavoritelist', 'FavoriteController@getFavoriteEmployee');

        /*         * *************************Search Controller************************************** */
        Route::post('savesearch', 'SearchController@saveSearch');
        Route::post('nearbyjobs', 'SearchController@searchNearByJobs');
        Route::get('getfilters', 'SearchController@getFilters');
    });
});
