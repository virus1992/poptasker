//'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


//let tokenArrayNew = [];
function fetchUsers(users) {
    let tokenArray = [];
    console.log(users);
    let userresolve = new Promise((resolve, reject) => {
        for (var prop in users) {
            let userId = users[prop];
            // console.log("userid: ", userId);
            let userRef = admin.database().ref(`/users/${userId}`);

            userRef.on('value', (snapShot) => {
                var snapVal = snapShot.val();
                if (snapVal.token != '' && snapVal.token != null) {
                    for (value in snapVal.token) {
                        tokenArray.push(snapVal.token[value]);
                    }
                }
                //tokenArrayNew.push(snapVal.token);
                // console.log("snapshot val: ", snapShot.val());
                // console.log("token array: ", tokenArray);
                //console.log("token array new: ", tokenArrayNew);

                setTimeout(function () {
                    resolve(tokenArray);
                }, 1000);
            }, (err) => {
                reject(err);
            });
        }
    });


    return userresolve;
}

exports.sendMessageNotification = functions.database.ref('/conversations/{conversationsID}/last_message').onWrite((change, context) => {
    //const optionType = event.params.optionType;

    let conversationData = change.after.val();
    // Exit when the data is deleted.

    if (conversationData.text == "" || conversationData.text == null) {
        console.log("Stop Execution on blank value");
        return "Blank  Message";
        // break;
    }
    console.log("Convo data", conversationData);
    const message = conversationData.text;
    const senderName = conversationData.senderName;
//    const senderPicture = conversationData.senderPicture;
    const senderId = conversationData.senderId;
    // const isGroup = conversationData.isGroup;
    const messageUId = conversationData.msgID;

    const conversationUid = context.params.conversationsID;         // conversion id which gives the  conversation id to fetch  message data from messages table
    const parentId = change.after.ref.parent.key    // same as conversionID
    let groupName = "";
    console.log(message);

    let isGroup1 = change.after.ref.parent.child('isGroup');
    var isGroupVal = '';
    const isGroup = isGroup1.once("value").then(function (snapshot) {
        isGroupVal = snapshot.val();
        // to send group name as title for group messages
        if (isGroupVal == "true") {
            const groupData = admin.database().ref(`group/${parentId}`).on("value", (snapshotgroup) => {
                var snapVal1 = snapshotgroup.val();
                groupName = snapVal1.name;
            });
        }
        // to send group name as title for group messages 
        return snapshot.val();
    });


    // if is group then fetch the group name
    // if(isGroupVal == "true") {
    //   console.log("group true");
    //   const groupData = admin.database().ref(`group/${parentId}`).on("value", (snapshotgroup) => {
    //     var snapVal1 = snapshotgroup.val();
    //     groupName = snapVal1.name;
    //   });
    // }


    // console.log("group Key val: ", isGroup1.key.val);
    // console.log("group Val: ", event.data.current.child('isGroup').val());
    // console.log("group Val: ", event.data.current.child('isGroup'));
    // console.log("group Val: ", event.data.adminRef.parent.child('isGroup').data);
    // console.log("group Val: ", event.data.adminRef.parent.child('isGroup').data.val());
    // console.log(isGroup1.val());
    // isGroup1.on('value', (snapgroup) => {
    //   snapgroup.forEach((snapgrouploop) => {
    //     console.log("snapgrouploop:", snapgrouploop.key);
    //   })
    //   // isGroup.push(snapgroup.key);
    // });
    // console.log("after result: ", isGroup);
    // fetch the users of the conversations:
    let conversationUsers = [];
    let conversationUsersArray = [];
    // let obj = event.data.ref.parent.child('users');   // get the field value of the same table with the different key

    let obj = change.after.ref.parent.child('chat_history');   // get the field value of the same table with the different key

    obj.on('value', (snap) => {
        snap.forEach((snapLoop) => {
            if (conversationData.senderId !== snapLoop.key) {
                conversationUsersArray.push(snapLoop.val());
                conversationUsers.push(snapLoop.key);
            }
        });
        // console.log("conversation users: ", conversationUsers);
        // const snapVal = snap.val();
        // console.log("conversation user name in loop:", conversationUsers);

        return fetchUsers(conversationUsers).then(results => {
            var payload = {};
            // console.log("result token conversion  data: ", conversationUsersArray);
            // console.log("result tokens: ", results);
            for (var key in results) {
                console.log("result individual value: ", results[key]);
                console.log("individual conversion timestamp value: ", conversationUsersArray[key]);
                // console.log("is group type of ", typeof(isGroupVal));
                if (isGroupVal == "true") {
                    notificationTitle = `${senderName} sent a new message in ${groupName}!`;
                    // notificationTitle = `${groupName}`;
                    notificationMesssage = `${senderName}: ${message}`;
                    // notificationTitle = `You have a new message in ${groupName} from ${senderName}s!`;
                } else {
                    notificationTitle = `${senderName} sent a new message!`;
                    notificationMesssage = message;
                }
                console.log('device type', results[key].device_type);
                if (String(results[key].device_type) == "ios") {
                    payload = {
                        notification: {
//                        conv_id: conversationUid,
                            id: conversationUid,
                            msgid: messageUId,
                            //title: `${senderName} sent a new message!`,
                            title: notificationTitle,
                            // title: `You have a new message from !`,
                            body: notificationMesssage,
                            sound: 'default',
                            badge: '1',
                            priority: "high",
                            type: "chat_message",
                            message: notificationMesssage,
                            senderId: senderId,
//                            picture: senderPicture
                        },
//                    data: {
//                        conv_id: conversationUid,
//                        msgid: messageUId,
//                        senderId: senderId,
//                        isGroup: isGroupVal,
//                        // timestamp: conversationUsersArray[key],
//                        //title: `${senderName} sent a new message!`,
//                        title: notificationTitle,
//                        message: notificationMesssage,
//                        type: "chat_message",
//                    }
                    };
                } else {
                    payload = {
                        notification: {
//                        conv_id: conversationUid,
                            id: conversationUid,
                            msgid: messageUId,
                            //title: `${senderName} sent a new message!`,
                            title: notificationTitle,
                            // title: `You have a new message from !`,
                            body: notificationMesssage,
                            sound: 'default',
                            badge: '1',
                            priority: "high",
                            type: "chat_message",
                            message: notificationMesssage,
                            senderId: senderId,
                        },
                        data: {
                            conv_id: conversationUid,
                            msgid: messageUId,
                            senderId: senderId,
                            isGroup: isGroupVal,
                            // timestamp: conversationUsersArray[key],
                            //title: `${senderName} sent a new message!`,
                            title: notificationTitle,
                            message: notificationMesssage,
                            type: "chat_message",
//                            picture: senderPicture
                        }
                    };
                }
                const options = {
                    priority: "high",
                    alert: "true",
                    timeToLive: 60 * 60 * 24
                };
                //console.log(payload);
                console.log('Payload', payload);
                admin.messaging().sendToDevice(results[key].fcm_token, payload, options)
                        .then(function (response) {
                            console.log("Successfully sent message:", response);
                        })
                        .catch(function (error) {
                            console.log("Error sending message:", error);
                        });
            }
            // let payload = {
            //   notification: {
            //     // conv_id: conversationUid,
            //     id: conversationUid,
            //     msgid: messageUId,
            //     title: `You have a new message from ${senderName}!`,
            //     // title: `You have a new message from !`,
            //     body: message,
            //     // body: 'message',
            //     sound: 'default',
            //     badge: '1',
            //     priority: "high",
            //   },
            //   data: {
            //       conv_id: conversationUid,
            //       msgid: messageUId,
            //       senderId: senderId,
            //       isGroup: isGroupVal,
            //       title: `You have a new message from ${senderName}!`,
            //       message: message,
            //   }
            // };
            // const options = {
            //   priority: "high",
            //   alert: "true",
            //   timeToLive: 60 * 60 * 24
            // };
            // //console.log(payload);
            // return admin.messaging().sendToDevice(results, payload, options)
            //   .then(function (response) {
            //     console.log("Successfully sent message:", response);
            //   })
            //   .catch(function (error) {
            //     console.log("Error sending message:", error);
            //   });

        });
        return conversationUsers;
    });
});

/*exports.sendMessageNotification = functions.database.ref('/conversations/{conversationsID}/last_message').onWrite(event => {
 //const optionType = event.params.optionType;
 //console.log("message loads: ");
 let conversationData = event.data.val();
 console.log("conversation data: ", conversationData);
 });*/
