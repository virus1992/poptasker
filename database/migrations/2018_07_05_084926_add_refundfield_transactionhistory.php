<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefundfieldTransactionhistory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('transaction_history', function($table) {
            $table->integer('refund')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('transaction_history', 'refund')) {
            Schema::table('transaction_history', function (Blueprint $table) {
                $table->dropColumn('refund');
            });
        }
    }

}
