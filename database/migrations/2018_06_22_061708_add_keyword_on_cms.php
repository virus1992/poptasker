<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeywordOnCms extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('cms', function($table) {
            $table->char('keyword', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('cms', 'keyword'))
        {
            Schema::table('cms', function (Blueprint $table)
            {
                $table->dropColumn('keyword');
            });
        }
    }

}
