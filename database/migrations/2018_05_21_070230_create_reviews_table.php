<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('review_id');
            $table->integer('task_id')->unsigned();
            $table->integer('employer_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('tasks');
            $table->foreign('employer_id')->references('id')->on('users');
            $table->foreign('employee_id')->references('id')->on('users');
            $table->string('comments');
            $table->float('rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('reviews');
    }

}
