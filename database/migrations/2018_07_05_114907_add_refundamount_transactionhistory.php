<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefundamountTransactionhistory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('transaction_history', function($table) {
            $table->float('refund_amount', 8, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('transaction_history', 'refund_amount')) {
            Schema::table('transaction_history', function (Blueprint $table) {
                $table->dropColumn('refund_amount');
            });
        }
    }

}
