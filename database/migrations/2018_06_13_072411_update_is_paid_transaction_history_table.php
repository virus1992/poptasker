<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIsPaidTransactionHistoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('transaction_history', function($table) {
            $table->float('commision', 8, 2)->nullable();
            $table->tinyInteger('is_paid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('transaction_history', function($table) {
            $table->dropColumn('commision');
            $table->dropColumn('is_paid');
        });
    }

}
