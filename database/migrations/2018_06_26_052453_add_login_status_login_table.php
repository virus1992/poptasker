<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoginStatusLoginTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('login', function($table) {
            $table->tinyInteger('is_login')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('login', 'is_login')) {
            Schema::table('login', function (Blueprint $table) {
                $table->dropColumn('is_login');
            });
        }
    }

}
