<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefunddateTransactionhistory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('transaction_history', function($table) {
            $table->date('refund_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('transaction_history', 'refund_date')) {
            Schema::table('transaction_history', function (Blueprint $table) {
                $table->dropColumn('refund_date');
            });
        }
    }

}
