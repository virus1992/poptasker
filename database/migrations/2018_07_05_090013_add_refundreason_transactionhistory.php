<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefundreasonTransactionhistory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('transaction_history', function($table) {
            $table->integer('refund_reason')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('transaction_history', 'refund_reason')) {
            Schema::table('transaction_history', function (Blueprint $table) {
                $table->dropColumn('refund_reason');
            });
        }
    }

}
