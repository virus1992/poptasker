<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('address');
            $table->string('task_lat');
            $table->string('task_long');
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->float('working_hours', 8, 2)->nullable();
            $table->string('pictures')->nullable();
            $table->float('wage', 8, 2);
            $table->integer('require_employee')->default(1);
            $table->enum('status', ['pending', 'accepted', 'completed', 'cancelled', 'inprogress']);
            $table->tinyInteger('is_deleted')->default(0);
            $table->tinyInteger('no_show')->default(0);
            $table->integer('total_application')->default(0);
            $table->integer('employer_id')->unsigned();
            $table->integer('employee_id')->unsigned()->nullable();
            $table->foreign('employer_id')->references('id')->on('users');
            $table->foreign('employee_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tasks');
    }

}
