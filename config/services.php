<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Stripe, Mailgun, SparkPost and others. This file provides a sane
      | default location for this type of information, allowing packages
      | to have a conventional place to find your various credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-west-2'),
    ],
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'stripe' => [
        'model' => App\StripeUser::class,
        'key' => env('STRIPE_KEY', 'pk_live_fEAb0ffUrItLpgqZqlxK2hhD'),
        'secret' => env('STRIPE_SECRET', 'sk_live_bc3PFNBSIrOjrFVeNkBgtHT9'),
    ],
    'firebase' => [
        'database_url' => env('FB_DATABASE', 'https://poptasker-83740.firebaseio.com'),
        'secret' => env('FB_DATABASE_KEY', 'dbsecretkey'),
    ]
];
