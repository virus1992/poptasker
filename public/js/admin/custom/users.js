$(document).ready(function () {
    $('.switch-active').each(function () {
        var switchery = new Switchery(this, {color: '#50b46e'});
    });
    
    $('.dataTables-users').DataTable({
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });


    /* FOR UPDATING STATUS */
    $(document).on('change', '.switch-active', function () {
        var userid = $(this).attr('data-id');
        var statusurl = $(this).attr('data-url');
        var is_confirm = "";
        if ($(this).prop('checked')) {
            is_confirm = 1;
            $(this).val(1);
        } else {
            is_confirm = 0;
            $(this).val(0);
        }
        $.ajax({
            type: "POST",
            url: statusurl,
            data: {
                userid: userid,
                is_confirm: is_confirm
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result.status === "success") {
                    if (result.data.is_confirm == 1) {
                        $("#confirm-status-" + userid).html('Active');
                        $("#confirm-status-" + userid).addClass('active');
                    } else {
                        $("#confirm-status-" + userid).html('Inactive');
                        $("#confirm-status-" + userid).removeClass('active');
                    }
                    swal({
                        title: "Status updated successfully",
                        type: "success",
                        showCancelButton: false,
                        customClass: "sweet-alert-success",
                        confirmButtonColor: "rgb(80, 180, 110)",
                        confirmButtonText: "Ok"
                    });
                }
            }
        });
    });
});