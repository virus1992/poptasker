$(document).ready(function () {

    $('.switch-active').each(function () {
        var switchery = new Switchery(this, {color: '#50b46e'});
    });
    
    $('.dataTables-status').DataTable({
        'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
    });


    /* FOR UPDATING STATUS */
    $('.wrapper').on('change', '.change-helpstatus', function () {
        var helpid = $(this).attr('data-id');
        var statusurl = $(this).attr('data-url');
        var is_resolved = "";
        if ($(this).prop('checked')) {
            is_resolved = 1;
            $(this).val(1);
        } else {
            is_resolved = 0;
            $(this).val(0);
        }
        $.ajax({
            type: "POST",
            url: statusurl,
            data: {
                helpid: helpid,
                is_resolved: is_resolved
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result.status === "success") {
                    if (result.data.is_resolved == 1) {
                        $("#help-status-" + helpid).html('Resolved');
                        $("#help-status-" + helpid).addClass('active');
                    } else {
                        $("#help-status-" + helpid).html('Not Resolved');
                        $("#help-status-" + helpid).removeClass('active');
                    }
                    swal({
                        title: "Status updated successfully",
                        type: "success",
                        showCancelButton: false,
                        customClass: "sweet-alert-success",
                        confirmButtonColor: "rgb(80, 180, 110)",
                        confirmButtonText: "Ok"
                    });
                }
            }
        });
    });
});