$(document).ready(function () {
    $('.dataTables-payouts').DataTable({
        'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
    });

    /* FOR UPDATING STATUS */
    $('.switch-active').each(function () {
        var switchery = new Switchery(this, {color: '#50b46e'});
    });

    /*$('.wrapper').on('change', '.change-payouttatus', function () {
     
     var user_id = $(this).data('user_id');
     var month_year = $(this).data('month');
     var rowid = $(this).data('rowid');
     var statusurl = $(this).attr('data-url');
     var is_paid = "";
     if ($(this).prop('checked')) {
     is_paid = 1;
     $(this).val(1);
     } else {
     is_paid = 0;
     $(this).val(0);
     }
     $.ajax({
     type: "POST",
     url: statusurl,
     data: {
     user_id: user_id,
     month_year : month_year,
     is_paid: is_paid
     },
     headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     success: function (result) {
     if (result.status === "success") {
     if (result.data.is_paid == 1) {
     $("#payouts-status-" + rowid).html('Paid');
     $("#payouts-status-" + rowid).addClass('active');
     } else {
     $("#payouts-status-" + rowid).html('Unpaid');
     $("#payouts-status-" + rowid).removeClass('active');
     }
     swal({
     title: "Status updated successfully",
     type: "success",
     showCancelButton: false,
     customClass: "sweet-alert-success",
     confirmButtonColor: "rgb(80, 180, 110)",
     confirmButtonText: "Ok"
     });
     }
     }
     });
     });*/


    /*Get Details in Paid Popup*/
    $('.wrapper').on('click', '.PayAmountPopup', function () {

        var user_id = $(this).data('user_id');
        var month_year = $(this).data('month');
        var rowid = $(this).data('rowid');

        var PayAmountMonthlyUrl = $(this).attr('data-url');
        $('#SavePayoutform')[0].reset();
        $.ajax({
            type: "POST",
            url: PayAmountMonthlyUrl,
            data: {
                user_id: user_id,
                month_year: month_year,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result.status === "success") {
                    $("#payout_month_year").val(result.data.Monthyear);
                    $("#payout_month_yeartext").html(result.data.Monthyear);
                    $("#payout_user_id").val(result.data.user_id);
                    $("#payout_user_name_text").html(result.data.Name);
                    $("#PayableAmountTxt").html(result.data.Payableamount);
                    $("#PayableAmount").val(result.data.Payableamount);
                    $("#PaidAmount").html(result.data.paid_amount);
                    $("#BalanceAmount").val(result.data.unpaid_balance);
                    $("#BalanceAmountTxt").html(result.data.unpaid_balance);
                    $("#rowid").val(rowid);
                }
            }
        });
    });

    /*Save Paid amount Details*/
    $('#SavePayout').click(function () {
        var savePaidamountdetailsURl = $("#savePaidamountdetailsURl").attr('data-url');
        var BalanceAmount = $("#BalanceAmount").val();
        var Amount = $("input[name=amount]").val();
        var rowid = $("#rowid").val();

        if (Amount > BalanceAmount) {
            swal({
                title: "Amount is greater than balance Date",
                type: "warning",
                showCancelButton: false,
                customClass: "sweet-alert-warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok"
            });
        } else {
            $.ajax({
                type: "POST",
                url: savePaidamountdetailsURl,
                data:
                    $("#SavePayoutform").serialize(),
                        
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if (result.status === "success") {
                        if(result.data.is_paid == 1) {
                            alert(rowid);
                            $("#PayAmountMonthly-" + rowid).remove();
                            $("#payouts-status-" + rowid).html('Paid');
                            $("#payouts-status-" + rowid).addClass('active');
                        } 
                        $('#PayAmountModal').modal('hide');
                        $("#MonthlyPaidamount-" + rowid).html(result.data.ToatlpaidByMonth);
                        swal({
                            title: "Amount ($" + result.data.paidamount + ") Paid successfully",
                            type: "success",
                            showCancelButton: false,
                            customClass: "sweet-alert-success",
                            confirmButtonColor: "rgb(80, 180, 110)",
                            confirmButtonText: "Ok"
                        });
                    }
                }
            });
        }
    });
});