$(document).ready(function () {
    var dates = {'from_date': "", 'to_date': "", "select": ""};
    getJobseekerPoster(dates);
    getCategorywisetask(dates);
    getTasksByStatus(dates);
    getTransaction(dates);
    getCategoryRatings(dates);
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.categoriesrating.from_date').datepicker({
    todayBtn: false,
    autoclose: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('.categoriesrating.to_date').datepicker('setStartDate', minDate);
    if ($('.categoriesrating.from_date').val().length > 0) {
        $('.categoriesrating.to_date').removeAttr('disabled');
    } else {
        $('.categoriesrating.to_date').attr('disabled', 'disabled');
    }
});

$('.categoriesrating.to_date').on('change', function () {
    if ($('.categoriesrating.to_date').val().length > 0) {
        var from_date = $('.categoriesrating.from_date').val();
        var to_date = $('.categoriesrating.to_date').val();
        var dates = {'from_date': from_date, 'to_date': to_date};
        getCategoryRatings(dates);
    }
});

$('.category.from_date').datepicker({
    todayBtn: false,
    autoclose: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('.category.to_date').datepicker('setStartDate', minDate);
    if ($('.category.from_date').val().length > 0) {
        $('.category.to_date').removeAttr('disabled');
    } else {
        $('.category.to_date').attr('disabled', 'disabled');
    }
});

$('.category.to_date').on('change', function () {
    if ($('.category.to_date').val().length > 0) {
        var from_date = $('.category.from_date').val();
        var to_date = $('.category.to_date').val();
        var dates = {'from_date': from_date, 'to_date': to_date};
        getCategorywisetask(dates);
    }

});


$('.posterseeker.from_date').datepicker({
    todayBtn: false,
    autoclose: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('.posterseeker.to_date').datepicker('setStartDate', minDate);
    if ($('.posterseeker.from_date').val().length > 0) {
        $('.posterseeker.to_date').removeAttr('disabled');
    } else {
        $('.posterseeker.to_date').attr('disabled', 'disabled');
    }
});

$('.posterseeker.to_date').on('change', function () {
    if ($('.posterseeker.to_date').val().length > 0) {
        var from_date = $('.posterseeker.from_date').val();
        var to_date = $('.posterseeker.to_date').val();
        var dates = {'from_date': from_date, 'to_date': to_date};
        getJobseekerPoster(dates);
    }

});

$('.tasks.from_date').datepicker({
    todayBtn: false,
    autoclose: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('.tasks.to_date').datepicker('setStartDate', minDate);
    if ($('.tasks.from_date').val().length > 0) {
        $('.tasks.to_date').removeAttr('disabled');
    } else {
        $('.tasks.to_date').attr('disabled', 'disabled');
    }
});

$('.tasks.to_date').on('change', function () {
    if ($('.tasks.to_date').val().length > 0) {
        var from_date = $('.tasks.from_date').val();
        var to_date = $('.tasks.to_date').val();
        var dates = {'from_date': from_date, 'to_date': to_date};
        getTasksByStatus(dates);
    }

});
$('.transaction.from').on('change', function () {
    var from = $('.transaction.from').val();
    var year = $('.transaction.year').val();
    var dates = {"select": from, "year": year};
    getTransaction(dates);
});
$('.transaction.year').on('change', function () {
    var from = $('.transaction.from').val();
    var year = $('.transaction.year').val();
    var dates = {"select": from, "year": year};
    getTransaction(dates);
});


function getJobseekerPoster(dates) {
    $.post("task/getJobseekerPoster", dates, function (data) {
// Create the chart
        Highcharts.chart('jobseekerposter', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Task Seeker vs Task Poster'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Tasks'
                }

            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },
            "series": [
                {
                    "name": "Tasks",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }, 'json');
}
function getCategorywisetask(dates) {
    $.post("task/getCategorywiseTask", dates, function (data) {

// Create the chart
        Highcharts.chart('categories', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Categories'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Tasks'
                }

            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },
            "series": [
                {
                    "name": "Tasks",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }, 'json');
}

function getTasksByStatus(dates) {
    $.post("task/getTasksByStatus", dates, function (data) {

// Create the chart
        Highcharts.chart('tasks', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Task State'
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                title: {
                    text: 'Tasks'
                }

            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },
            series: [
                {
                    "name": "Tasks",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }, 'json');
}

function getTransaction(dates) {
    $.post("task/getTransaction", dates, function (data) {
// Create the chart
        Highcharts.chart('transaction', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Transactions'
            },
            xAxis: {
                type: 'category',

            },
            yAxis: {
                title: {
                    text: 'Wages'
                }

            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },
            series: [
                {
                    "name": "Total Wage",
                    "colorByPoint": true,
                    dataLabels: {
                        enabled: true},
                    "data": data
                }
            ]
        });
    }, 'json');
}

function getCategoryRatings(dates) {
    $.post("task/getCategoryRatings", dates, function (data) {
// Create the chart
        Highcharts.chart('categoriesrating', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Category By Rating'
            },
            xAxis: {
                type: 'category',
                tickInterval: 1
            },
            yAxis: {
                title: {
                    text: 'Average Rating'
                }

            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },
            series: [
                {
                    "name": "Average Rating",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }, 'json');
}