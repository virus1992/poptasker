$(document).ready(function () {
    $('.dataTables-transactions').DataTable({
        'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
    });


    /* CHANGE END DATE ACCORDING TO START DATE AND START DATE ACCORDING TO END DATE */
    $("#startdate").datepicker({
        todayBtn: false,
        autoclose: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#enddate').datepicker('setStartDate', minDate);
    });

    $("#enddate").datepicker().on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('#startdate').datepicker('setEndDate', maxDate);
    });


    /* GET REFUNDABLE TASK'S DEDATILS */
    $('.wrapper').on('click', '.RefundPopup', function () {
        $('#form')[0].reset();   
        var transactionid = $(this).data('id');
        var getRefundableTaskUrl = $("#getRefundableTaskUrl").attr('data-url');
        $.ajax({
            type: "POST",
            url: getRefundableTaskUrl,
            data: {
                transactionid: transactionid
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result.status === "success") {
                    $("#TaskAmount").html(result.data.amount);
                    $("#RefundedAmount").html(result.data.refund_amount);
                    $("#BalanceAmount").html(result.data.balance);
                    $("#ExtraRefundedAmount").html(result.data.extrarefunded);
                    $("#RefundableTaskid").val(result.data.id);
                }
            }
        });
    });

    /* SAVE REFUND DETAILS */
    $('#SaveRefund').click(function () {
        var transactionid = $("#RefundableTaskid").val();
        var refundamountUrl = $("#refundamountUrl").attr('data-url');
        var refund_reason = $("#refund_reason").val();
        var refund_amount = $("#refund_amount").val();
        $.ajax({
            type: "POST",
            url: refundamountUrl,
            data: {
                transactionid: transactionid,
                refund_amount: refund_amount,
                refund_reason: refund_reason
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result.status === "success") {
                    $('#refundModal').modal('hide');
                    $(".refund_amount-" + transactionid).html(result.data.refund_amount);
                    $(".refund_reason-" + transactionid).html(result.data.refund_reason);
                    if(result.data.balance === 0) {
                        $("#refundModal-" + transactionid).remove();
                        $("#RefundTxt-" + transactionid).html('<span class="text-refunded">Refunded</span>');
                    }
                    swal({
                        title: "Amount ($" + result.data.refund_amount + ") Refunded successfully",
                        type: "success",
                        showCancelButton: false,
                        customClass: "sweet-alert-success",
                        confirmButtonColor: "rgb(80, 180, 110)",
                        confirmButtonText: "Ok"
                    });
                }
            }
        });
    });

});