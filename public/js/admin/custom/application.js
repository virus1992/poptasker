$(document).ready(function () {
    $('.dataTables-application').DataTable({
        'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
    });



    /* CHANGE END DATE ACCORDING TO START DATE AND START DATE ACCORDING TO END DATE */
    $("#startdate").datepicker({
        todayBtn: false,
        autoclose: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#enddate').datepicker('setStartDate', minDate);
    });

    $("#enddate").datepicker().on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('#startdate').datepicker('setEndDate', maxDate);
    });

    $(".JobStatus").click(function () {
        var JobListStatusUrl = $("#JobListStatusUrl").data('url');
        var JobStatus = $(".JobStatus").data('status');
        $.ajax({
            type: "POST",
            url: JobListStatusUrl,
            data: {
                JobStatus: JobStatus
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                /*if (result.status === "success") {
                 swal({
                 title: "Status updated successfully",
                 type: "success",
                 showCancelButton: false,
                 confirmButtonColor: "rgb(80, 180, 110)",
                 confirmButtonText: "Ok"
                 });
                 }*/
            }
        });
    });
});
