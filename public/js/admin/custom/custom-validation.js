$(document).ready(function () {
    $('.FormValidate').validate({
        rules: {
            password: {
                minlength: 5,
                required:true
            },
            confirm_password: {
                minlength: 5,
                equalTo: "#password",
                required:true
            }
        }
    });
});
