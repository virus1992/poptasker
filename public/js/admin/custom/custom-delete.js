$(document).ready(function () {

    $('.wrapper').on('click', '.delete_item', function () {
        var deleteurl = $(this).attr( 'data-url');
            swal({
                title: "Are you sure? Want to Remove This Record",
                type: "warning",
                showCancelButton: true,
                customClass: "sweet-alert-warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!",
                closeOnConfirm: false
            }, function () {
                $('.sweet-alert').on('click', '.confirm', function () { 
                        window.location.href = deleteurl;
                });
            });
            return false;
        });
});


